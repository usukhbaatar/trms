﻿WITH Results_CTE AS (
	SELECT r.id, r.credit, r.send_date, rt.name AS research_type, r.[status], fr.[view], fr.id AS form_id,
	ROW_NUMBER() OVER (ORDER BY r.id DESC) AS row_number
	FROM dbo.BT_ResearchConfirmation AS rc
	INNER JOIN dbo.BT_Research AS r ON rc.research_id = r.id
	INNER JOIN dbo.BT_Form AS fr ON r.form_id = fr.id
	INNER JOIN dbo.BT_ResearchType AS rt ON r.research_type_id = rt.id
	WHERE rc.faculty_id = 1
) SELECT * FROM Results_CTE WHERE row_number >= 1 AND row_number < 3
