﻿SELECT r.id AS research_id,
	rd.id, rd.value,
	fe.label, fe.static_type, fe.type,
	so.name AS selection,
	j.name AS journal_name, j.date as journal_date,
	c.name AS conference_name, c.date AS conference_date,
	fl.name AS file_name, fl.id as file_id
FROM dbo.BT_Research AS r
INNER JOIN dbo.BT_ResearchData AS rd ON rd.research_id = r.id
INNER JOIN dbo.BT_FormElement AS fe ON fe.id = rd.element_id AND fe.permission > -1
LEFT JOIN dbo.BT_FormElementSelectionOption AS so ON fe.type = 'select' AND rd.value = CONVERT(NVARCHAR ,so.id)
LEFT JOIN dbo.BT_Journal AS j ON fe.static_type = 1 AND CONVERT(NVARCHAR, j.id) = rd.value
LEFT JOIN dbo.BT_Conference AS c ON fe.static_type = 2 AND CONVERT(NVARCHAR, c.id) = rd.value
LEFT JOIN dbo.BT_File AS fl ON fe.type = 'file' AND rd.value = CONVERT(NVARCHAR, fl.id)
WHERE r.id IN (1, 2, 3)
ORDER BY r.id DESC

