﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;

namespace RMS
{
    /// <summary>
    /// Custom configuration class for Global Configuration
    /// </summary>
    public class MyConfig
    {
        public static void DoConfigurations(HttpConfiguration config)
        {            
            // Ignore self refferencing loop in json nested objects
            var jsonFormatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            jsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            // TODO: custom configurations are here
        }
    }
}