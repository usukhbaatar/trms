﻿using RMS.Areas.BTime.Models;
using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class ConferenceController : ApiController
    {
        private BTimeDbContext db = new BTimeDbContext();

        [HttpGet]
        public object find(string v)
        {
            var res = db.conferences.Where(j => j.name.StartsWith(v) || db.conference_names.Where(n => n.name.StartsWith(v) && n.root_id == j.root_id).Count() > 0).Select(x => new { id = x.id, name = x.name, date = x.date.Year.ToString() + "-" + x.date.Month.ToString() + "-" + x.date.Day.ToString() }).ToList();
            return new { res = res };
        }

        [HttpGet]
        public object get_root()
        {
            var res = db.conference_roots.ToList();
            var dimensions = db.dimensions.ToList();
            return new { res = res, dimensions = dimensions };
        }

        [HttpGet]
        public object get(int id)
        {
            var root = db.conference_roots.Where(x => x.id == id).FirstOrDefault();
            var res = db.conferences.Where(x => x.root_id == id).ToList();
            var names = db.conference_names.Where(x => x.root_id == id).ToList();
            return new { root = root, res = res, names = names };
        }

        [HttpGet]

        /* ROOT */
        [HttpPost]
        public object post_root(BT_ConferenceRoot data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            db.conference_roots.Add(data);
            db.SaveChanges();
            return new { id = data.id };
        }

        [HttpPost]
        public object put_root(BT_ConferenceRoot data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var old = db.conference_roots.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм сэтгүүл олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpDelete]
        public object delete_root(int id)
        {
            BT_ConferenceRoot current = db.conference_roots.Find(id);
            if (current != null)
            {
                db.conference_roots.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм сэтгүүл олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        /* conference */

        [HttpPost]
        public object post(BT_Conference data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            db.conferences.Add(data);
            db.SaveChanges();
            return new { id = data.id };
        }

        [HttpPost]
        public object put(BT_Conference data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var old = db.conferences.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм сэтгүүл олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpDelete]
        public object delete(int id)
        {
            BT_Conference current = db.conferences.Find(id);
            if (current != null)
            {
                db.conferences.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм сэтгүүл олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        /* NAMES */

        [HttpPost]
        public object post_names(BT_ConferenceNames data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            db.conference_names.Add(data);
            db.SaveChanges();
            return new { id = data.id };
        }

        [HttpPost]
        public object put_names(BT_ConferenceNames data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var old = db.conference_names.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм сэтгүүл олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpDelete]
        public object delete_names(int id)
        {
            BT_ConferenceNames current = db.conference_names.Find(id);
            if (current != null)
            {
                db.conference_names.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм сэтгүүл олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

    }
}
