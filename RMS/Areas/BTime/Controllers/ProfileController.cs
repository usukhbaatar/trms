﻿using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class ProfileController : ApiController
    {

        private BTimeDbContext db = new BTimeDbContext();

        [HttpGet]
        public object get()
        {
            int my_id = 1;
            int faculty_id = 1;

            var data = db.users.Include("role").Where(x => x.id == my_id).FirstOrDefault();
            var faculty = db.school_faculties.Include("school").Where(x => x.id == faculty_id).FirstOrDefault();
            var school = db.schools.Select(x => new { x.id, x.name, x.short_name }).Where(x => x.id == faculty.school.id).FirstOrDefault();
            var degree_history = db.degree_histories.Include("degree").Where(x => x.user.id == my_id).OrderByDescending(x => x.date).ToList();
            var rank_history = db.rank_histories.Include("rank").Where(x => x.user.id == my_id).OrderByDescending(x => x.year).ThenBy(x => x.semister).ToList();
            var dimensions = db.dimensions.ToList();
            string researches_sql = @"
                SELECT
                    ru.id, ru.research_id, ru.status, ru.year, ru.semister,
                    r.credit, rt.name AS research_type, r.user_id AS user_id, r.send_date,
                    fr.[view], fr.id AS form_id, fr.title AS form_name, fr.form_type AS form_type, fr.dimension_id AS dimension_id,
                    g.number AS group_number, g.title AS group_name
                FROM dbo.BT_ResearchUser AS ru
                    INNER JOIN dbo.BT_Research AS r ON r.id = ru.research_id
                    INNER JOIN dbo.BT_ResearchType AS rt ON rt.id = r.research_type_id
                    INNER JOIN dbo.BT_Form AS fr ON fr.id = r.form_id
                    INNER JOIN dbo.BT_Group AS g ON g.id = fr.group_id
                WHERE ru.user_id = 1 ORDER BY r.id DESC";

            var researches = db.Database.SqlQuery<ResultResearch>(researches_sql, my_id).ToList();

            string ids = ""; //this is test
            foreach (var research in researches)
            {
                if (ids == "")
                    ids += research.research_id.ToString();
                else
                    ids += ", " + research.research_id.ToString();
            }

            int year = DateTime.Now.Year;
            int semister = 0;
            if (DateTime.Now.Month < 9)
                year--;
            if (DateTime.Now.Month == 1 || DateTime.Now.Month > 8)
                semister = 1;
            else
                semister = 2;

            if (ids != "")
            {
                string research_datas_sql = @"SELECT r.id AS research_id, rd.id, rd.value, fe.id AS element_id, fe.label, fe.static_type, fe.type, so.name AS selection, j.name AS journal_name, j.date as journal_date, c.name AS conference_name, c.date AS conference_date, fl.name AS file_name, fl.id as file_id FROM dbo.BT_Research AS r INNER JOIN dbo.BT_ResearchData AS rd ON rd.research_id = r.id INNER JOIN dbo.BT_FormElement AS fe ON fe.id = rd.element_id AND fe.permission > -1 LEFT JOIN dbo.BT_FormElementSelectionOption AS so ON fe.type = 'select' AND rd.value = CONVERT(NVARCHAR ,so.id) LEFT JOIN dbo.BT_Journal AS j ON fe.static_type = 1 AND CONVERT(NVARCHAR, j.id) = rd.value LEFT JOIN dbo.BT_Conference AS c ON fe.static_type = 2 AND CONVERT(NVARCHAR, c.id) = rd.value LEFT JOIN dbo.BT_File AS fl ON fe.type = 'file' AND rd.value = CONVERT(NVARCHAR, fl.id) WHERE r.id IN (" + ids + ") ORDER BY r.id DESC";
                var research_datas = db.Database.SqlQuery<ResultData>(research_datas_sql).ToList();
                string research_user_sql = @"SELECT u.firstname, u.lastname, u.picture, u.id, r.id AS research_id, d.name AS degree FROM dbo.BT_ResearchUser AS ru INNER JOIN dbo.BT_Research AS r ON r.id = ru.research_id INNER JOIN dbo.BT_User AS u ON u.id = ru.user_id LEFT JOIN ( SELECT MAX(date) AS date, user_id FROM dbo.BT_UserDegreeHistory GROUP BY user_id ) AS dh_date ON dh_date.user_id = u.id LEFT JOIN dbo.BT_UserDegreeHistory AS dh ON dh.user_id = u.id AND dh.date = dh_date.date LEFT JOIN dbo.BT_UserDegree AS d ON d.id = dh.degree_id WHERE ru.research_id IN (" + ids + ") ORDER BY r.id DESC, d.id DESC";
                var research_users = db.Database.SqlQuery<ResultUser>(research_user_sql).ToList();
                string research_external_user_sql = @"SELECT reu.firstname, reu.lastname, reu.id, reu.research_id AS research_id, d.name AS degree FROM dbo.BT_ResearchExternalUser AS reu LEFT JOIN dbo.BT_UserDegree AS d ON d.id = reu.degree WHERE reu.research_id IN (" + ids + ") ORDER BY reu.research_id DESC, reu.degree DESC";
                var research_external_users = db.Database.SqlQuery<ResultUser>(research_external_user_sql).ToList();
                return new { info = data, faculty = faculty, school = school, rank_history = rank_history, degree_history = degree_history, researches = researches, research_datas = research_datas, research_users = research_users, research_external_users = research_external_users, current = new { year = year, semister = semister }, dimensions = dimensions };
            }
            return new { info = data, faculty = faculty, school = school, rank_history = rank_history, degree_history = degree_history, researches = researches, current = new { year = year, semister = semister }, dimensions = dimensions };
        }
    }
}
