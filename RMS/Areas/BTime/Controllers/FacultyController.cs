﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class FacultyController : ApiController
    {
        /*RMS_DBEntitiesCFG db = new RMS_DBEntitiesCFG();
        // GET: api/Faculty
        public List<Faculty> Get()
        {
            return db.Faculties.ToList();
        }

        // GET: api/Faculty/5
        public Faculty Get(int id)
        {
            return db.Faculties.Where(x => x.id == id).FirstOrDefault();
        }

        // POST: api/Faculty
        public object Post(Faculty data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            db.Faculties.Add(data);
            db.SaveChanges();
            return new { id = data.id };
        }

        // PUT: api/Faculty/5
        public object Put(Faculty data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            Faculty old = db.Faculties.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм сургууль олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        // DELETE: api/Faculty/5
        public object Delete(int id)
        {
            Faculty current = db.Faculties.Find(id);
            if (current != null)
            {
                db.Faculties.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм тэнхим олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }*/
    }
}
