﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMS.Areas.BTime.Controllers
{
    public class IndexController : Controller
    {
        // GET: BTime/Index
        public ActionResult Index()
        {
            return View();
        }
    }
}