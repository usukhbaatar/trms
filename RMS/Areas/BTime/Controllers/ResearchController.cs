﻿using RMS.Areas.BTime.Models;
using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{

    public class ResultResearch
    {
        public int id { get; set; }
        public int research_id { get; set; }
        public double credit { get; set; }
        public int user_id { get; set; }
        public DateTime send_date { get; set; }
        public string research_type { get; set; }
        public int dimension_id { get; set; }
        public int form_id { get; set; }
        public string form_name { get; set; }
        public int form_type { get; set; }
        public string status { get; set; }
        public string view { get; set; }
        public string group_number { get; set; }
        public string group_name { get; set; }
        public int? year { get; set; }
        public int? semister { get; set; }
    }

    public class ResultData
    {
        public int research_id { get; set; }
        public int id { get; set; }
        public string value { get; set; }
        public int element_id { get; set; }
        public string label { get; set; }
        public int static_type { get; set; }
        public string type { get; set; }
        public string selection { get; set; }
        public string journal_name { get; set; }
        public Nullable<DateTime> journal_date { get; set; }
        public string conference_name { get; set; }
        public Nullable<DateTime> conference_date { get; set; }
        public string file_name { get; set; }
        public Nullable<int> file_id { get; set; }
    }

    public class ResultUser
    {
        public int research_id { get; set; }
        public int id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string degree { get; set; }
        public string institute { get; set; }
        public string picture { get; set; }
    }

    public class ResearchController : ApiController
    {

        private BTimeDbContext db = new BTimeDbContext();

        // GET: api/Research
        [HttpGet]
        public object get_research_types()
        {
            return db.research_types.ToList();
        }

        [HttpGet]
        public object get(int research_type, string status, int page)
        {
            int page_size = 10;
            int faculty = 1;
            string researches_sql = "";
            
            researches_sql = @"WITH Results_CTE AS ( 
                SELECT
                    rc.id,
                    r.id AS research_id,
                    r.credit,
                    r.user_id AS user_id,
                    r.send_date,
                    rt.name AS research_type,
                    rc.[status],
                    fr.[view],
                    fr.id AS form_id,
                    fr.title AS form_name,
                    fr.form_type AS form_type,
                    fr.dimension_id,
                    g.number AS group_number,
                    g.title AS group_name,
                    ROW_NUMBER() OVER (ORDER BY r.id DESC) AS row_number
                FROM dbo.BT_ResearchConfirmation AS rc
                    INNER JOIN dbo.BT_Research AS r ON rc.research_id = r.id
                    INNER JOIN dbo.BT_Form AS fr ON r.form_id = fr.id
                    INNER JOIN dbo.BT_ResearchType AS rt ON r.research_type_id = rt.id
                    INNER JOIN dbo.BT_Group AS g ON g.id = fr.group_id
                WHERE rc.faculty_id = {0} AND (rc.[status] = {1} OR {1} = 'all') AND (r.research_type_id = {2} OR {2} = 0))
                SELECT * FROM Results_CTE WHERE row_number >= {3} AND row_number < {3} + {4}";

            var researches = db.Database.SqlQuery<ResultResearch>(researches_sql, faculty, status, research_type, page, page_size).ToList();

            string ids = "";
            foreach (var research in researches)
            {
                if (ids == "")
                    ids += research.research_id.ToString();
                else
                    ids += ", " + research.research_id.ToString();
            }
            
            int cnt = db.research_confirmations.Where(x => status == "all" || x.status == status).Where(x => x.research.research_type.id == research_type || research_type == 0).Count();
            var dimensions = db.dimensions.ToList();
            if (ids != "")
            {
                string research_datas_sql = @"SELECT r.id AS research_id, rd.id, rd.value, fe.id AS element_id, fe.label, fe.static_type, fe.type, so.name AS selection, j.name AS journal_name, j.date as journal_date, c.name AS conference_name, c.date AS conference_date, fl.name AS file_name, fl.id as file_id FROM dbo.BT_Research AS r INNER JOIN dbo.BT_ResearchData AS rd ON rd.research_id = r.id INNER JOIN dbo.BT_FormElement AS fe ON fe.id = rd.element_id AND fe.permission > -1 LEFT JOIN dbo.BT_FormElementSelectionOption AS so ON fe.type = 'select' AND rd.value = CONVERT(NVARCHAR ,so.id) LEFT JOIN dbo.BT_Journal AS j ON fe.static_type = 1 AND CONVERT(NVARCHAR, j.id) = rd.value LEFT JOIN dbo.BT_Conference AS c ON fe.static_type = 2 AND CONVERT(NVARCHAR, c.id) = rd.value LEFT JOIN dbo.BT_File AS fl ON fe.type = 'file' AND rd.value = CONVERT(NVARCHAR, fl.id) WHERE r.id IN (" + ids + ") ORDER BY r.id DESC";
                var research_datas = db.Database.SqlQuery<ResultData>(research_datas_sql).ToList();
                string research_user_sql = @"SELECT u.firstname, u.lastname, u.picture, u.id, r.id AS research_id, d.name AS degree FROM dbo.BT_ResearchUser AS ru INNER JOIN dbo.BT_Research AS r ON r.id = ru.research_id INNER JOIN dbo.BT_User AS u ON u.id = ru.user_id LEFT JOIN ( SELECT MAX(date) AS date, user_id FROM dbo.BT_UserDegreeHistory GROUP BY user_id ) AS dh_date ON dh_date.user_id = u.id LEFT JOIN dbo.BT_UserDegreeHistory AS dh ON dh.user_id = u.id AND dh.date = dh_date.date LEFT JOIN dbo.BT_UserDegree AS d ON d.id = dh.degree_id WHERE ru.research_id IN (" + ids + ") ORDER BY r.id DESC, d.id DESC";
                var research_users = db.Database.SqlQuery<ResultUser>(research_user_sql).ToList();
                string research_external_user_sql = @"SELECT reu.firstname, reu.lastname, reu.id, reu.research_id AS research_id, d.name AS degree FROM dbo.BT_ResearchExternalUser AS reu LEFT JOIN dbo.BT_UserDegree AS d ON d.id = reu.degree WHERE reu.research_id IN (" + ids + ") ORDER BY reu.research_id DESC, reu.degree DESC";
                var research_external_users = db.Database.SqlQuery<ResultUser>(research_external_user_sql).ToList();
                return new { cnt = cnt, page_size = page_size, researches = researches, research_datas = research_datas, research_users = research_users, research_external_users = research_external_users, dimensions = dimensions};
            }

            return new { cnt = cnt, page_size = page_size, researches = researches, dimensions = dimensions };
        }

        // GET: api/Research/5
        [HttpGet]
        public object get(int id)
        {
            int faculty = 1;
            var research_sql = @"SELECT rc.id, r.id AS research_id, r.credit, r.user_id AS user_id, r.send_date, rt.name AS research_type, rc.[status], fr.[view], fr.id AS form_id, fr.title AS form_name, fr.form_type AS form_type, fr.dimension_id, g.number AS group_number, g.title AS group_name FROM dbo.BT_ResearchConfirmation AS rc INNER JOIN dbo.BT_Research AS r ON rc.research_id = r.id INNER JOIN dbo.BT_Form AS fr ON r.form_id = fr.id INNER JOIN dbo.BT_ResearchType AS rt ON r.research_type_id = rt.id INNER JOIN dbo.BT_Group AS g ON g.id = fr.group_id WHERE rc.faculty_id = {0} AND rc.research_id = {1}";
            var research = db.Database.SqlQuery<ResultResearch>(research_sql, faculty, id).ToList();

            if (research.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.Forbidden, new List<string> { "Ийм судалгааны ажил олдсонгүй!" });

            var research_datas_sql = @"SELECT r.id AS research_id, rd.id, rd.value, fe.id AS element_id, fe.label, fe.static_type, fe.type, so.name AS selection, j.name AS journal_name, j.date as journal_date, c.name AS conference_name, c.date AS conference_date, fl.name AS file_name, fl.id as file_id FROM dbo.BT_Research AS r INNER JOIN dbo.BT_ResearchData AS rd ON rd.research_id = r.id INNER JOIN dbo.BT_FormElement AS fe ON fe.id = rd.element_id AND fe.permission > -1 LEFT JOIN dbo.BT_FormElementSelectionOption AS so ON fe.type = 'select' AND rd.value = CONVERT(NVARCHAR ,so.id) LEFT JOIN dbo.BT_Journal AS j ON fe.static_type = 1 AND CONVERT(NVARCHAR, j.id) = rd.value LEFT JOIN dbo.BT_Conference AS c ON fe.static_type = 2 AND CONVERT(NVARCHAR, c.id) = rd.value LEFT JOIN dbo.BT_File AS fl ON fe.type = 'file' AND rd.value = CONVERT(NVARCHAR, fl.id) WHERE r.id = {0} ORDER BY r.id DESC";
            var research_datas = db.Database.SqlQuery<ResultData>(research_datas_sql, id).ToList();

            var research_users_sql = @"SELECT  u.firstname, u.lastname,  u.picture, u.id, r.id AS research_id, d.name AS degree, s.short_name + ', ' + f.short_name AS institute FROM dbo.BT_ResearchUser AS ru INNER JOIN dbo.BT_Research AS r ON r.id = ru.research_id INNER JOIN dbo.BT_User AS u ON u.id = ru.user_id LEFT JOIN ( SELECT MAX(date) AS date, user_id  FROM dbo.BT_UserDegreeHistory GROUP BY user_id ) AS dh_date ON dh_date.user_id = u.id LEFT JOIN dbo.BT_UserDegreeHistory AS dh ON dh.user_id = u.id AND dh.date = dh_date.date LEFT JOIN dbo.BT_UserDegree AS d ON d.id = dh.degree_id INNER JOIN dbo.BT_SchoolFaculty AS f ON f.id = u.faculty_id INNER JOIN dbo.BT_School AS s ON s.id = f.school_id WHERE ru.research_id = 1 ORDER BY r.id DESC, d.id DESC";
            var research_users = db.Database.SqlQuery<ResultUser>(research_users_sql, id).ToList();

            var research_external_users_sql = @"SELECT reu.firstname, reu.lastname, reu.id, reu.research_id AS research_id, d.name AS degree, reu.institute AS institute FROM dbo.BT_ResearchExternalUser AS reu LEFT JOIN dbo.BT_UserDegree AS d ON d.id = reu.degree WHERE reu.research_id = {0} ORDER BY reu.research_id DESC, reu.degree DESC";
            var research_external_users = db.Database.SqlQuery<ResultUser>(research_external_users_sql, id).ToList();

            var comments = db.comments.Where(x => x.research.id == id).ToList();
            var dimensions = db.dimensions.ToList();

            return new { research = research, research_datas = research_datas, research_users = research_users, research_external_users = research_external_users, comments = comments, dimensions = dimensions};
        }

        [HttpGet]
        public object get_my(int id)
        {
            int my_id = 1;
            var research_sql = @"SELECT
                ru.id,
                r.id AS research_id, r.credit, r.user_id AS user_id, r.send_date,
                rt.name AS research_type,
                ru.[status], ru.year, ru.semister,
                fr.[view], fr.id AS form_id, fr.title AS form_name, fr.form_type AS form_type, fr.dimension_id,
                g.number AS group_number, g.title AS group_name
                FROM dbo.BT_ResearchUser AS ru
                    INNER JOIN dbo.BT_Research AS r ON ru.research_id = r.id
                    INNER JOIN dbo.BT_Form AS fr ON r.form_id = fr.id
                    INNER JOIN dbo.BT_ResearchType AS rt ON r.research_type_id = rt.id
                    INNER JOIN dbo.BT_Group AS g ON g.id = fr.group_id
                WHERE ru.user_id = {0} AND ru.research_id = {1}";
            var research = db.Database.SqlQuery<ResultResearch>(research_sql, my_id, id).ToList();

            if (research.Count() == 0)
                return Request.CreateResponse(HttpStatusCode.Forbidden, new List<string> { "Ийм судалгааны ажил олдсонгүй!" });

            var research_datas_sql = @"SELECT r.id AS research_id, rd.id, rd.value, fe.id AS element_id, fe.label, fe.static_type, fe.type, so.name AS selection, j.name AS journal_name, j.date as journal_date, c.name AS conference_name, c.date AS conference_date, fl.name AS file_name, fl.id as file_id FROM dbo.BT_Research AS r INNER JOIN dbo.BT_ResearchData AS rd ON rd.research_id = r.id INNER JOIN dbo.BT_FormElement AS fe ON fe.id = rd.element_id AND fe.permission > -1 LEFT JOIN dbo.BT_FormElementSelectionOption AS so ON fe.type = 'select' AND rd.value = CONVERT(NVARCHAR ,so.id) LEFT JOIN dbo.BT_Journal AS j ON fe.static_type = 1 AND CONVERT(NVARCHAR, j.id) = rd.value LEFT JOIN dbo.BT_Conference AS c ON fe.static_type = 2 AND CONVERT(NVARCHAR, c.id) = rd.value LEFT JOIN dbo.BT_File AS fl ON fe.type = 'file' AND rd.value = CONVERT(NVARCHAR, fl.id) WHERE r.id = {0} ORDER BY r.id DESC";
            var research_datas = db.Database.SqlQuery<ResultData>(research_datas_sql, id).ToList();

            var research_users_sql = @"SELECT  u.firstname, u.lastname,  u.picture, u.id, r.id AS research_id, d.name AS degree, s.short_name + ', ' + f.short_name AS institute FROM dbo.BT_ResearchUser AS ru INNER JOIN dbo.BT_Research AS r ON r.id = ru.research_id INNER JOIN dbo.BT_User AS u ON u.id = ru.user_id LEFT JOIN ( SELECT MAX(date) AS date, user_id  FROM dbo.BT_UserDegreeHistory GROUP BY user_id ) AS dh_date ON dh_date.user_id = u.id LEFT JOIN dbo.BT_UserDegreeHistory AS dh ON dh.user_id = u.id AND dh.date = dh_date.date LEFT JOIN dbo.BT_UserDegree AS d ON d.id = dh.degree_id INNER JOIN dbo.BT_SchoolFaculty AS f ON f.id = u.faculty_id INNER JOIN dbo.BT_School AS s ON s.id = f.school_id WHERE ru.research_id = 1 ORDER BY r.id DESC, d.id DESC";
            var research_users = db.Database.SqlQuery<ResultUser>(research_users_sql, id).ToList();

            var research_external_users_sql = @"SELECT reu.firstname, reu.lastname, reu.id, reu.research_id AS research_id, d.name AS degree, reu.institute AS institute FROM dbo.BT_ResearchExternalUser AS reu LEFT JOIN dbo.BT_UserDegree AS d ON d.id = reu.degree WHERE reu.research_id = {0} ORDER BY reu.research_id DESC, reu.degree DESC";
            var research_external_users = db.Database.SqlQuery<ResultUser>(research_external_users_sql, id).ToList();

            var comments = db.comments.Where(x => x.research.id == id).ToList();
            var dimensions = db.dimensions.ToList();

            return new { research = research, research_datas = research_datas, research_users = research_users, research_external_users = research_external_users, comments = comments, dimensions = dimensions };
        }


        [HttpPost]
        public object status(int id, string status, string comment)
        {
            if (!(status == "accepted" || status == "canceled"))
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Буруу төлөв байна!");

            int my_id = 1;
            int faculty = 1;

            var target = db.research_confirmations.Where(x => x.research.id == id && x.faculty.id == faculty).FirstOrDefault();
            if (target == null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Ийм судалгааны ажил олдсонгүй!");

            if (status == "canceled")
            {
                System.Diagnostics.Debug.WriteLine("A");

                var confirmations = db.research_confirmations.Include("research").Include("faculty").Where(x => x.research.id == id).ToList();
                confirmations.ForEach(x => x.status = status);
                db.SaveChanges();
                
                System.Diagnostics.Debug.WriteLine("B");
                
                var users = db.research_users.Include("user").Include("research").Where(x => x.research.id == id).ToList();
                users.ForEach(x => x.status = status);
                db.SaveChanges();
                
                System.Diagnostics.Debug.WriteLine("C");

                var target_research = db.researches.Find(id);
                if (target_research != null)
                {
                    if (comment != null && comment != "")
                    {
                        db.comments.Add(new BT_ResearchComment { research = target_research, user_id = my_id, date = DateTime.Now, comment = comment });
                        db.SaveChanges();
                    }
                }
                db.researches.Where(x => x.id == id).ToList().ForEach(x => x.status = status);
                db.SaveChanges();
            }
            else
            {
                var temp = target;
                target.status = status;
                db.Entry(temp).CurrentValues.SetValues(target);
                db.SaveChanges();
                db.research_users.Include("user").Include("research").Where(x => x.research.id == id && x.user.faculty.id == faculty).ToList().ForEach(x => x.status = status);
                db.SaveChanges();
                
                int cnt = db.research_confirmations.Include("research").Include("faculty").Where(x => x.research.id == id && x.status == "pending").Count();
                if (cnt == 0)
                {
                    var target_research = db.researches.Find(id);
                    if (target_research != null)
                    {
                        if (comment != null && comment != "")
                        {
                            db.comments.Add(new BT_ResearchComment { research = target_research, user_id = my_id, date = DateTime.Now, comment = comment });
                            db.SaveChanges();
                        }
                    }
                    db.researches.Where(x => x.id == id).ToList().ForEach(x => x.status = status);
                    db.SaveChanges();
                }
            }
            
            return "Амжилттай хадгаллаа";
        }

        [HttpPost]
        public object confirm(int id, int year, int semister)
        {
            int my_id = 1;

            var research = db.researches.Find(id);
            if (research == null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Ийм судалгааны ажил олдсонгүй");

            var ru = db.research_users.Where(x => x.user.id == my_id && x.research.id == id && x.status == "accepted").FirstOrDefault();
            if (ru == null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Та энэ судалгааны ажилд оролцоогүй эсвэл энэ судалгааны ажил зөвшөөрөгдөөгүй байна!");

            int y = research.send_date.Year;
            int m = research.send_date.Month;
            int s = 0;
            if (m < 9) y--;
            s = (m == 1 || m > 8) ? 1 : 2;

            if (!(year < y + 1 || (year == y && semister < s)))
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Буруу улирал сонгосон байна.");

            db.research_users.Include("user").Include("research").Where(x => x.user.id == my_id && x.research.id == id).ToList().ForEach(x => x.status = "confirmed");
            //db.SaveChanges();
            db.research_users.Include("user").Include("research").Where(x => x.user.id == my_id && x.research.id == id).ToList().ForEach(x => x.year = year);
            //db.SaveChanges();
            db.research_users.Include("user").Include("research").Where(x => x.user.id == my_id && x.research.id == id).ToList().ForEach(x => x.semister = semister);
            db.SaveChanges();
            
            return Ok();
        }


        // POST: api/Research
        [HttpPost]
        public object post(int form_id, int research_type_id, BT_NoDb_Research datas)
        {
            var my_id = 1;
            datas.users.Add(my_id);

            var research = new BT_Research();
            var form = db.forms.Find(form_id);
            var research_type = db.research_types.Find(research_type_id);
            List<BT_NoDb_ErrorMessage> errors = new List<BT_NoDb_ErrorMessage>();

            var data = BT_Research.filter(datas.items, form);
            var users = BT_Research.filter(datas.users);
            
            var external_users = datas.external_users;

            if (form == null)
            {
                errors.Add(new BT_NoDb_ErrorMessage { element_id = 0, message = "Ийм форм олдсонгүй." });
            }

            if (research_type == null)
            {
                errors.Add(new BT_NoDb_ErrorMessage{ element_id = -1, message = "Ийм судалгааны төрөл олдсонгүй."});
            }

            if (errors.Count > 0)
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);

            errors = BT_Research.get_errors(form, data);

            if (errors.Count > 0)
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);

            research.form_id = form_id;
            research.user_id = my_id;
            research.send_date = DateTime.Now;
            research.status = "pending";
            research.credit = BT_Research.calc(form, data, users.Count + external_users.Count);
            research.research_type = research_type;

            db.researches.Add(research);

            db.SaveChanges();

            foreach (var item in data)
            {
                var elem = db.form_elements.Find(item.element_id);
                var temp = new BT_ResearchData { element = elem, research = research, value = item.value };
                db.research_datas.Add(temp);
            }

            db.SaveChanges();

            List<BT_ResearchConfirmation> confitmations = new List<BT_ResearchConfirmation>();

            foreach (var user_id in users)
            {
                var user = db.users.Include("faculty").Where(x => x.id == user_id).FirstOrDefault();
                if (user != null)
                {
                    bool exists = false;
                    foreach (var confitmation in confitmations)
                        if (confitmation.faculty.id == user.faculty.id)
                            exists = true;
                    if (!exists)
                        confitmations.Add(new BT_ResearchConfirmation { faculty = user.faculty, date = DateTime.Now, research = research, status = "pending" });
                    db.research_users.Add(new BT_ResearchUser { research = research, status = "pending", user = user });
                }
            }

            foreach (var confirmation in confitmations)
                db.research_confirmations.Add(confirmation);

            db.SaveChanges();

            foreach (var item in external_users) {
                var temp = new BT_ResearchExternalUser { research = research, firstname = item.firstname, lastname = item.lastname, degree = item.degree, institute = item.institute };
                db.research_external_users.Add(temp);
            }

            db.SaveChanges();

            return Ok();
        }

        // PUT: api/Research/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Research/5
        public void Delete(int id)
        {
        }
    }
}
