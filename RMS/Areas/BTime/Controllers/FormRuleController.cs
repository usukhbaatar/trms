﻿using RMS.Areas.BTime.Models.DAL;
using RMS.Areas.BTime.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class FormRuleController : ApiController
    {
        private BTimeDbContext db = new BTimeDbContext();

        [HttpGet]
        public object get(int id)
        {
            var elements = db.form_elements.Where(x => x.form.id == id && x.type == "number").ToList();
            var authors = db.form_rule_authors.Where(x => x.form.id == id).ToList();
            var multipliers = db.form_rule_multipliers.Where(x => x.form.id == id).ToList();
            var dividers = db.form_rule_dividers.Where(x => x.form.id == id).ToList();

            return new { elements = elements, authors = authors, multipliers = multipliers, dividers = dividers };
        }

        // ЗОХИОГЧИЙН ТООГООР

        [HttpPost]
        public object add_author(int id, BT_FormRuleAuthors data)
        {
            var form = db.forms.Find(id);
            if (form == null)
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            data.form = form;

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            db.form_rule_authors.Add(data);
            db.SaveChanges();
            data = db.form_rule_authors.Find(data.id);
            return new { id = data.id, data = data };
        }

        [HttpPost]
        public object edit_author(int id, BT_FormRuleAuthors data)
        {
            var form = db.forms.Find(id);
            if (form == null)
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            data.form = form;

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var old = db.form_rule_authors.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                data = db.form_rule_authors.Find(data.id);
                return new {id = data.id, data = data};
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм дүрэм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpDelete]
        public object delete_author(int id)
        {
            var current = db.form_rule_authors.Find(id);
            if (current != null)
            {
                db.form_rule_authors.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм дүрэм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        // ҮРЖҮҮЛЭГЧ ЭЛЕМЕНТҮҮД

        [HttpPost]
        public object add_multiplier(int id, BT_FormRuleMultiplier data)
        {
            var form = db.forms.Find(id);
            if (form == null)
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            data.form = form;

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            db.form_rule_multipliers.Add(data);
            db.SaveChanges();
            data = db.form_rule_multipliers.Find(data.id);
            return new { id = data.id, data = data };
        }

        [HttpPost]
        public object edit_multiplier(int id, BT_FormRuleMultiplier data)
        {
            var form = db.forms.Find(id);
            if (form == null)
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            data.form = form;

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var old = db.form_rule_multipliers.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                data = db.form_rule_multipliers.Find(data.id);
                return new { id = data.id, data = data };
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм дүрэм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpDelete]
        public object delete_multiplier(int id)
        {
            var current = db.form_rule_multipliers.Find(id);
            if (current != null)
            {
                db.form_rule_multipliers.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм дүрэм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        // ХУВААГЧ ТОО

        [HttpPost]
        public object add_divider(int id, BT_FormRuleDivider data)
        {
            var form = db.forms.Find(id);
            if (form == null)
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            data.form = form;

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            db.form_rule_dividers.Add(data);
            db.SaveChanges();
            data = db.form_rule_dividers.Find(data.id);
            return new { id = data.id, data = data };
        }

        [HttpPost]
        public object edit_divider(int id, BT_FormRuleDivider data)
        {
            var form = db.forms.Find(id);
            if (form == null)
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            data.form = form;

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var old = db.form_rule_dividers.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                data = db.form_rule_dividers.Find(data.id);
                return new { id = data.id, data = data };
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм дүрэм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpDelete]
        public object delete_divider(int id)
        {
            var current = db.form_rule_dividers.Find(id);
            if (current != null)
            {
                db.form_rule_dividers.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм дүрэм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }
    }
}
