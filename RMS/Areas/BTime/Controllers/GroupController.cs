﻿using RMS.Areas.BTime.Models;
using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class GroupController : ApiController
    {

        private BTimeDbContext db = new BTimeDbContext();

        [HttpGet]
        public object get()
        {
            var root = db.groups.Where(x => x.type == "root").FirstOrDefault();
            if (root == null)
                get(1);
            return get(root.id);
        }

        [HttpGet]
        public object get(int id)
        {
            var data = db.groups.Find(id);
            List<BT_Group> child = db.groups.Where(x => x.parent_id == id).ToList();
            var cnt = db.forms.Where(x => x.group_id == id && x.status == "active").Count();
            var status = new { has_form = 0 };
            if (cnt > 0)
                status = new { has_form = 1 };
            var breads = new List<object>();
            var current = data;
            while (current != null)
            {
                breads.Add(new { id = current.id, name = current.number });
                if (current.type == "root") break;
                current = db.groups.Find(current.parent_id);
            }

            return new { data, child, status, breads };
        }

        [HttpPost]
        public object post(BT_Group data)
        {
            data.group_year = DateTime.Now.Year;
            data.type = "leaf";
            
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            if (data.root_id == 0)
            {
                var root_data = new BT_GroupRoot { number = data.number, title = data.title, description = data.description };
                data.root_id = db.group_roots.Add(root_data).id;
            }
            else
            {
                var old = db.group_roots.Find(data.root_id);
                if (old != null)
                {
                    db.Entry(old).CurrentValues.SetValues(new BT_GroupRoot { number = data.number, title = data.title, description = data.description });
                }
                else
                {
                    var root_data = new BT_GroupRoot { number = data.number, title = data.title, description = data.description };
                    data.root_id = db.group_roots.Add(root_data).id;
                }
            }

            db.groups.Add(data);

            var parent = db.groups.Find(data.parent_id);
            if (parent != null)
            {
                var new_parent = parent;
                if (new_parent.type != "root")
                    new_parent.type = "node";
                db.Entry(parent).CurrentValues.SetValues(new_parent);
            }

            db.SaveChanges();
            return new { id = data.id };
        }

        [HttpPost]
        public object put(BT_Group data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var old = db.groups.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм бүлэг олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpDelete]
        public object delete(int id)
        {
            var current = db.groups.Find(id);
            if (current != null && current.type == "leaf")
            {
                if (db.groups.Where(x => x.parent_id == current.parent_id).Count() == 1)
                {
                    var parent = db.groups.Find(current.parent_id);
                    if (parent != null)
                    {
                        var new_parent = parent;
                        if (new_parent.type != "root")
                            new_parent.type = "leaf";
                        db.Entry(parent).CurrentValues.SetValues(new_parent);
                    }
                }
                db.groups.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм бүлэг олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }
    }
}
