﻿using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class CommentController : ApiController
    {

        private BTimeDbContext db = new BTimeDbContext();

        private bool can(int id, int role)
        {
            return true;
        }

        [HttpPost]
        public object post(int research_id, string comment)
        {
            int my_id = 1;
            int role_id = 3;
            if (!can(my_id, role_id))
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Та энэ үйлдлийг хийх эрхгүй байна!");

            var research = db.researches.Find(research_id);
            if (research != null)
                if (!(comment == null || comment == "")) {
                    var last = new Models.BT_ResearchComment { research = research, comment = comment, user_id = my_id, date = DateTime.Now };
                    db.comments.Add(last);
                    db.SaveChanges();
                    return new {id = last.id, date = last.date, user_id = my_id};
                }
                else
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "Сэтгэгдэл хоосон байж болохгүй!");
            else
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Ийм судалгааны ажил олдсонгүй!");
        }

        // DELETE: api/comment/5
        [HttpDelete]
        public object delete(int id)
        {
            int my_id = 1;
            var current = db.comments.Find(id);
            if (current != null)
            {
                if (my_id != current.user_id)
                    return Request.CreateResponse(HttpStatusCode.Forbidden, "Та энэ үйлдлийг хийх боломжгүй!");
                db.comments.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
                return Request.CreateResponse(HttpStatusCode.Forbidden, "Ийм сэтгэгдэл олдсонгүй!");
        }
    }
}
