﻿using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class SchoolController : ApiController
    {

        BTimeDbContext db = new BTimeDbContext();
        // GET: api/School
        public dynamic get()
        {
            return db.forms.ToList();
        }
        /*
        // GET: api/School/5
        public School get(int id)
        {
            return db.Schools.Where(x => x.id == id).FirstOrDefault();
        }

        // POST: api/School
        public object Post(School data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            db.Schools.Add(data);
            db.SaveChanges();
            return new { id = data.id };
        }

        // PUT: api/School/5
        public object Put(School data)
        {
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            School old = db.Schools.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм сургууль олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        // DELETE: api/School/5
        public object Delete(int id)
        {
            School current = db.Schools.Find(id);
            if (current != null)
            {
                db.Schools.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм сургууль олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }*/
    }
}
