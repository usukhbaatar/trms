﻿using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{

    public class ReportController : ApiController
    {
        private BTimeDbContext db = new BTimeDbContext();

        public class ResultReport
        {
            public int id { get; set; }
            public int group_id { get; set; }
            public string group_number { get; set; }
            public string group_name { get; set; }
            public int form_type { get; set; }
            public string faculties { get; set; }
            public string users { get; set; }
            public int research_type_id { get; set; }
            public string research_type_name { get; set; }
            public int dimension_id { get; set; }
            public string dimension_name { get; set; }
            public string name { get; set; }
            public string date { get; set; }
            public int? journal_id { get; set; }
            public string journal_name { get; set; }
            public int? conference_id { get; set; }
            public string conference_name { get; set; }

        }

        
        [HttpGet]
        public object get()
        {
            var schools = db.schools.ToList();
            var groups = db.groups.ToList();
            var dimensions = db.dimensions.ToList();
            var research_types = db.research_types.ToList();

            string sql = @"SELECT
                r.id,
	            g.id AS group_id, g.number AS group_number, g.title AS group_name,
	            fr.form_type,
	            rc.faculties,
	            ru.users,
	            jr.id AS journal_id, jr.name AS journal_name,
	            cr.id AS conference_id, cr.name AS conference_name,
	            d.id AS dimension_id, d.name AS dimension_name,
	            rt.id AS research_type_id, rt.name AS research_type_name,
                rdn.value AS name,
	            rdd.value AS [date]

            FROM BT_Research AS r
            INNER JOIN BT_Form AS fr ON fr.id=r.form_id
            INNER JOIN BT_Group AS g ON g.id=fr.group_id
            INNER JOIN BT_ResearchType AS rt ON rt.id=r.research_type_id

            INNER JOIN (SELECT research_id, STUFF((SELECT DISTINCT ',' + CONVERT(NVARCHAR, id) FROM BT_ResearchConfirmation AS temp1 WHERE temp.research_id = temp1.research_id FOR XML PATH ('')) , 1, 1, '')  AS faculties FROM BT_ResearchConfirmation AS temp GROUP BY research_id) AS rc ON rc.research_id=r.id
            INNER JOIN (SELECT research_id, STUFF((SELECT DISTINCT ',' + CONVERT(NVARCHAR, id) FROM BT_ResearchUser AS temp1 WHERE temp.research_id = temp1.research_id FOR XML PATH ('')) , 1, 1, '')  AS users FROM BT_ResearchUser AS temp GROUP BY research_id) AS ru ON ru.research_id=r.id

            LEFT JOIN BT_FormElement AS fej ON fr.form_type=0 AND fej.form_id=fr.id AND fej.static_type=1
            LEFT JOIN BT_ResearchData AS rdj ON rdj.element_id=fej.id
            LEFT JOIN BT_Journal AS j ON fr.form_type=0 AND rdj.value=CONVERT(NVARCHAR, j.id)
            LEFT JOIN BT_JournalRoot AS jr ON jr.id=j.root_id

            LEFT JOIN BT_FormElement AS fec ON fr.form_type=1 AND fec.form_id=fr.id AND fec.static_type=2
            LEFT JOIN BT_ResearchData AS rdc ON rdc.element_id=fec.id
            LEFT JOIN BT_Conference AS c ON fr.form_type=1 AND rdc.value=CONVERT(NVARCHAR, c.id)
            LEFT JOIN BT_ConferenceRoot AS cr ON cr.id=c.root_id

            LEFT JOIN BT_FormElement AS fen ON fen.form_id=fr.id AND fen.static_type=5
            LEFT JOIN BT_ResearchData AS rdn ON rdn.element_id=fen.id

            LEFT JOIN BT_FormElement AS fed ON fed.form_id=fr.id AND fed.static_type=7
            LEFT JOIN BT_ResearchData AS rdd ON rdd.element_id=fed.id

            LEFT JOIN BT_Dimension AS d ON (fr.form_type=0 AND jr.dimension_id=d.id) OR (fr.form_type=1 AND cr.dimension_id=d.id)
            WHERE r.[status]='accepted'";

            var researches = db.Database.SqlQuery<ResultReport>(sql).ToList();

            return new { schools, groups, dimensions, research_types, researches };
        }
    }
}
