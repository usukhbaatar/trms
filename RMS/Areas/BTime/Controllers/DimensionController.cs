﻿using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace RMS.Areas.BTime.Controllers
{
    public class DimensionController : ApiController
    {
        private BTimeDbContext db = new BTimeDbContext();

        // GET: BTime/Dimension
        public object get()
        {
            var ret = db.dimensions.Where(x => x.id > 0).ToList();
            return ret;
        }
    }
}