﻿using RMS.Areas.BTime.Models;
using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class FormController : ApiController
    {
        private BTimeDbContext db = new BTimeDbContext();

        [HttpGet]
        public object get()
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, new List<string> { "Ийм форм олдсонгүй" });
        }

        [HttpGet]
        public object get(int id)
        {
            var ret = db.forms.Where(x => x.id == id).FirstOrDefault();
            if (ret == null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, new List<string> { "Ийм форм олдсонгүй" });
            var selections = db.form_element_selections.Where(x => x.id > 0).ToList();
            return new { form = ret, selections = selections };
        }

        [HttpGet]
        public object show(int id)
        {
            var ret = db.forms.Where(x => x.group_id == id && x.status == "active").FirstOrDefault();
            if (ret == null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, new List<string> { "Ийм форм олдсонгүй" });
            var research_types = db.research_types.ToList();
            var selections = db.form_element_selections.Where(x => x.id > 0).ToList();

            var test = db.users.Where(x => x.id == 1).FirstOrDefault();

            /*if (test.faculty == null)
                System.Diagnostics.Debug.WriteLine("null");
            else
                System.Diagnostics.Debug.WriteLine(test.data.username);*/


            return new { form = ret, research_types = research_types, selections = selections, test = test };
           }

        [HttpGet]
        public object last(int id)
        {
            var ret = db.forms.Where(x => x.group_id == id && x.status == "active").OrderByDescending(x => x.id).FirstOrDefault();
            if (ret == null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, new List<string> { "Ийм форм олдсонгүй" });
            var selections = db.form_element_selections.Where(x => x.id > 0).ToList();
            var research_types = db.research_types.ToList();
            return new { form = ret, dimensions = db.dimensions.Where(x => x.id > 0).ToList(), selections = selections };
        }

        [HttpGet]
        public object back(int id)
        {
            var ret = db.forms.Find(id);
            if (ret == null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, new List<string> { "Ийм форм олдсонгүй" });
            var selections = db.form_element_selections.Where(x => x.id > 0).ToList();
            var research_types = db.research_types.ToList();
            return new { form = ret, dimensions = db.dimensions.Where(x => x.id > 0).ToList(), selections = selections };
        }

        [HttpPost]
        public object post(BT_Form data)
        {
            data.status = "new";
            data.type = "Б";

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var last = db.forms.Where(x => x.group_id == data.group_id && x.status == "active").OrderByDescending(x => x.id).FirstOrDefault();
            
            db.forms.Add(data);

            db.SaveChanges();

            data = db.forms.Find(data.id);
            
            BT_FormElement elem = null;
            BT_FormElementValidator required = null;

            switch (data.form_type)
            {
                case 0:
                    elem = new BT_FormElement {form = data, label = "Сэтгүүлийн нэр", permission = 0, static_type = 1, type = "search" };
                    db.form_elements.Add(elem);
                    required = new BT_FormElementValidator { element = elem, name = "required" };
                    db.form_element_validators.Add(required);

                    elem = new BT_FormElement { form = data, label = "Нэр", permission = 0, static_type = 5, type = "text" };
                    db.form_elements.Add(elem);

                    elem = new BT_FormElement { form = data, label = "Хураангуй", permission = 0, static_type = 6, type = "textarea" };
                    db.form_elements.Add(elem);

                    elem = new BT_FormElement { form = data, label = "Огноо", permission = 0, static_type = 7, type = "date" };
                    db.form_elements.Add(elem);

                    db.SaveChanges();
                    break;
                case 1:
                    elem = new BT_FormElement {form = data, label = "Хурлын нэр", permission = 0, static_type = 2, type = "search" };
                    db.form_elements.Add(elem);

                    required = new BT_FormElementValidator { element = elem, name = "required" };
                    db.form_element_validators.Add(required);

                    elem = new BT_FormElement { form = data, label = "Нэр", permission = 0, static_type = 5, type = "text" };
                    db.form_elements.Add(elem);

                    elem = new BT_FormElement { form = data, label = "Хураангуй", permission = 0, static_type = 6, type = "textarea" };
                    db.form_elements.Add(elem);

                    elem = new BT_FormElement { form = data, label = "Огноо", permission = 0, static_type = 7, type = "date" };
                    db.form_elements.Add(elem);

                    db.SaveChanges();
                    break;
            }


            var old_forms = db.forms.Where(x => x.group_id == data.group_id && x.status == "new").ToList();
            foreach (var item in old_forms)
            {
                if (item != data)
                {
                    foreach (var el in db.form_elements.Where(x => x.form.id == item.id).ToList())
                        db.form_elements.Remove(el);
                    db.forms.Remove(item);
                }
            }
            db.SaveChanges();
            return new { id = data.id };
        }

        [HttpPost]
        public object put(BT_Form data)
        {
            data.status = "new";
            data.type = "Б";

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var last = db.forms.Where(x => x.group_id == data.group_id && x.status == "active").OrderByDescending(x => x.id).FirstOrDefault();
            var view = last.view;

            db.forms.Add(data);
            db.SaveChanges();

            // last - ийн элементүүд, дүрмүүдийг хуулах
            foreach (var item in db.form_elements.Where(x => x.form.id == last.id).ToList())
            {
                var clone = item;
                clone.form = data;
                db.form_elements.Add(clone);
                db.SaveChanges();
                if (view != null)
                {
                    view = view.Replace("{" + item.id.ToString() + "}", "{" + clone.id.ToString() + "}");
                    view = view.Replace("(" + item.id.ToString() + ")", "(" + clone.id.ToString() + ")");
                }

                foreach (var m in db.form_rule_multipliers.Where(x => x.element_id == item.id).ToList())
                {
                    var copy = m;
                    copy.element_id = clone.id;
                    db.form_rule_multipliers.Add(copy);
                }

                foreach (var v in db.form_element_validators.Where(x => x.element.id == item.id).ToList())
                {
                    var copy = v;
                    copy.element = clone;
                    db.form_element_validators.Add(copy);
                }
            }

            System.Diagnostics.Debug.WriteLine(view);


            data.view = view;

            var old = db.forms.Find(data.id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
            }

            foreach (var item in db.form_rule_authors.Where(x => x.form.id == last.id).ToList())
            {
                var clone = item;
                clone.form = data;
                db.form_rule_authors.Add(clone);
            }

            foreach (var item in db.form_rule_dividers.Where(x => x.form.id == last.id).ToList())
            {
                var clone = item;
                clone.form = data;
                db.form_rule_dividers.Add(clone);
            }

            var old_forms = db.forms.Where(x => x.group_id == data.group_id && x.status == "new").ToList();
            // Өмнөх баталгаажуулаагүй формуудыг устгах
            foreach (var item in old_forms)
            {
                if (item != data)
                {
                    db.forms.Remove(item);
                }
            }
            db.SaveChanges();
            return new { id = data.id };
        }

        [HttpPost]
        public object patch(BT_Form data)
        {
            data.status = "new";
            data.type = "Б";
            var id = data.id;

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }


            var old = db.forms.Find(id);
            if (old != null)
            {
                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                return new { data = data };
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpPost]
        public object activate(int id, Boolean status)
        {
            //var new_form = db.forms.Find(id);
            //var group_id = new_form.group_id;

            var old = db.forms.Find(id);//Where(x => x.group_id == group_id && x.status == "active").FirstOrDefault();

            if (old != null)
            {
                var old_active = db.forms.Where(x => x.group_id == old.group_id && x.status == "active").FirstOrDefault();
                if (old_active != null)
                {
                    var temp = old_active;
                    temp.status = "deactivated";
                    db.Entry(old_active).CurrentValues.SetValues(temp);
                }

                var _new = old;
                _new.status = "active";
                db.Entry(old).CurrentValues.SetValues(_new);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpPost]
        public object activate(int id, string view)
        {
            var old = db.forms.Find(id);
            if (old != null)
            {
                
                var _new = old;
                _new.view = view;
                db.Entry(old).CurrentValues.SetValues(_new);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpDelete]
        public object delete(int id)
        {
            id = db.forms.Where(x => x.group_id == id).FirstOrDefault().id;
            var old = db.forms.Find(id);
            if (old != null)
            {
                var _new = old;
                _new.status = "deleted";
                db.Entry(old).CurrentValues.SetValues(_new);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм форм олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }
    }
}
