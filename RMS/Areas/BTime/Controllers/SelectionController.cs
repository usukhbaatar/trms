﻿using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class SelectionController : ApiController
    {
        private BTimeDbContext db = new BTimeDbContext();

        // GET: BTime/Dimension
        public object get()
        {
            var ret = db.form_element_selections.Where(x => x.id > 0).ToList();
            return ret;
        }
    }
}
