﻿using RMS.Areas.BTime.Models;
using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class FormElementController : ApiController
    {
        private BTimeDbContext db = new BTimeDbContext();

        [HttpGet]
        public object get()
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, new List<string> { "Ийм элемент олдсонгүй" });
        }

        [HttpGet]
        public object get(int id)
        {
            var ret = db.form_elements.Find(id);
            if (ret == null)
                return Request.CreateResponse(HttpStatusCode.Forbidden, new List<string> { "Ийм элемент олдсонгүй" });
            return ret;
        }

        [HttpPost]
        public object post(int form_id, BT_FormElement data)
        {
            var form = db.forms.Find(form_id);
            data.form = form;

            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            db.form_elements.Add(data);
            db.SaveChanges();
            return new { id = data.id, data = data };
        }

        [HttpPost]
        public object put(int form_id, bool has_required, BT_FormElement data)
        {
            var form = db.forms.Find(form_id);
            data.form = form;
            var id = data.id;
            data.static_type = 0;
            if (!ModelState.IsValid)
            {
                List<string> errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }

            var old = db.form_elements.Find(id);
            if (old != null)
            {
                foreach (var item in db.form_element_validators.Where(x => x.element.id == old.id).ToList())
                    db.form_element_validators.Remove(item);
                if (has_required)
                    db.form_element_validators.Add(new BT_FormElementValidator { element = old, name = "required" });

                db.Entry(old).CurrentValues.SetValues(data);
                db.SaveChanges();
                data = db.form_elements.Find(data.id);
                return new { data = data };
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм элемент олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }

        [HttpDelete]
        public object delete(int id)
        {
            var current = db.form_elements.Find(id);
            if (current != null)
            {
                db.form_elements.Remove(current);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                List<string> errors = new List<string>();
                errors.Add("Ийм элемент олдсонгүй.");
                return Request.CreateResponse(HttpStatusCode.Forbidden, errors);
            }
        }
    }
}
