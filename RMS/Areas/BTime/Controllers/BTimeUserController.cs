﻿using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RMS.Areas.BTime.Controllers
{
    public class BTimeUserController : ApiController
    {

        private List<string[]> users = new List<string[]> {
            new string[] {"1", "usukhbaatar", "Өсөхбаатар", "Дотгонванчиг", "admin"},
            new string[] {"2", "ganbol", "Ганболд", "Равжаа", "teacher"},
            new string[] {"3", "gantumur", "Гантөмөр", "Сайнбаяр", "teacher"},
            new string[] {"4", "ganbat", "Ганбат", "Гомбожав", "teacher"},
            new string[] {"5", "gerelmaa", "Гэрэлмаа", "Баярпүрэв", "teacher"},
            new string[] {"6", "navchaa", "Навчаа", "Сүрэнбаяр", "teacher"}
        };

        private BTimeDbContext db = new BTimeDbContext();

        [HttpGet]
        public string[] get()
        {
            return users[0];
        }

        [HttpGet]
        public object get(string s)
        {
            int my_id = 1;
            List<object> ret = new List<object>();
            var user_list = db.users.Where(user => (user.firstname.StartsWith(s) || user.lastname.StartsWith(s) || user.username.StartsWith(s))).ToList();
            return new {
                res = user_list
            };
        }
    }
}
