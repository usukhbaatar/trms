'use strict';

BtimeModule.controller('AdminJournalController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {
		init();
	});
	var id = 1;

	function init() {
		id = parseInt($state.params.id);
		$scope.loading = true;
		$http.get(apiBaseUrl + '/journal/get/'+id).
			success(function(res, status, headers, config) {
				$scope.loading = false;
				$scope.root = res.root;
				$scope.journals = res.res;
				$scope.names = res.names;
			}).
			error(function(res, status, headers, config) {
				$scope.loading = false;
				$scope.toast('error', 'Алдаа', data[0]);
			});
	}
	
	function find(id) {
		for (var i = 0; i < $scope.journals.length; i++)
			if ($scope.journals[i].id == id)
				return i;
		return -1;
	}

	$scope.del = function(id) {
		$scope.confirm({title: "Сэтгүүл устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$http.delete(apiBaseUrl + '/journal/delete/' + id).
				success(function(res, status, headers, config) {
					var i = find(id);
					if (i > -1)
						$scope.journals.splice(i, 1);
					$scope.toast('success', 'Устгах', 'Устгах үйлдэл амжилттай боллоо!');
				}).
				error(function(res, status, headers, config) {
					$scope.toast('error', 'Устгах', res[0]);
				});
		});
	}

	$scope.add = function() {
		manage({name: $scope.root.name, description: $scope.root.description});
	}

	$scope.edit = function(id) {
		var i = find(id);
		manage(angular.copy($scope.journals[i]));
	}

	function manage(data) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageJournalContent.html',
			controller: 'ManageJournalInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		modalInstance.result.then(function (data) {
			save(data);
		}, function () {
			
		});
	}

	function save(data) {
		var send_data = angular.copy(data);
		send_data.root_id = id;
		send_data.date = $scope.parseDateFormat(send_data.date);
		
		if(data.hasOwnProperty('id')) {
			$http.post(apiBaseUrl + '/journal/put', send_data).
			success(function(res, status, headers, config) {
				var i = find(data.id);
				if (i > -1) $scope.journals[i] = angular.copy(data);
				$scope.toast('success', 'Сэтгүүл засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Сэтгүүл засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/journal/post', send_data).
			success(function(res, status, headers, config) {
				data.id = res.id;
				$scope.journals.push(data);
				$scope.toast('success', 'Сэтгүүл бүртгэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Сэтгүүл бүртгэх', res[0]);
			});
		}
	};

	// NAMES

	function find_names(id) {
		for (var i = 0; i < $scope.names.length; i++)
			if ($scope.names[i].id == id)
				return i;
		return -1;
	}

	$scope.del_names = function(id) {
		$scope.confirm({title: "Сэтгүүлийн нэршил устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$http.delete(apiBaseUrl + '/journal/delete_names/' + id).
				success(function(res, status, headers, config) {
					var i = find_names(id);
					if (i > -1)
						$scope.names.splice(i, 1);
					$scope.toast('success', 'Устгах', 'Устгах үйлдэл амжилттай боллоо!');
				}).
				error(function(res, status, headers, config) {
					$scope.toast('error', 'Устгах', res[0]);
				});
		});
	}

	$scope.add_names = function() {
		manage_names();
	}

	$scope.edit_names = function(id) {
		var i = find_names(id);
		manage_names(angular.copy($scope.names[i]));
	}

	function manage_names(data) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageNamesContent.html',
			controller: 'ManageNamesInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		modalInstance.result.then(function (data) {
			save_names(data);
		}, function () {
			
		});
	}

	function save_names(data) {
		var send_data = angular.copy(data);
		send_data.root_id = id;
		if(data.hasOwnProperty('id')) {
			$http.post(apiBaseUrl + '/journal/put_names', send_data).
			success(function(res, status, headers, config) {
				var i = find_names(data.id);
				if (i > -1) $scope.names[i] = angular.copy(data);
				$scope.toast('success', 'Сэтгүүлийн нэршил засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Сэтгүүлийн нэршил засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/journal/post_names', send_data).
			success(function(res, status, headers, config) {
				data.id = res.id;
				$scope.names.push(data);
				$scope.toast('success', 'Сэтгүүлийн нэршил бүртгэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Сэтгүүлийн нэршил бүртгэх', res[0]);
			});
		}
	};
})

.controller('ManageJournalInstanceCtrl', function ($scope, $modalInstance, data) {
	if (data && data.date) data.date = new Date(Date.parse(data.date));
	$scope.data = data;
	$scope.ok = function () {
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
})

.controller('ManageNamesInstanceCtrl', function ($scope, $modalInstance, data) {
	$scope.data = data;
	$scope.ok = function () {
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});