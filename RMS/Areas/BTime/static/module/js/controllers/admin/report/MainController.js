'use strict';

BtimeModule.controller('AdminReportMainController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.my_filters = [];
	$scope.dimension_map = [];
	$scope.faculty_map = [];
	$scope.group_map = [];
	$scope.research_type_map = [];
	$scope.school_map = [];
	$scope.form_type_map = ['Сэтгүүл', 'Хурал', 'Төсөл, хөтөлбөр', 'Бүтээл', 'Бусад'];
	$scope.form_types = [];
	$scope.filter = {
		school_id: -1,
		faculty_id: -1,
		group_id: -1,
		dimension_id: -1,
		form_type_id: -1,
		research_type_id: -1
	}

	var filters = [];

	$scope.loading = false;
	$scope.researches = [];
	$scope.table = [];

	$scope.$on('$viewContentLoaded', function() {
		init();
	});

	function render(i) {
		var ret = [];
		ret.push($scope.researches[i].group_number);
		ret.push($scope.researches[i].group_name);
		ret.push($scope.researches[i].research_type_name);
		ret.push($scope.researches[i].dimension_name);
		ret.push($scope.researches[i].form_type_name);

		if ($scope.researches[i].form_type == 0)
			ret.push($scope.researches[i].journal_name);
		else if ($scope.researches[i].form_type == 1)
			ret.push($scope.researches[i].conference_name);
		else
			ret.push("");

		ret.push($scope.researches[i].name);
		ret.push($scope.researches[i].date);
		return ret;
	}

	function filter_data() {
		$scope.table = [];
		for (var i in $scope.researches) {
			var flag = true;
			for (var j in filters)
				if (!filters[j](i)) {
					flag = false;
					console.log("w");
				} else {
					console.log("a");
				}
			if (flag) {
				$scope.table.push(render(i));
			}
		}
		if(!$scope.$$phase) 
			$scope.$apply();
	}

	function init() {
		$scope.loading = true;
		$http.get(apiBaseUrl + '/report/get').
			success(function(res, status, headers, config) {
				$scope.loading = false;
				res.faculties = [];
				

				for (var i in res.schools) {
					for (var j in res.schools[i].faculties) {
						res.schools[i].faculties[j].school_id =  res.schools[i].id;
						res.faculties.push(res.schools[i].faculties[j]);
					}
				}
				$scope.researches = res.researches;
				extract_ids();
				build_maps(res);
				filter_data();
				
				$scope.schools = res.schools;
				$scope.faculties = res.faculties;
				console.log(res.schools);
				console.log(res.faculties);
				$scope.groups = res.groups;
				$scope.dimensions = res.dimensions;
				$scope.research_types = res.research_types;
				for (var i = 0; i < $scope.form_type_map.length; i++)
					$scope.form_types.push({'id': i, name: $scope.form_type_map[i]});

			}).
			error(function(data, status, headers, config) {
				$scope.loading = false;
				$scope.toast('error', 'Алдаа', 'Өгөгдлийг ачаалж чадсангүй!');
			});
	}


	function filter_school(x) {
		if ($scope.filter.school_id == -1) return true;
		for (var i in $scope.researches[x].faculties) {
			console.log($scope.faculty_map[$scope.researches[x].faculties[i]]);
			if ($scope.faculty_map[$scope.researches[x].faculties[i]].school_id == $scope.filter.school_id)
				return true;
		}
		return false;
	}
	filters.push(filter_school);

	function filter_faculty(x) {
		if ($scope.filter.faculty_id == -1) return true;
		for (var i in $scope.researches[x].faculties) {
			if ($scope.researches[x].faculties[i] == $scope.filter.faculty_id)
				return true;
		}
		return false;
	}
	filters.push(filter_faculty);

	function filter_group(x) {
		if ($scope.filter.group_id == -1) return true;
		var group_id = $scope.researches[x].group_id;
		while (group_id != 0 && group_id != null && group_id != undefined) {
			if (group_id == $scope.filter.group_id) return true;
			group_id = $scope.group_map[group_id].parent_id;
		}
		return false;
	}
	filters.push(filter_group);

	function filter_dimension(x) {
		if ($scope.filter.dimension_id == -1) return true;
		if ($scope.researches[x].dimension_id == $scope.filter.dimension_id) return true;
		return false;
	}
	filters.push(filter_dimension);

	function filter_research_type(x) {
		if ($scope.filter.research_type_id == -1) return true;
		if ($scope.researches[x].research_type_id == $scope.filter.research_type_id) return true;
		return false;
	}
	filters.push(filter_research_type);

	function filter_form_type(x) {
		if ($scope.filter.form_type_id == -1) return true;
		if ($scope.researches[x].form_type == $scope.filter.form_type_id) return true;
		return false;
	}
	filters.push(filter_form_type);


	function extract_ids() {
		for (var i in $scope.researches) {
			$scope.researches[i].form_type_name = $scope.form_type_map[$scope.researches[i].form_type];
			$scope.researches[i].faculties = $scope.researches[i].faculties.split(',').map( Number );
			$scope.researches[i].users = $scope.researches[i].users.split(',').map( Number );
		}
	}

	function build_maps(res) {
		for (var i in res.dimensions) {
			$scope.dimension_map[res.dimensions[i].id] = res.dimensions[i];
		}
		for (var i in res.faculties) {
			$scope.faculty_map[res.faculties[i].id] = res.faculties[i];
		}
		for (var i in res.groups) {
			$scope.group_map[res.groups[i].id] = res.groups[i];
		}
		for (var i in res.research_types) {
			$scope.research_type_map[res.research_types[i].id] = res.research_types[i];
		}
		for (var i in res.schools) {
			$scope.school_map[res.schools[i].id] = res.schools[i];
		}
	}

	$scope.remove_filter = function(type) {
		var id = -1;
		if (type == 'school') $scope.filter.school_id = id;
		if (type == 'faculty') $scope.filter.faculty_id = id;
		if (type == 'group') $scope.filter.group_id = id;
		if (type == 'dimension') $scope.filter.dimension_id = id;
		if (type == 'research_type') $scope.filter.research_type_id = id;
		if (type == 'form_type') $scope.filter.form_type_id = id;
		filter_data();
	}

	$scope.add_filter = function(type) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'addFilterContent.html',
			controller: 'AddFilterInstanceCtrl',
			resolve: {
				type: function () {
					return type;
				},
				data: function () {
					if (type == 'school') return $scope.schools;
					if (type == 'faculty') return $scope.faculties;
					if (type == 'group') return $scope.groups;
					if (type == 'dimension') return $scope.dimensions;
					if (type == 'research_type') return $scope.research_types;
					if (type == 'form_type') return $scope.form_types;
				}
			}
		});
		modalInstance.result.then(function (id) {
			if (type == 'school') $scope.filter.school_id = id;
			if (type == 'faculty') $scope.filter.faculty_id = id;
			if (type == 'group') $scope.filter.group_id = id;
			if (type == 'dimension') $scope.filter.dimension_id = id;
			if (type == 'research_type') $scope.filter.research_type_id = id;
			if (type == 'form_type') $scope.filter.form_type_id = id;
			filter_data();
		}, function () {
			
		});
	}

})

.controller('AddFilterInstanceCtrl', function ($scope, $modalInstance, type, data) {
	$scope.type = type;
	$scope.data = data;
	if (type == 'group')
		for (var i = 0; i < $scope.data.length; i++)
			$scope.data[i].name = $scope.data[i].number + " " + $scope.data[i].title;
	console.log(data);
	$scope.ok = function () {
		$modalInstance.close($scope.selected.id);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});