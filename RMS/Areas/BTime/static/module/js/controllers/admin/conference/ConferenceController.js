'use strict';

BtimeModule.controller('AdminConferenceController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {
		init();
	});
	var id = 1;

	function init() {
		id = parseInt($state.params.id);
		$scope.loading = true;
		$http.get(apiBaseUrl + '/conference/get/'+id).
			success(function(res, status, headers, config) {
				$scope.loading = false;
				$scope.root = res.root;
				$scope.conferences = res.res;
				$scope.names = res.names;
			}).
			error(function(res, status, headers, config) {
				$scope.loading = false;
				$scope.toast('error', 'Алдаа', data[0]);
			});
	}
	
	function find(id) {
		for (var i = 0; i < $scope.conferences.length; i++)
			if ($scope.conferences[i].id == id)
				return i;
		return -1;
	}

	$scope.del = function(id) {
		$scope.confirm({title: "Эрдэм шинжилгээний хурал устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$http.delete(apiBaseUrl + '/conference/delete/' + id).
				success(function(res, status, headers, config) {
					var i = find(id);
					if (i > -1)
						$scope.conferences.splice(i, 1);
					$scope.toast('success', 'Устгах', 'Устгах үйлдэл амжилттай боллоо!');
				}).
				error(function(res, status, headers, config) {
					$scope.toast('error', 'Устгах', res[0]);
				});
		});
	}

	$scope.add = function() {
		manage({name: $scope.root.name, description: $scope.root.description});
	}

	$scope.edit = function(id) {
		var i = find(id);
		manage(angular.copy($scope.conferences[i]));
	}

	function manage(data) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageConferenceContent.html',
			controller: 'ManageConferenceInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		modalInstance.result.then(function (data) {
			save(data);
		}, function () {
			
		});
	}

	function save(data) {
		var send_data = angular.copy(data);
		send_data.root_id = id;
		send_data.date = $scope.parseDateFormat(send_data.date);
		
		if(data.hasOwnProperty('id')) {
			$http.post(apiBaseUrl + '/conference/put', send_data).
			success(function(res, status, headers, config) {
				var i = find(data.id);
				if (i > -1) $scope.conferences[i] = angular.copy(data);
				$scope.toast('success', 'Эрдэм шинжилгээний хурал засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Эрдэм шинжилгээний хурал засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/conference/post', send_data).
			success(function(res, status, headers, config) {
				data.id = res.id;
				$scope.conferences.push(data);
				$scope.toast('success', 'Эрдэм шинжилгээний хурал бүртгэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Эрдэм шинжилгээний хурал бүртгэх', res[0]);
			});
		}
	};

	// NAMES

	function find_names(id) {
		for (var i = 0; i < $scope.names.length; i++)
			if ($scope.names[i].id == id)
				return i;
		return -1;
	}

	$scope.del_names = function(id) {
		$scope.confirm({title: "Хурлын нэршил устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$http.delete(apiBaseUrl + '/conference/delete_names/' + id).
				success(function(res, status, headers, config) {
					var i = find_names(id);
					if (i > -1)
						$scope.names.splice(i, 1);
					$scope.toast('success', 'Устгах', 'Устгах үйлдэл амжилттай боллоо!');
				}).
				error(function(res, status, headers, config) {
					$scope.toast('error', 'Устгах', res[0]);
				});
		});
	}

	$scope.add_names = function() {
		manage_names();
	}

	$scope.edit_names = function(id) {
		var i = find_names(id);
		manage_names(angular.copy($scope.names[i]));
	}

	function manage_names(data) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageNamesContent.html',
			controller: 'ManageNamesInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		modalInstance.result.then(function (data) {
			save_names(data);
		}, function () {
			
		});
	}

	function save_names(data) {
		var send_data = angular.copy(data);
		send_data.root_id = id;
		if(data.hasOwnProperty('id')) {
			$http.post(apiBaseUrl + '/conference/put_names', send_data).
			success(function(res, status, headers, config) {
				var i = find_names(data.id);
				if (i > -1) $scope.names[i] = angular.copy(data);
				$scope.toast('success', 'Хурлын нэршил засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Хурлын нэршил засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/conference/post_names', send_data).
			success(function(res, status, headers, config) {
				data.id = res.id;
				$scope.names.push(data);
				$scope.toast('success', 'Хурлын нэршил бүртгэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Хурлын нэршил бүртгэх', res[0]);
			});
		}
	};
})

.controller('ManageConferenceInstanceCtrl', function ($scope, $modalInstance, data) {
	if (data && data.date) data.date = new Date(Date.parse(data.date));
	$scope.data = data;
	$scope.ok = function () {
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
})

.controller('ManageNamesInstanceCtrl', function ($scope, $modalInstance, data) {
	$scope.data = data;
	$scope.ok = function () {
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});