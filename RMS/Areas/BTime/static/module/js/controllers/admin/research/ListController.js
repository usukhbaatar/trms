'use strict';

BtimeModule.controller('AdminResearchListController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {	 
		init();
	});
	var page, status, type;
	function init() {
		$scope.loading = true;
		$scope.page = parseInt($state.params.page);
		$scope.status = $state.params.status
		$scope.type = $state.params.type;

		$scope.status_list = [{
			id: 'all',
			name: 'Бүгд'
		}, {
			id: 'pending',
			name: 'Хүлээгдэж байгаа'
		}, {
			id: 'accepted',
			name: 'Зөвшөөрөгдсөн'
		}, {
			id: 'canceled',
			name: 'Цуцлагдсан'
		}];

		for (var i = 0; i < $scope.status_list.length; i++)
			if ($state.params.status == $scope.status_list[i].id)
				$scope.status = $scope.status_list[i];

		$scope.research_types = [];

		$scope.refresh = function() {
			$state.go('btime.admin.researches', {type: $scope.research_type.id, page: $scope.page, status: $scope.status.id});
		}

		$http.get(apiBaseUrl + '/research/get_research_types').
			success(function(res, status, headers, config) {
				$scope.research_types.push({id: 0, name: 'Бүгд' });
				for (var i = 0; i < res.length; i++)
					$scope.research_types.push(res[i]);

				for (var i = 0; i < $scope.research_types.length; i++)
					if ($state.params.type == $scope.research_types[i].id)
						$scope.research_type = $scope.research_types[i];

				$http.get(apiBaseUrl + '/research/get?research_type='+$scope.research_type.id+'&status='+$scope.status.id+'&page='+$scope.page).
					success(function(res, status, headers, config) {
						$scope.dimensions=res.dimensions;
						$scope.researches = [];
						$scope.researches = res.researches;
						make_pager(res.cnt, res.page_size);
						if (res.researches) {
							for (var i = 0; i < res.researches.length; i++) {
								var temp = [];
								for (var j = 0; res.research_datas && j < res.research_datas.length; j++)
									if (res.researches[i].research_id == res.research_datas[j].research_id)
										temp.push(res.research_datas[j])
								res.researches[i].data = angular.copy(temp);

								temp = [];
								for (var j = 0; res.research_users && j < res.research_users.length; j++)
									if (res.researches[i].research_id == res.research_users[j].research_id)
										temp.push(res.research_users[j])
								res.researches[i].users = angular.copy(temp);

								temp = [];
								for (var j = 0; res.research_external_users && j < res.research_external_users.length; j++)
									if (res.researches[i].research_id == res.research_external_users[j].research_id)
										temp.push(res.research_external_users[j])
								res.researches[i].external_users = angular.copy(temp);
							}
						}
						$scope.loading = false;
						
						$scope.render_view = function(id) {
							var i = find(id);
							if (i == -1) return;

							var temp = $scope.researches[i].view;
							if (temp == null) return "";

							temp = temp.split("\\").join("<br>")
								.split("[[[").join("<strong><em>")
								.split("]]]").join("</em></strong>")
								.split("[[").join("<em>")
								.split("]]").join("</em>")
								.split("[").join("<strong>")
								.split("]").join("</strong>")
								.replace("***", render_users(i));

							var res = findID(temp);
							while (res != "") {
								var element_id = parseInt(res.substr(1, res.length - 2));
								var val = "";
								if (res[0] == '(') {
									var tmp = findElement(i, element_id);
									if (tmp != null) {
										val = tmp.value;
									}
								} else {
									var tmp = findElement(i, element_id);
									if (tmp != null) {
										val = tmp.label;
									}
								}

								temp = temp.split(res).join(val);
								res = findID(temp);
							}
							return temp;
						}
					}).
					error(function(data, status, headers, config) {
						$scope.loading = false;
						$scope.toast('error', 'Алдаа', data[0]);
					});
			}).
			error(function(data, status, headers, config) {
				$scope.loading = false;
				$scope.toast('error', 'Алдаа', data[0]);
			});
	}

	function find(id) {
		for (var i = 0; $scope.researches && i < $scope.researches.length; i++)
			if ($scope.researches[i].id == id)
				return i;
		return -1;
	}

	function render_users(i) {
		var ret = "";
		for (var j = 0; $scope.researches[i].users && j < $scope.researches[i].users.length; j++) {
			if ($scope.researches[i].users[j].degree == null) $scope.researches[i].users[j].degree = "";
			if (j == 0) {
				ret += '' + $scope.researches[i].users[j].degree + ' ' + ' <a ui-sref = "btime.user({id: ' + $scope.researches[i].users[j].id + '})">'+ $scope.researches[i].users[j].firstname + ' ' + $scope.researches[i].users[j].lastname + '</a>';
			} else {
				ret += ', ' + $scope.researches[i].users[j].degree + ' ' + ' <a ui-sref = "btime.user({id: ' + $scope.researches[i].users[j].id + '})">'+ $scope.researches[i].users[j].firstname + ' ' + $scope.researches[i].users[j].lastname + '</a>';
			}
		}

		for (var j = 0; $scope.researches[i].external_users && j < $scope.researches[i].external_users.length; j++) {
			ret += ', ' + $scope.researches[i].users[j].degree + " " + $scope.researches[i].external_users[j].firstname + " " + $scope.researches[i].external_users[j].lastname;
		}
		return ret;
	}

	function findID(str) {
		var target = false;
		var ret = "";
		var l = -1;
		for (var i = 0; i < str.length; i++) {
			if (str[i] == '(') {
				l = i; ret = "(";
			} else if (str[i] == ')' && str[l] == '(') {
				if (l < i - 1) {
					return ret + ")";
				} else { l = -1; ret = ""; }
			} else if (str[i] == '{') {
				l = i; ret = "{";
			} else if (str[i] == '}' && l > -1 && str[l] == '{') {
				if (l < i - 1) {
					return ret + "}";
				} else { l = -1; ret = ""; }
			} else if (l > -1) {
				if (str[i] < '0' || str[i] > '9') {
					l = -1;
					ret = "";
				} else { ret = ret + str[i]; }
			}
		}
		return "";
	}

	function findElement(i, element_id) {
		for (var j = 0; j < $scope.researches[i].data.length; j++) {
			if ($scope.researches[i].data[j].element_id == element_id) {
				var val = "";
				if ($scope.researches[i].data[j].static_type == 2) {
					val = $scope.researches[i].data[j].conference_name
				} else if ($scope.researches[i].data[j].static_type == 1) {
					val = $scope.researches[i].data[j].journal_name
				} else if ($scope.researches[i].data[j].type == 'file') {
					val = $scope.researches[i].data[j].file_name
				} else if ($scope.researches[i].data[j].type == 'select') {
					val = $scope.researches[i].data[j].selection
				} else {
					val = $scope.researches[i].data[j].value
				}
				return {label : $scope.researches[i].data[j].label, value: val};
			}
		}
		return null;
	}

		

	function make_pager(cnt, sz) {
		cnt = Math.floor((cnt - 1) / sz) + 1;
		$scope.pages = [];
		$scope.count = cnt;
		var left = cnt - 5;
		if (left < 1) left = 1;
		var right = left + 10;
		if (right > cnt) right = cnt;
		for (var i = left; i <= right; i++)
			$scope.pages.push(i);
	}
});
