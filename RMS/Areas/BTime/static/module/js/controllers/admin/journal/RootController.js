'use strict';

BtimeModule.controller('AdminJournalRootController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {
		init();
	});
	function init() {
		$scope.loading = true;
		$http.get(apiBaseUrl + '/journal/get_root').
			success(function(res, status, headers, config) {
				$scope.loading = false;
				$scope.dimensions = [{id: 0, name: 'Бүгд'}];
				for (var i = 0; res.dimensions && i < res.dimensions.length; i++)
					$scope.dimensions.push(res.dimensions[i]);
				$scope.dimension = $scope.dimensions[0];
				$scope.roots = res.res;
			}).
			error(function(res, status, headers, config) {
				$scope.loading = false;
				$scope.toast('error', 'Алдаа', data[0]);
			});
	}
	$scope.get_dimension_name = function(id) {
		for (var i = 0; $scope.dimensions && i < $scope.dimensions.length; i++)
			if (id == $scope.dimensions[i].id)
				return $scope.dimensions[i].name;
		return '';
	}

	function find(id) {
		for (var i = 0; i < $scope.roots.length; i++)
			if ($scope.roots[i].id == id)
				return i;
		return -1;
	}

	$scope.del = function(id) {
		$scope.confirm({title: "Сэтгүүл устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$http.delete(apiBaseUrl + '/journal/delete_root/' + id).
				success(function(res, status, headers, config) {
					var i = find(id);
					if (i > -1)
						$scope.roots.splice(i, 1);
					$scope.toast('success', 'Устгах', 'Устгах үйлдэл амжилттай боллоо!');
				}).
				error(function(res, status, headers, config) {
					$scope.toast('error', 'Устгах', res[0]);
				});
		});
	}

	$scope.add = function() {
		manage();
	}

	$scope.edit = function(id) {
		var i = find(id);
		manage(angular.copy($scope.roots[i]));
	}

	function manage(data) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageRootContent.html',
			controller: 'ManageRootInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				},
				dimensions: function() {
					return $scope.dimensions;
				}
			}
		});
		modalInstance.result.then(function (data) {
			save(data);
		}, function () {
			
		});
	}

	function save(data) {
		if(data.hasOwnProperty('id')) {
			$http.post(apiBaseUrl + '/journal/put_root', data).
			success(function(res, status, headers, config) {
				var i = find(data.id);
				if (i > -1) $scope.roots[i] = angular.copy(data);
				$scope.toast('success', 'Сэтгүүл засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Сэтгүүл засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/journal/post_root', data).
			success(function(res, status, headers, config) {
				data.id = res.id;
				$scope.roots.push(data);
				$scope.toast('success', 'Сэтгүүл бүртгэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Сэтгүүл бүртгэх', res[0]);
			});
		}
	};
})

.controller('ManageRootInstanceCtrl', function ($scope, $modalInstance, data, dimensions) {
	$scope.data = data;
	$scope.dimensions = dimensions
	$scope.dimension = dimensions[1];
	if (data)
		for (var i = 0 ; i < dimensions.length; i++)
			if (dimensions[i].id == data.dimension_id)
				$scope.dimension = dimensions[i];

	$scope.ok = function () {
		$scope.data.dimension_id = $scope.dimension.id;
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});