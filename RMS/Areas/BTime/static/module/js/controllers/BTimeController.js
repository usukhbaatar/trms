﻿'use strict';

BtimeModule

.controller('BTimeController', function ($rootScope, $scope, $http, $timeout, toaster, $modal) {
	$scope.can = function() {
		return true;
	}

	$scope.canThis = function() {
		return true;
	}

	$scope.$on('$viewContentLoaded', function () {});
	$scope.toast = function(type, title, body) {
		toaster.pop(type, title, body);
	}

	$http.get(apiBaseUrl + '/btimeuser/get').
		success(function(data, status, headers, config) {
			$scope.user = {
				id: data[0],
				username: data[1],
				firstname: data[2],
				lastname: data[3],
				role: data[4]
			};
		}).
		error(function(data, status, headers, config) {
			$scope.toast('error', '', data);
		});

	$scope.confirm = function (data, callback, size) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'confirmContent.html',
			controller: 'ConfirmInstanceCtrl',
			size: size,
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		modalInstance.result.then(function () {
			callback()
		}, function () {
			
		});
	};

	Date.prototype.getMonthFormatted = function() {
		var month = this.getMonth() + 1;
		return month < 10 ? '0' + month : '' + month;
	}

	Date.prototype.getDayFormatted = function() {
		var day = this.getDate();
		return day < 10 ? '0' + day : '' + day;
	}

	$scope.parseDateFormat = function(date) {
		return date.getFullYear() + "-" + date.getMonthFormatted() + "-" + date.getDayFormatted();	
	}

	$scope.parseDate1 = function(str) {
		var date = new Date(Date.parse(str));
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var semister = 0;
		if (month < 9) year--;
		if (month == 1 || month > 8) semister = 1;
		else semister = 2;
		return {year: year, semister: semister}
	}
})

.controller('ConfirmInstanceCtrl', function ($scope, $modalInstance, data) {
	$scope.data = data;
	$scope.ok = function () {
		$modalInstance.close();
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});