'use strict';

BtimeModule.controller('GroupController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {	 
		init();
	});
	var id;
	function init() {
		id = $state.params.id;
		$scope.breads = [{url: serverUrl + '#/btime/group/1', name: 'Нүүр'}];
		$http.get(apiBaseUrl + '/group/get/' + id).
			success(function(res, status, headers, config) {
				$scope.data = res.data;
				$scope.child = res.child;
				$scope.note = {body: res.data.description}
				makeBreads(res.breads);
				$scope.status = res.status;
				if ($scope.status.has_form == 1) {
					$http.get(apiBaseUrl + '/form/show/' + id).
						success(function(res, status, headers, config) {
							$scope.form = res.form;
							$scope.selections = res.selections;
							$scope.research_types = res.research_types;
							}).
						error(function(data, status, headers, config) {
							$scope.toast('error', 'Алдаа', 'Формыг ачаалж чадсангүй!');
						});
				}
			}).
			error(function(data, status, headers, config) {
				$scope.toast('error', 'Алдаа', 'Өгөгдлийг ачаалж чадсангүй!');
			});
		$scope.users = [];
		$scope.external_users = [];
	}

	$scope.removeUser = function(id) {
		for (var i = 0; i < $scope.users.length; i++) {
			if ($scope.users[i].id == id)
				$scope.users.splice(i, 1);
		}
	}

	$scope.removeExternalUser = function(id) {
		for (var i = 0; i < $scope.users.length; i++) {
			if ($scope.users[i].id == id)
				$scope.users.splice(i, 1);
		}
	}

	$scope.selectUser = function(selected) {
		if (selected && selected.originalObject) {
			var exist = false;
			for (var i = 0; i < $scope.users.length; i++)
				if ($scope.users[i].id == selected.originalObject.id)
					exist = true;
			if (!exist)
				$scope.users.push(selected.originalObject);
			else
				$scope.toast('warning', 'Хамтрагч нэмэх', 'Аль хэдийнээ нэмэгдсэн байна!');
		}
	}

	$scope.selectJournal = function(selected) {
		if (selected && selected.originalObject) {
			$scope.journal_id = selected.originalObject.id;
			for (var i = 0; i < $scope.form.elements.length; i++) {
				if ($scope.form.elements[i].static_type == 1) {
					$scope.form.elements[i].errors = [];
				}
			}
		} else {
			$scope.journal_id = undefined;
		}
	}

	$scope.selectConference = function(selected) {
		if (selected.originalObject) {
			$scope.conference_id = selected.originalObject.id;
			for (var i = 0; i < $scope.form.elements.length; i++) {
				if ($scope.form.elements[i].static_type == 2) {
					$scope.form.elements[i].errors = [];
				}
			}
		} else {
			$scope.conference_id = undefined;
		}
	}

	$scope.focusState = 'None';

	$scope.addForm = function() {
		$state.go('btime.form_new', {id: id});
	}

	$scope.editForm = function() {
		$state.go('btime.form_edit', {id: id});
	}

	function isFloat(n){
	    return n === Number(n) && n % 1 !== 0;
	}

	$scope.changed = function(id) {
		for (var i = 0; i < $scope.form.elements.length; i++) {
			if ($scope.form.elements[i].id == id) {
				var errors = [];
				var valid = true;
				var value = $scope.form.elements[i].value;
				for (var j = 0; j < $scope.form.elements[i].validators.length; j++) {
					if($scope.form.elements[i].validators[j].name == 'required') {
						if (!value) {
							valid = false;
							errors.push('Энэ талбар хоосон байж болохгүй!');
							break;
						}
					} else if ($scope.form.elements[i].validators[j].name == 'float') {
						if (!isFloat(value)) {
							valid = false;
							errors.push('Тоо оруулна уу!');
						}
					}
				}

				if (!valid) {
					$scope.form.elements[i].errors = angular.copy(errors);
				} else {
					$scope.form.elements[i].errors = [];
				}
			}
		}
	}

	$scope.submit = function() {
		var data = [];
		var is_valid = true;

		for (var i = 0; i < $scope.form.elements.length; i++) {
			var valid = true;
			var value = null;
			var id = $scope.form.elements[i].id;

			if ($scope.form.elements[i].static_type == 1) {
				value = $scope.journal_id;
			} else if ($scope.form.elements[i].static_type == 2) {
				value = $scope.conference_id;
			} else {
				value = document.getElementById('elem_' + $scope.form.elements[i].id).value;
			}

			var errors = [];
			for (var j = 0; j < $scope.form.elements[i].validators.length; j++) {
				if($scope.form.elements[i].validators[j].name == 'required') {
					if (!value) {
						is_valid = false;
						valid = false;
						errors.push('Энэ талбар хоосон байж болохгүй!');
						break;
					}
				} else if ($scope.form.elements[i].validators[j].name == 'float') {
					if (!isFloat(value)) {
						is_valid = false;
						valid = false;
						errors.push('Тоо оруулна уу!');
					}
				}
			}

			if (!valid) {
				$scope.form.elements[i].errors = angular.copy(errors);
			} else {
				data.push({element_id: id, value: value});
				$scope.form.elements[i].errors = [];
			}
		}
		
		if (is_valid) {
			console.log($scope.form);
			var users = [];
			for (var i = 0; i < $scope.users.length; i++)
				users.push($scope.users[i].id);

			var datas = {
				items: data,
				users: users,
				external_users: $scope.external_users
			};

			$http.post(apiBaseUrl + '/research/post?form_id=' + $scope.form.id + "&research_type_id=" + document.getElementById('research_type').value, datas).
				success(function(res, status, headers, config) {
					alert('1');
				}).
				error(function(errors, status, headers, config) {
					$scope.toast('error', 'Алдаа', 'Мэдээллээ дахин шалгана уу!');
					for (var j = 0; j < $scope.form.elements.length; j++) {
						$scope.form.elements[j].errors = [];	
					}
					for (var i = 0; i < errors.length; i++) {
						if (errors[i].element_id == 0) {
							$scope.toast('error', 'Алдаа', errors[i].message);
						} else if (errors[i].element_id == -1) {
							$scope.research_type_error = [errors[i].message];
						} else {
							for (var j = 0; j < $scope.form.elements.length; j++) {
								if (errors[i].element_id == $scope.form.elements[j].id)
									$scope.form.elements[j].errors.push(errors[i].message);
							}
						}
					}
				});
		} else {
			console.log(data);
		}
	}

	function save(data) {
		if(data.hasOwnProperty('id')) {
			$http.post(apiBaseUrl + '/group/put', data).
			success(function(res, status, headers, config) {
				$scope.data = angular.copy(data);
				$scope.breads[$scope.breads.length - 1].name = data.number;
				$scope.note.body = data.description;
				$scope.toast('success', 'Бүлэг засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Бүлэг засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/group/post', data).
			success(function(res, status, headers, config) {
				data.id = res.id;
				$scope.child.push(data);
				$scope.child.sort(function(a, b) { return a.number > b.number});
				$scope.toast('success', 'Бүлэг нэмэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Бүлэг нэмэх', res[0]);
			});
		}
	};

	$scope.addExternalUser = function() {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'addExternalUserContent.html',
			controller: 'AddExternalUserInstanceCtrl',
			resolve: {
				data: function () {
					return {degree: 0};
				}
			}
		});
		modalInstance.result.then(function (data) {
			$scope.external_users.push(data);
		}, function () {
			
		});
	}

	function manage(data) {
		if (data == null)
			data = {parent_id: id};
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageGroupContent.html',
			controller: 'ManageGroupInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				}
			}
		});
		modalInstance.result.then(function (data) {
			save(data);
		}, function () {
			
		});
	}

	$scope.add = function() {
		manage();
	}

	$scope.edit = function() {
		manage(angular.copy($scope.data));
	}

	$scope.del = function() {
		if ($scope.status.has_form == 1) {
			$scope.confirm({title: "Форм устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
				$http.delete(apiBaseUrl + '/form/delete/' + id).
					success(function(res, status, headers, config) {
						$scope.status.has_form = 0;
						$scope.toast('success', 'Устгах', 'Устгах үйлдэл амжилттай боллоо!');
					}).
					error(function(res, status, headers, config) {
						$scope.toast('error', 'Устгах', res[0]);
					});
			});
		}
		else {
			$scope.confirm({title: "Бүлэг устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
				$http.delete(apiBaseUrl + '/group/delete/' + id).
					success(function(res, status, headers, config) {
						$scope.toast('success', 'Устгах', 'Устгах үйлдэл амжилттай боллоо!');
						$state.go('btime.group', {id: $scope.data.parent_id});
					}).
					error(function(res, status, headers, config) {
						$scope.toast('error', 'Устгах', res[0]);
					});
			});
		}
	}

	function makeBreads(raw) {
		$scope.breads = [];
		var url = serverUrl + '#/btime/group/';
		if (raw) {
			for (var i = raw.length - 1; i >= 0; i--) {
				$scope.breads.push({url: url + raw[i].id, name: raw[i].name});
			}
		}
	}
})

.controller('ManageGroupInstanceCtrl', function ($scope, $modalInstance, data) {
	$scope.data = data;
	$scope.ok = function () {
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
})

.controller('AddExternalUserInstanceCtrl', function ($scope, $modalInstance, data) {
	$scope.data = data;
	$scope.ok = function () {
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});
