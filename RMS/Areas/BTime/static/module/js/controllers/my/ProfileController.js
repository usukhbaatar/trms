'use strict';

BtimeModule.controller('MyProfileController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {	 
		init();
	});
	$scope.list = [];
	function reload() {
		$scope.cnt11 = 0;
		$scope.cnt12 = 0;
		$scope.cnt13 = 0;
		$scope.cnt14 = 0;

		for (var i = 0; i <  $scope.researches.length; i++) {
			var research = $scope.researches[i];
			if (research['status'] == 'accepted') { $scope.cnt11++; $scope.subtab = 11; }
			if (research['status'] == 'confirmed') $scope.cnt12++;
			if (research['status'] == 'pending') $scope.cnt13++;
			if (research['status'] == 'canceled') $scope.cnt14++;
		}
	}

	function find_idndex(y, s) {
		for (var i = 0; i < $scope.list.length; i++) {
			if ($scope.list[i].year == y && $scope.list[i].semister == s)
				return i;
		}
		return -1;
	}


	var current = {};
	function init() {
		$scope.tab = 1;
		$scope.subtab = 12;
		
		

		$http.get(apiBaseUrl + '/profile/get').
			success(function(res, status, headers, config) {
				current = res.current;
				console.log(res);
				res.info.school = res.school;
				res.info.faculty = res.faculty;
				$scope.info = res.info;
				$scope.info.credit_a = 21;
				$scope.info.credit_a1 = 18;
				$scope.info.credit_c = 6;
				$scope.researches = res.researches;
				$scope.degree_history = res.degree_history;
				$scope.rank_history = res.rank_history;
				$scope.dimensions = res.dimensions;

				$scope.dimension_chart_data = [];
				for (var i = 0; i < res.dimensions.length; i++) {
					var sum = 0;
					for (var j = -1; $scope.researches && j < $scope.researches.length; j++) {
						if (j >= 0 && ($scope.researches[j].status == 'accepted' || $scope.researches[j].status == 'confirmed') && $scope.researches[j].dimension_id == res.dimensions[i].id)
							sum += $scope.researches[j].credit;
						if (j == $scope.researches.length - 1) {
							$scope.dimension_chart_data.push({label: res.dimensions[i].name, value: sum});
						}
					}
				}
				reload();
				$scope.getCredit = function() {
					var ret = 0;
					for (var i = 0; i < $scope.researches.length; i++) {
						if ($scope.researches[i].year == current.year && $scope.researches[i].semister == current.semister)
							ret += $scope.researches[i].credit;
					}
					return ret;
				}

				$scope.getMax = function() {
					if ($scope.rank_history)
						return $scope.rank_history[0].rank.credit;
					return 0;
				}

				function getMaxCredit(y, s) {
					for (var i = 0; i < $scope.rank_history.length; i++)
						if (y > $scope.rank_history[i].year || (y == $scope.rank_history[i].year && $scope.rank_history[i].semister >= s))
							return $scope.rank_history[i].rank.credit;
					return 0;
				}

				if (res.researches) {
					for (var i = 0; i < res.researches.length; i++) {
						var temp = [];
						for (var j = 0; res.research_datas && j < res.research_datas.length; j++)
							if (res.researches[i].research_id == res.research_datas[j].research_id)
								temp.push(res.research_datas[j])
						res.researches[i].data = angular.copy(temp);

						temp = [];
						for (var j = 0; res.research_users && j < res.research_users.length; j++)
							if (res.researches[i].research_id == res.research_users[j].research_id)
								temp.push(res.research_users[j])
						res.researches[i].users = angular.copy(temp);

						temp = [];
						for (var j = 0; res.research_external_users && j < res.research_external_users.length; j++)
							if (res.researches[i].research_id == res.research_external_users[j].research_id)
								temp.push(res.research_external_users[j])
						res.researches[i].external_users = angular.copy(temp);
					}
				}

				$scope.render_view = function(id) {
					var i = find(id);
					if (i == -1) return;

					var temp = $scope.researches[i].view;
					if (temp == null) return "";

					temp = temp.split("\\").join("<br>")
						.split("[[[").join("<strong><em>")
						.split("]]]").join("</em></strong>")
						.split("[[").join("<em>")
						.split("]]").join("</em>")
						.split("[").join("<strong>")
						.split("]").join("</strong>")
						.replace("***", render_users(i));

					var res = findID(temp);
					while (res != "") {
						var element_id = parseInt(res.substr(1, res.length - 2));
						var val = "";
						if (res[0] == '(') {
							var tmp = findElement(i, element_id);
							if (tmp != null) {
								val = tmp.value;
							}
						} else {
							var tmp = findElement(i, element_id);
							if (tmp != null) {
								val = tmp.label;
							}
						}

						temp = temp.split(res).join(val);
						res = findID(temp);
					}
					return temp;
				}

				for (var i = 0; i < $scope.researches.length; i++) {
					if ($scope.researches[i].status == 'confirmed') {
						var j = find_idndex($scope.researches[i].year, $scope.researches[i].semister);
						if (j == -1) {
							$scope.list.push({year: $scope.researches[i].year, semister: $scope.researches[i].semister, credit_b: $scope.researches[i].credit, credit_b1: getMaxCredit($scope.researches[i].year, $scope.researches[i].semister)});
						} else {
							$scope.list[j].credit_b += $scope.researches[i].credit;
						}
					}
				}
				
				//$scope.degree_history.sort(function(a, b) { return a.date > b.date});
				//$scope.rank_history.sort(function(a, b) { return a.year > b.year || (a.year == b.year && a.semister > b.semister)});

			}).
			error(function(data, status, headers, config) {
				$scope.toast('error', 'Алдаа', 'Өгөгдлийг ачаалж чадсангүй!');
			});
	}

	function find(id) {
			for (var i = 0; $scope.researches && i < $scope.researches.length; i++)
				if ($scope.researches[i].id == id)
					return i;
			return -1;
		}

	function render_users(i) {
		var ret = "";
		for (var j = 0; $scope.researches[i].users && j < $scope.researches[i].users.length; j++) {
			if ($scope.researches[i].users[j].degree == null) $scope.researches[i].users[j].degree = "";
			if (j == 0) {
				ret += $scope.researches[i].users[j].degree + ' ' + ' <a href="#/btime/user/' + $scope.researches[i].users[j].id + '">'+ $scope.researches[i].users[j].firstname + ' ' + $scope.researches[i].users[j].lastname + '</a>';
			} else {
				ret += ', ' + $scope.researches[i].users[j].degree + ' ' + ' <a href="#/btime/user/' + $scope.researches[i].users[j].id + '">'+ $scope.researches[i].users[j].firstname + ' ' + $scope.researches[i].users[j].lastname + '</a>';
			}
		}

		for (var j = 0; $scope.researches[i].external_users && j < $scope.researches[i].external_users.length; j++) {
			ret += ', ' + $scope.researches[i].users[j].degree + " " + $scope.researches[i].external_users[j].firstname + " " + $scope.researches[i].external_users[j].lastname;
		}
		return ret;
	}

	function findID(str) {
		var target = false;
		var ret = "";
		var l = -1;
		for (var i = 0; i < str.length; i++) {
			if (str[i] == '(') {
				l = i; ret = "(";
			} else if (str[i] == ')' && str[l] == '(') {
				if (l < i - 1) {
					return ret + ")";
				} else { l = -1; ret = ""; }
			} else if (str[i] == '{') {
				l = i; ret = "{";
			} else if (str[i] == '}' && l > -1 && str[l] == '{') {
				if (l < i - 1) {
					return ret + "}";
				} else { l = -1; ret = ""; }
			} else if (l > -1) {
				if (str[i] < '0' || str[i] > '9') {
					l = -1;
					ret = "";
				} else { ret = ret + str[i]; }
			}
		}
		return "";
	}

	function findElement(i, element_id) {
		for (var j = 0; j < $scope.researches[i].data.length; j++) {
			if ($scope.researches[i].data[j].element_id == element_id) {
				var val = "";
				if ($scope.researches[i].data[j].static_type == 2) {
					val = $scope.researches[i].data[j].conference_name
				} else if ($scope.researches[i].data[j].static_type == 1) {
					val = $scope.researches[i].data[j].journal_name
				} else if ($scope.researches[i].data[j].type == 'file') {
					val = $scope.researches[i].data[j].file_name
				} else if ($scope.researches[i].data[j].type == 'select') {
					val = $scope.researches[i].data[j].selection
				} else {
					val = $scope.researches[i].data[j].value
				}
				return {label : $scope.researches[i].data[j].label, value: val};
			}
		}
		return null;
	}
});