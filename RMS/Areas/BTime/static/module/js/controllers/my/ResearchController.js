'use strict';

BtimeModule.controller('MyResearchController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {	 
		init();
	});
	var id;
	function init() {
		$scope.loading = true;
		id = $state.params.id;
		$http.get(apiBaseUrl + '/research/get_research_types').
			success(function(res, status, headers, config) {
				$http.get(apiBaseUrl + '/research/get_my?id=' + id).
					success(function(res, status, headers, config) {
						$scope.dimensions = res.dimensions;
						$scope.research = res.research[0];
						var temp = [];
						for (var j = 0; res.research_datas && j < res.research_datas.length; j++)
							temp.push(res.research_datas[j])
						$scope.research.data = angular.copy(temp);

						temp = [];
						for (var j = 0; res.research_users && j < res.research_users.length; j++)
							temp.push(res.research_users[j])
						$scope.research.users = angular.copy(temp);

						temp = [];
						for (var j = 0; res.research_external_users && j < res.research_external_users.length; j++)
							temp.push(res.research_external_users[j])
						$scope.research.external_users = angular.copy(temp);

						temp = [];
						for (var j = 0; res.comments && j < res.comments.length; j++)
							temp.push(res.comments[j])
						$scope.research.comments = angular.copy(temp);

						$scope.loading = false;
						console.log($scope.research);

						$scope.render_view = function(id) {
							var temp = $scope.research.view;
							if (temp == null) return "";

							temp = temp.split("\\").join("<br>")
								.split("[[[").join("<strong><em>")
								.split("]]]").join("</em></strong>")
								.split("[[").join("<em>")
								.split("]]").join("</em>")
								.split("[").join("<strong>")
								.split("]").join("</strong>")
								.replace("***", render_users());

							var res = findID(temp);
							while (res != "") {
								var element_id = parseInt(res.substr(1, res.length - 2));
								var val = "";
								if (res[0] == '(') {
									var tmp = findElement(element_id);
									if (tmp != null) {
										val = tmp.value;
									}
								} else {
									var tmp = findElement(element_id);
									if (tmp != null) {
										val = tmp.label;
									}
								}

								temp = temp.split(res).join(val);
								res = findID(temp);
							}
							return temp;
						}
					}).
					error(function(data, status, headers, config) {
						$scope.loading = false;
						$scope.toast('error', 'Алдаа', data[0]);
					});
			}).
			error(function(data, status, headers, config) {
				$scope.loading = false;
				$scope.toast('error', 'Алдаа', data[0]);
			});
	}

	

	function render_users() {
		var ret = "";
		for (var j = 0; $scope.research.users && j < $scope.research.users.length; j++) {
			if ($scope.research.users[j].degree == null) $scope.research.users[j].degree = "";
			if (j == 0) {
				ret += $scope.research.users[j].degree + ' ' + ' <a href="#/btime/user/' + $scope.research.users[j].id + '">'+ $scope.research.users[j].firstname + ' ' + $scope.research.users[j].lastname + '</a>';
			} else {
				ret += ', ' + $scope.research.users[j].degree + ' ' + ' <a href="#/btime/user/' + $scope.research.users[j].id + '">'+ $scope.research.users[j].firstname + ' ' + $scope.research.users[j].lastname + '</a>';
			}
		}

		for (var j = 0; $scope.research.external_users && j < $scope.research.external_users.length; j++) {
			ret += ', ' + $scope.research.users[j].degree + " " + $scope.research.external_users[j].firstname + " " + $scope.research.external_users[j].lastname;
		}
		return ret;
	}

	function findID(str) {
		var target = false;
		var ret = "";
		var l = -1;
		for (var i = 0; i < str.length; i++) {
			if (str[i] == '(') {
				l = i; ret = "(";
			} else if (str[i] == ')' && str[l] == '(') {
				if (l < i - 1) {
					return ret + ")";
				} else { l = -1; ret = ""; }
			} else if (str[i] == '{') {
				l = i; ret = "{";
			} else if (str[i] == '}' && l > -1 && str[l] == '{') {
				if (l < i - 1) {
					return ret + "}";
				} else { l = -1; ret = ""; }
			} else if (l > -1) {
				if (str[i] < '0' || str[i] > '9') {
					l = -1;
					ret = "";
				} else { ret = ret + str[i]; }
			}
		}
		return "";
	}

	function findElement(element_id) {
		for (var j = 0; j < $scope.research.data.length; j++) {
			if ($scope.research.data[j].element_id == element_id) {
				var val = "";
				if ($scope.research.data[j].static_type == 2) {
					val = $scope.research.data[j].conference_name
				} else if ($scope.research.data[j].static_type == 1) {
					val = $scope.research.data[j].journal_name
				} else if ($scope.research.data[j].type == 'file') {
					val = $scope.research.data[j].file_name
				} else if ($scope.research.data[j].type == 'select') {
					val = $scope.research.data[j].selection
				} else {
					val = $scope.research.data[j].value
				}
				return {label : $scope.research.data[j].label, value: val};
			}
		}
		return null;
	}

	$scope.getValue = function(e) {
		var val = "";
		if (e.static_type == 2) {
			val = e.conference_name
		} else if (e.static_type == 1) {
			val = e.journal_name
		} else if (e.type == 'file') {
			val = e.file_name
		} else if (e.type == 'select') {
			val = e.selection
		} else {
			val = e.value
		}
		return val;
	}

	$scope.commenting = false;

	$scope.commentSubmit = function() {
		$scope.commenting = true;
		$http.post(apiBaseUrl + '/comment/post?research_id='+id+'&comment='+$scope.comment).
			success(function(res, status, headers, config) {
				$scope.commenting = false;
				$scope.toast('success', 'Сэтгэгдэл', 'Амжилттай хадгаллаа');
				$scope.research.comments.push({id: res.id, date: res.date, comment: $scope.comment, user_id: res.user_id});
				$scope.comment = "";
			}).
			error(function(error, status, headers, config) {
				$scope.commenting = false;
				$scope.toast('error', 'Алдаа', error);
			});
	}

	$scope.deleteComment = function(comment_id) {
		$scope.confirm({title: "Сэтгэгдэл устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$scope.commenting = true;
			$http.delete(apiBaseUrl + '/comment/delete?id='+comment_id).
				success(function(res, status, headers, config) {
					$scope.commenting = false;
					$scope.toast('success', 'Сэтгэгдэл', 'Амжилттай устгалаа');
					for (var i = 0; i < $scope.research.comments.length; i++) {
						if ($scope.research.comments[i].id == comment_id) {
							$scope.research.comments.splice(i, 1);
							break;
						}
					}
				}).
				error(function(error, status, headers, config) {
					$scope.commenting = false;
					$scope.toast('error', 'Алдаа', error);
				});
		});
		
	}

	$scope.getUserPicture = function(user_id) {
		for (var i = 0; i < $scope.research.users.length; i++) {
			if ($scope.research.users[i].id == user_id) {
				if ($scope.research.users[i].picture == null)
					return 1;
			}
		}
		return -1;
	}

	$scope.confirm = function() {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'ConfirmResearchContent.html',
			controller: 'ConfirmResearchCtrl',
			resolve: {
				send_date: function () {
					return $scope.research.send_date;
				}
			}
		});
		modalInstance.result.then(function (date) {
			$http.post(apiBaseUrl + '/research/confirm?id='+id+'&year=' + date.year + "&semister=" + date.semister).
				success(function(res, status, headers, config) {
					$scope.toast('success', 'Судалгааны ажил', 'Амжилттай хадгаллаа');
					$scope.research.status = 'confirmed';
					$scope.research.year = date.year;
					$scope.research.semister = date.semister;
				}).
				error(function(error, status, headers, config) {
					$scope.toast('error', 'Алдаа', error);
				});
		}, function () {
			
		});
	}


})

.controller('ConfirmResearchCtrl', function ($scope, $modalInstance, send_date) {

	Date.prototype.getMonthFormatted = function() {
		var month = this.getMonth() + 1;
		return month < 10 ? '0' + month : '' + month;
	}

	Date.prototype.getDayFormatted = function() {
		var day = this.getDate();
		return day < 10 ? '0' + day : '' + day;
	}

	$scope.parseDateFormat = function(date) {
		return date.getFullYear() + "-" + date.getMonthFormatted() + "-" + date.getDayFormatted();	
	}

	function parseDate(str) {
		var date = new Date(Date.parse(str));
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var semister = 0;
		if (month < 9) year--;
		if (month == 1 || month > 8) semister = 1;
		else semister = 2;
		return {year: year, semister: semister}
	}

	var start_date = parseDate(send_date);
	
	$scope.list = [];
	for (var i = 0; i < 4; i++) {
		var str = start_date.year + "-" + (start_date.year + 1) + ' оны хичээлийн жил, ' + start_date.semister + '-р улирал';
		$scope.list.push({id: i, year: start_date.year, semister: start_date.semister, name: str});
		if (start_date.semister == 1) start_date.semister++;
		else {
			start_date.year++; start_date.semister--;
		}
	}

	$scope.date = $scope.list[0];

	$scope.ok = function () {
		$modalInstance.close({year: $scope.date.year, semister: $scope.date.semister});
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});
