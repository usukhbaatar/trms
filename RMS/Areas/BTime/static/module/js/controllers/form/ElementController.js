'use strict';

BtimeModule.controller('FormElementController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {   
		init();
	});

	var id;

	function init() {
		id = $state.params.id;
		$scope.id = id;
		$http.get(apiBaseUrl + '/form/get/' + id).
			success(function(res, status, headers, config) {
				$scope.form = res.form;
				$scope.selections = res.selections;
			}).
			error(function(data, status, headers, config) {
			});
	}

	$scope.typeConvert = function(type) {
		if (type == 'text') return 'Текст';
		if (type == 'textarea') return 'Текст (том)';
		if (type == 'search') return 'Хайх';
		if (type == 'number') return 'Тоо';
		if (type == 'file') return 'Файл';
		if (type == 'select') return 'Сонгох';
		if (type == 'date') return 'Огноо';
	}

	function findElement(id) {
		for (var i = 0; i < $scope.form.elements.length; i++)
			if ($scope.form.elements[i].id == id)
				return i;
		return -1;
	}

	function save(data) {
		data.static_type = 0;
		var has_required = data.required;
		if(data.hasOwnProperty('required')){
			delete data.required;
		}

		if(data.hasOwnProperty('id')) {
			if(data.hasOwnProperty('validators')){
				delete data.validators;
			}
			$http.post(apiBaseUrl + '/formelement/put?form_id=' + id + "&has_required=" + has_required, data).
			success(function(res, status, headers, config) {
				var i = findElement(data.id);
				if (i > -1) {
					$scope.form.elements[i] = angular.copy(res.data);
					console.log(res.data);
				}
				$scope.toast('success', 'Элемент засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Элемент засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/formelement/post?form_id=' + id, data).
			success(function(res, status, headers, config) {
				$scope.form.elements.push(res.data);
				$scope.toast('success', 'Элемент нэмэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Элемент нэмэх', res[0]);
			});
		}
	};

	function manage(data) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageElementContent.html',
			controller: 'ManageElementInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				},
				selections: function() {
					return $scope.selections;
				}
			}
		});
		modalInstance.result.then(function (data) {
			save(data);
		}, function () {
			
		});
	}

	$scope.add = function() {
		manage();
	}

	$scope.edit = function(id) {
		var i = findElement(id);
		if (i > -1) {
			var temp = $scope.form.elements[i];
			var has_required = false;
			if (temp.validators) {
				for (var i = 0; i < temp.validators.length; i++) {
					if (temp.validators[i].name == 'required')
						has_required = true;
				}
			}
			temp.required = has_required;
			manage(angular.copy(temp));
		}
	}

	$scope.del = function(id) {
		$scope.confirm({title: "Элемент устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$http.delete(apiBaseUrl + '/formelement/delete?id=' + id).
				success(function(res, status, headers, config) {
					var i = findElement(id);
					if (i > -1) {
						$scope.form.elements.splice(i, 1);
					}
					$scope.toast('success', 'Элемент устгах', 'Устгах үйлдэл амжилттай боллоо!');
				}).
				error(function(res, status, headers, config) {
					$scope.toast('error', 'Элемент устгах', res[0]);
				});
		});
	}
})

.controller('ManageElementInstanceCtrl', function ($scope, $modalInstance, data, selections) {
	if (data == null) data = {required: false};
	$scope.data = data;
	$scope.selections = selections;

	$scope.ok = function () {
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});