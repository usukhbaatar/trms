'use strict';

BtimeModule.controller('FormRuleController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {   
		init();
	});

	var id;

	function init() {
		id = $state.params.id;
		$scope.id = id;
		$http.get(apiBaseUrl + '/formrule/get/' + id).
			success(function(res, status, headers, config) {
				$scope.elements = res.elements;
				$scope.A = res.authors;
				$scope.M = res.multipliers;
				$scope.D = res.dividers;
			}).
			error(function(data, status, headers, config) {
			});
	}

	// AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

	function findA(id) {
		for (var i = 0; i < $scope.A.length; i++)
			if ($scope.A[i].id == id)
				return i;
		return -1;
	}

	function saveA(data) {
		if(data.hasOwnProperty('id')) {
			$http.post(apiBaseUrl + '/formrule/edit_author?id=' + id, data).
			success(function(res, status, headers, config) {
				var i = findA(res.id);
				if (i > -1) {
					$scope.A[i] = angular.copy(res.data);
					console.log(res.data);
				}
				$scope.toast('success', 'Дүрэм засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Дүрэм засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/formrule/add_author?id=' + id, data).
			success(function(res, status, headers, config) {
				$scope.A.push(res.data);
				$scope.toast('success', 'Дүрэм нэмэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Дүрэм нэмэх', res[0]);
			});
		}
	};

	function manageA(data) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageRuleAContent.html',
			controller: 'ManageRuleInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				},
				elements: function() {
					return $scope.elements;
				}
			}
		});
		modalInstance.result.then(function (data) {
			saveA(data);
		}, function () {
			
		});
	}

	$scope.addA = function() {
		manageA();
	}

	$scope.editA = function(id) {
		var i = findA(id);
		if (i > -1) {
			manageA(angular.copy($scope.A[i]));
		}
	}

	$scope.delA = function(id) {
		$scope.confirm({title: "Дүрэм устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$http.delete(apiBaseUrl + '/formrule/delete_author?id=' + id).
				success(function(res, status, headers, config) {
					var i = findA(id);
					if (i > -1) {
						$scope.A.splice(i, 1);
					}
					$scope.toast('success', 'Дүрэм устгах', 'Устгах үйлдэл амжилттай боллоо!');
				}).
				error(function(res, status, headers, config) {
					$scope.toast('error', 'Дүрэм устгах', res[0]);
				});
		});
	}

	// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

	$scope.findLabel = function(id) {
		for (var i = 0; i < $scope.elements.length; i++) {
			if ($scope.elements[i].id == id)
				return $scope.elements[i].label;
		}
		return 'Ийм элемент олдсонгүй';
	}

	function findM(id) {
		for (var i = 0; i < $scope.M.length; i++)
			if ($scope.M[i].id == id)
				return i;
		return -1;
	}

	function saveM(data) {
		if(data.hasOwnProperty('id')) {
			$http.post(apiBaseUrl + '/formrule/edit_multiplier?id=' + id, data).
			success(function(res, status, headers, config) {
				var i = findM(res.id);
				if (i > -1) {
					$scope.M[i] = angular.copy(res.data);
					console.log(res.data);
				}
				$scope.toast('success', 'Дүрэм засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Дүрэм засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/formrule/add_multiplier?id=' + id, data).
			success(function(res, status, headers, config) {
				$scope.M.push(res.data);
				$scope.toast('success', 'Дүрэм нэмэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Дүрэм нэмэх', res[0]);
			});
		}
	};

	function manageM(data) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageRuleMContent.html',
			controller: 'ManageRuleInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				},
				elements: function() {
					return $scope.elements;
				}
			}
		});
		modalInstance.result.then(function (data) {
			saveM(data);
		}, function () {
			
		});
	}

	$scope.addM = function() {
		manageM();
	}

	$scope.editM = function(id) {
		var i = findM(id);
		if (i > -1) {
			manageM(angular.copy($scope.M[i]));
		}
	}

	$scope.delM = function(id) {
		$scope.confirm({title: "Дүрэм устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$http.delete(apiBaseUrl + '/formrule/delete_multiplier?id=' + id).
				success(function(res, status, headers, config) {
					var i = findM(id);
					if (i > -1) {
						$scope.M.splice(i, 1);
					}
					$scope.toast('success', 'Дүрэм устгах', 'Устгах үйлдэл амжилттай боллоо!');
				}).
				error(function(res, status, headers, config) {
					$scope.toast('error', 'Дүрэм устгах', res[0]);
				});
		});
	}

	// DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD

	function findD(id) {
		for (var i = 0; i < $scope.D.length; i++)
			if ($scope.D[i].id == id)
				return i;
		return -1;
	}

	function saveD(data) {
		if(data.hasOwnProperty('id')) {
			$http.post(apiBaseUrl + '/formrule/edit_divider?id=' + id, data).
			success(function(res, status, headers, config) {
				var i = findD(res.id);
				if (i > -1) {
					$scope.D[i] = angular.copy(res.data);
					console.log(res.data);
				}
				$scope.toast('success', 'Дүрэм засах', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Дүрэм засах', res[0]);
			});
		} else {
			$http.post(apiBaseUrl + '/formrule/add_divider?id=' + id, data).
			success(function(res, status, headers, config) {
				$scope.D.push(res.data);
				$scope.toast('success', 'Дүрэм нэмэх', 'Амжилттай хадгаллаа!');
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Дүрэм нэмэх', res[0]);
			});
		}
	};

	function manageD(data) {
		var modalInstance = $modal.open({
			animation: $scope.animationsEnabled,
			templateUrl: 'manageRuleDContent.html',
			controller: 'ManageRuleInstanceCtrl',
			resolve: {
				data: function () {
					return data;
				},
				elements: function() {
					return $scope.elements;
				}
			}
		});
		modalInstance.result.then(function (data) {
			saveD(data);
		}, function () {
			
		});
	}

	$scope.addD = function() {
		manageD();
	}

	$scope.editD = function(id) {
		var i = findD(id);
		if (i > -1) {
			manageD(angular.copy($scope.D[i]));
		}
	}

	$scope.delD = function(id) {
		$scope.confirm({title: "Дүрэм устгах", body: 'Та энэ үйлдлийг хийхдээ итгэлтэй байна уу?'}, function() {
			$http.delete(apiBaseUrl + '/formrule/delete_divider?id=' + id).
				success(function(res, status, headers, config) {
					var i = findD(id);
					if (i > -1) {
						$scope.D.splice(i, 1);
					}
					$scope.toast('success', 'Дүрэм устгах', 'Устгах үйлдэл амжилттай боллоо!');
				}).
				error(function(res, status, headers, config) {
					$scope.toast('error', 'Дүрэм устгах', res[0]);
				});
		});
	}
})

.controller('ManageRuleInstanceCtrl', function ($scope, $modalInstance, data, elements) {
	$scope.data = data;
	$scope.elements = elements;

	$scope.ok = function () {
		if ($scope.data.hasOwnProperty('max')) {
			if (!$scope.data.max || $scope.data.max == null || $scope.data.max == undefined)
				$scope.data.max = $scope.data.min;
			else {
				if ($scope.data.max < $scope.data.min) {
					var temp = $scope.data.max;
					$scope.data.max = $scope.data.min;
					$scope.data.min = temp;
				}
				if ($scope.data.max > 100) $scope.data.max = 1000000000;
			}
		}
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});