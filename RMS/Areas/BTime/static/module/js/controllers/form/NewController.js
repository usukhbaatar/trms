'use strict';

BtimeModule.controller('FormNewController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {   
		init();
	});

	var id;

	function init() {
		id = $state.params.id;
		$scope.group_id = id;
		$scope.form = {form_type: 5, dimension_id: 6, ph_score: 0};
		$http.get(apiBaseUrl + '/dimension/get').
			success(function(res, status, headers, config) {
				$scope.dimensions = res;
			}).
			error(function(data, status, headers, config) {
			});

		$scope.note = {
			title: 'Формын үндсэн мэдээлэл',
			body: 'Формын мэдээллийг засах алхамуудыг бүгдийг нь хийж гүйцсэний даараа формийн мэдээлэл хадгалагдах болохыг анхаарна уу. Формын үндсэн мэдээллийг бөглөхдөө дараах зүйлсийг заавал оруулах шаардлагатай!',
			hints: ['Формын нэр', 'Судалгааны төрөл', 'Хамрах хүрээ']
		}
	}

	$scope.submit = function() {
		$scope.form.group_id = id;
		if($scope.form.hasOwnProperty('elements')){
			delete $scope.form.elements;
		}

		if($scope.form.hasOwnProperty('status')){
			delete $scope.form.status;
		}

		if($scope.form.hasOwnProperty('id')){
			delete $scope.form.id;
		}

		if($scope.form.hasOwnProperty('authors')){
			delete $scope.form.authors;
		}

		if($scope.form.hasOwnProperty('authors')){
			delete $scope.form.authors;
		}

		if($scope.form.hasOwnProperty('multipliers')){
			delete $scope.form.multipliers;
		}

		if($scope.form.hasOwnProperty('dividers')){
			delete $scope.form.dividers;
		}

		$http.post(apiBaseUrl + '/form/post', $scope.form).
			success(function(res, status, headers, config) {
				var form_id = res.id;
				$scope.toast('success', 'Формын үндсэн мэдээлэл', 'Амжилттай хадгаллаа!');
				$state.go('btime.form_element', {id: form_id});
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Формын үндсэн мэдээлэл', res[0]);
			});
	}
});