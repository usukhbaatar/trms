'use strict';

BtimeModule.controller('FormViewController', function ($rootScope, $scope, $http, $timeout, $state, $modal) {
	$scope.$on('$viewContentLoaded', function() {   
		init();
	});

	var id;

	function init() {
		$scope.busy = false;
		id = $state.params.id;
		$scope.id = id;
		$http.get(apiBaseUrl + '/form/get/' + id).
			success(function(res, status, headers, config) {
				$scope.form = res.form;
			}).
			error(function(data, status, headers, config) {
				
			});

		$scope.elems = [{
			id: 1,
			val: "RMS төсөл",
			label: 'Бүтээлийн нэр',
			type: 'text'
		}, {
			id: 2,
			val: "2012-01-21",
			label: 'Огноо',
			type: 'date'
		}];
		$scope.users = [{firsname: 'Бат', lastname: 'Дорж'}, {firsname: 'Сүрэн', lastname: 'Цэнд'}];
		$scope.example = '[Харагдцыг үүсгэх заавар]\\[{1}]: (1)\\[[Зохиогчид:]] *** \\[[(2)]]';
		$scope.convert();

	}

	function getUsers() {
		var ret = "";
		for (var i = 0; i < $scope.users.length; i++) {
			if (i > 0) ret = ret + ", ";
			ret = ret + $scope.users[i].firsname + " " + $scope.users[i].lastname
		}
		return ret;
	}

	function findID(str) {
		var target = false;
		var ret = "";
		var l = -1;
		for (var i = 0; i < str.length; i++) {
			if (str[i] == '(') {
				l = i; ret = "(";
			} else if (str[i] == ')' && str[l] == '(') {
				if (l < i - 1) {
					return ret + ")";
				} else { l = -1; ret = ""; }
			} else if (str[i] == '{') {
				l = i; ret = "{";
			} else if (str[i] == '}' && l > -1 && str[l] == '{') {
				if (l < i - 1) {
					return ret + "}";
				} else { l = -1; ret = ""; }
			} else if (l > -1) {
				if (str[i] < '0' || str[i] > '9') {
					l = -1;
					ret = "";
				} else { ret = ret + str[i]; }
			}
		}
		return "";
	}

	function findElem(id) {
		for (var i = 0; i < $scope.elems.length; i++) {
			if ($scope.elems[i].id == id)
				return $scope.elems[i];
		}
	}

	function findElement(id) {
		for (var i = 0; i < $scope.form.elements.length; i++) {
			if ($scope.form.elements[i].id == id)
				return $scope.form.elements[i];
		}
		return null;
	}

	$scope.convert = function() {
		var temp = $scope.example;
		temp = temp.split("\\").join("<br>")
			.split("[[[").join("<strong><em>")
			.split("]]]").join("</em></strong>")
			.split("[[").join("<em>")
			.split("]]").join("</em>")
			.split("[").join("<strong>")
			.split("]").join("</strong>")
			.replace("***", getUsers());

		var res = findID(temp);
		while (res != "") {
			var id = parseInt(res.substr(1, res.length - 2));
			var val = "";
			if (res[0] == '(') {
				val = findElem(id).val;
			} else {
				val = findElem(id).label;
			}

			temp = temp.split(res).join(val);
			res = findID(temp);
		}
		return temp;
	}

	$scope.render = function() {
		if (!$scope.form) return "";
		var temp = $scope.form.view;
		if (temp == null) return "";
		temp = temp.split("\\").join("<br>")
			.split("[[[").join("<strong><em>")
			.split("]]]").join("</em></strong>")
			.split("[[").join("<em>")
			.split("]]").join("</em>")
			.split("[").join("<strong>")
			.split("]").join("</strong>")
			.replace("***", '<h5>Зохогчдийн жагсаалт энд байна</h5>');

		var res = findID(temp);
		while (res != "") {
			var id = parseInt(res.substr(1, res.length - 2));
			var val = "";
			if (res[0] == '(') {
				var tmp = findElement(id);
				if (tmp == null) {
					val = "<cite class=\"text-danger\">Буруу ID</cite>";
				} else {
					if (tmp.value) {
						val = tmp.value;
					} else {
						val = "<cite>Утга - " + id + "</cite>";
					}
				}
			} else {
				var tmp = findElement(id);
				if (tmp == null) {
					val = "<cite class=\"text-danger\">Буруу ID</cite>";
				} else {
					val = tmp.label;
				}
			}

			temp = temp.split(res).join(val);
			res = findID(temp);
		}
		return temp;
	}

	$scope.typeConvert = function(type) {
		if (type == 'text') return 'Текст';
		if (type == 'textarea') return 'Текст (том)';
		if (type == 'search') return 'Хайх';
		if (type == 'number') return 'Тоо';
		if (type == 'file') return 'Файл';
		if (type == 'select') return 'Сонгох';
		if (type == 'date') return 'Огноо';
	}

	$scope.submit = function() {
		if ($scope.busy) return;
		$http.post(apiBaseUrl + '/form/activate?id=' + $scope.id + "&view=" + $scope.form.view).
			success(function(res, status, headers, config) {
				$scope.toast('success', 'Формын харагдах дүрэм', 'Амжилттай хадгаллаа!');
				$scope.busy = false;
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Формын харагдах дүрэм', res[0]);
				$scope.busy = false;
			});
	}

	$scope.save = function() {
		if ($scope.busy) return;
		$http.post(apiBaseUrl + '/form/activate?id=' + $scope.id + "&view=" + $scope.form.view).
			success(function(res, status, headers, config) {
				$http.post(apiBaseUrl + '/form/activate?id=' + $scope.id + "&status=true").
					success(function(res, status, headers, config) {
						$scope.toast('success', 'Форм удирдах', 'Формыг амжилттай үүсгэлээ!');
						$state.go('btime.group', {id: $scope.form.group_id});
						$scope.busy = false;
					}).
					error(function(res, status, headers, config) {
						$scope.toast('error', 'Форм удирдах', res[0]);
						$scope.busy = false;
					});
				$scope.busy = false;
			}).
			error(function(res, status, headers, config) {
				$scope.toast('error', 'Формын харагдах дүрэм', res[0]);
				$scope.busy = false;
			});
	}
});