﻿'use strict';
var BtimeModule = angular.module("BtimeModule", ['toaster', "angucomplete-alt"]);
var btimeBaseUrl = "/Areas/BTime/static/module";
	
BtimeModule.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
	
	$stateProvider
	
	.state('btime', {
		url: '/btime',
		templateUrl: btimeBaseUrl + "/views/base.html",
		controller: 'BTimeController',
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/BTimeController.js'
					]
				});
			}]
		}
	})

	.state('btime.group', {
		url: "/group/{id}",
		templateUrl: btimeBaseUrl + "/views/group.html",
		data: { pageTitle: 'Эрдэм шинжилгээний бүлгүүд', pageSubTitle: '' },
		controller: "GroupController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/GroupController.js'
					]
				});
			}]
		}
	})

	.state('btime.form_edit', {
		url: "/form_edit/{id}",
		templateUrl: btimeBaseUrl + "/views/form/manage.html",
		data: { pageTitle: 'Форм удирдах', pageSubTitle: '' },
		controller: "FormEditController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/form/EditController.js'
					]
				});
			}]
		}
	})

	.state('btime.form_new', {
		url: "/form_new/{id}",
		templateUrl: btimeBaseUrl + "/views/form/manage.html",
		data: { pageTitle: 'Форм удирдах', pageSubTitle: '' },
		controller: "FormNewController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/form/NewController.js'
					]
				});
			}]
		}
	})

	.state('btime.form_back', {
		url: "/form_back/{id}",
		templateUrl: btimeBaseUrl + "/views/form/manage.html",
		data: { pageTitle: 'Форм удирдах', pageSubTitle: '' },
		controller: "FormBackController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/form/BackController.js'
					]
				});
			}]
		}
	})

	.state('btime.form_element', {
		url: "/form_element/{id}",
		templateUrl: btimeBaseUrl + "/views/form/element.html",
		data: { pageTitle: 'Форм удирдах', pageSubTitle: '' },
		controller: "FormElementController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/form/ElementController.js'
					]
				});
			}]
		}
	})

	.state('btime.form_rule', {
		url: "/form_rule/{id}",
		templateUrl: btimeBaseUrl + "/views/form/rule.html",
		data: { pageTitle: 'Форм удирдах', pageSubTitle: '' },
		controller: "FormRuleController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/form/RuleController.js'
					]
				});
			}]
		}
	})

	.state('btime.form_view', {
		url: "/form_view/{id}",
		templateUrl: btimeBaseUrl + "/views/form/view.html",
		data: { pageTitle: 'Форм удирдах', pageSubTitle: '' },
		controller: "FormViewController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/form/ViewController.js'
					]
				});
			}]
		}
	})

	.state('btime.admin', {
		url: "/admin",
		templateUrl: btimeBaseUrl + "/views/admin/index.html",
		data: { pageTitle: 'Aдмин', pageSubTitle: '' },
		controller: "AdminController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/admin/IndexController.js'
					]
				});
			}]
		}
	})

	.state('btime.admin.researches', {
		url: "/researches/{type}/{page}/{status}",
		templateUrl: btimeBaseUrl + "/views/admin/research/list.html",
		data: { pageTitle: 'Судалгааны ажлууд', pageSubTitle: '' },
		controller: "AdminResearchListController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/admin/research/ListController.js'
					]
				});
			}]
		}
	})

	.state('btime.admin.research', {
		url: "/research/{id}",
		templateUrl: btimeBaseUrl + "/views/admin/research/single.html",
		data: { pageTitle: 'Судалгааны ажлын дэлгэрэнгүй', pageSubTitle: '' },
		controller: "AdminResearchSingleController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/admin/research/SingleController.js',
						"/assets/admin/pages/css/timeline.css"
					]
				});
			}]
		}
	})

	.state('btime.admin.journals', {
		url: "/journals",
		templateUrl: btimeBaseUrl + "/views/admin/journal/root.html",
		data: { pageTitle: '', pageSubTitle: '' },
		controller: "AdminJournalRootController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/admin/journal/RootController.js'
					]
				});
			}]
		}
	})

	.state('btime.admin.journal', {
		url: "/journal/{id}",
		templateUrl: btimeBaseUrl + "/views/admin/journal/journal.html",
		data: { pageTitle: '', pageSubTitle: '' },
		controller: "AdminJournalController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/admin/journal/JournalController.js'
					]
				});
			}]
		}
	})

	.state('btime.admin.conferences', {
		url: "/conferences",
		templateUrl: btimeBaseUrl + "/views/admin/conference/root.html",
		data: { pageTitle: '', pageSubTitle: '' },
		controller: "AdminConferenceRootController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/admin/conference/RootController.js'
					]
				});
			}]
		}
	})

	.state('btime.admin.conference', {
		url: "/conference/{id}",
		templateUrl: btimeBaseUrl + "/views/admin/conference/conference.html",
		data: { pageTitle: '', pageSubTitle: '' },
		controller: "AdminConferenceController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/admin/conference/ConferenceController.js'
					]
				});
			}]
		}
	})

	.state('btime.my', {
		url: "/my",
		templateUrl: btimeBaseUrl + "/views/my/index.html",
		data: { pageTitle: '', pageSubTitle: '' },
		controller: "MyController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/my/IndexController.js',
						"/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css",
						"/assets/admin/pages/css/profile-old.css"
					]
				});
			}]
		}
	})

	.state('btime.my.profile', {
		url: "/profile",
		templateUrl: btimeBaseUrl + "/views/my/profile.html",
		data: { pageTitle: '', pageSubTitle: '' },
		controller: "MyProfileController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/my/ProfileController.js',
						"/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css",
						"/assets/admin/pages/css/profile-old.css",
						btimeBaseUrl + "/js/directives/chart/DimensionChart.js"
					]
				});
			}]
		}
	})

	.state('btime.my.research', {
		url: "/research/{id}",
		templateUrl: btimeBaseUrl + "/views/my/research.html",
		data: { pageTitle: 'Судалгааны ажлын дэлгэрэнгүй', pageSubTitle: '' },
		controller: "MyResearchController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/my/ResearchController.js',
						"/assets/admin/pages/css/timeline.css"
					]
				});
			}]
		}
	})

	.state('btime.user', {
		url: "/user/{id}",
		templateUrl: btimeBaseUrl + "/views/my/profile.html",
		data: { pageTitle: '', pageSubTitle: '' },
		controller: "MyProfileController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/my/ProfileController.js',
						"/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css",
						"/assets/admin/pages/css/profile-old.css"
					]
				});
			}]
		}
	})

	.state('btime.researches', {
		url: "/researches",
		templateUrl: btimeBaseUrl + "/views/researches.html",
		data: { pageTitle: 'Судалгааны ажлууд', pageSubTitle: '' },
		controller: "ResearchesController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/ResearchesController.js',
						"/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css",
						"/assets/admin/pages/css/profile-old.css"
					]
				});
			}]
		}
	})

	.state('btime.admin.report', {
		url: "/report",
		templateUrl: btimeBaseUrl + "/views/admin/report/index.html",
		data: { pageTitle: '', pageSubTitle: '' },
		controller: "AdminReportController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/admin/report/IndexController.js'
					]
				});
			}]
		}
	})

	.state('btime.admin.report.main', {
		url: "/main",
		templateUrl: btimeBaseUrl + "/views/admin/report/main.html",
		data: { pageTitle: 'Судалгааны ажлууд', pageSubTitle: '' },
		controller: "AdminReportMainController",
		resolve: {
			deps: ['$ocLazyLoad', function ($ocLazyLoad) {
				return $ocLazyLoad.load({
					name: 'BtimeModule',
					insertBefore: '#ng_load_plugins_before',
					files: [
						btimeBaseUrl + '/js/controllers/admin/report/MainController.js',
						btimeBaseUrl + '/js/directives/table/datatable.js'
					]
				});
			}]
		}
	})
}]);
