'use strict';

BtimeModule.directive('dimensionChart', function () {
	var ret = {
		restrict: 'E',
		replace:true,
		scope: {
			'data': '=',
		},
		
		template: '<div id="dimension_chart" style="width: 100%; height: 300px; font-size: 11px;"></div>',
		link: {
			pre : function() {},
			post: function (scope, element, attrs) {	
				//setTimeout( function() {
					scope.$watch('data', function() {
						var chart = AmCharts.makeChart( "dimension_chart", {
							"type": "pie",
							"theme": "light",
							"dataProvider": scope.data,
							"valueField": "value",
							"titleField": "label",
							"outlineAlpha": 0,
							"depth3D": 15,
							"innerRadius": "60%",
							"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]] Б-Багц цаг</b> ([[percents]]%)</span>",
							"angle": 30,
							"startRadius": "20%",
							"startDuration": 0.3,
							"titles":[{"text": "Судалгааны цар хүрээ"}],
					 		"export": AmCharts.exportCFG
						});
					});
				//});
			}
		}
	}
	return ret;
});

function buildBands(tag) {
	var bands = [];

	if (tag.OK_MIN == null) {
		return [{
			'color': 'green',
			'startValue': 0,
			'endValue': 100,
		}];
	} else {
		bands.push({
			'color': 'red',
			'startValue': tag.MAJOR_MIN - 10,
			'endValue': tag.MAJOR_MIN,
		});

		bands.push({
			'color': 'orange',
			'startValue': tag.MAJOR_MIN,
			'endValue': tag.MINOR_MIN,
		});

		bands.push({
			'color': 'yellow',
			'startValue': tag.MINOR_MIN,
			'endValue': tag.OK_MIN,
		});

		bands.push({
			'color': 'green',
			'startValue': tag.OK_MIN,
			'endValue': tag.OK_MAX,
		});

		bands.push({
			'color': 'yellow',
			'startValue': tag.OK_MAX,
			'endValue': tag.MINOR_MAX,
		});

		bands.push({
			'color': 'orange',
			'startValue': tag.MINOR_MAX,
			'endValue': tag.MAJOR_MAX,
		});

		bands.push({
			'color': 'red',
			'startValue': tag.MAJOR_MAX,
			'endValue': tag.MAJOR_MAX + 10,
		});

		return bands;
	}
}