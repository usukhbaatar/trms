'use strict';

BtimeModule.directive('myTable', function ($compile) {
	var ret = {
		restrict: 'E',
		replace: true,
		scope: {
			'data': '=',
			'id': '='
		},
		templateUrl: function(elem, attr) {
			return btimeBaseUrl + '/js/directives/table/' + attr.template + '.html';
		},
		link: {
			pre: function (scope, element, attrs) {},
			post: function(scope, element, attrs) {
				console.log(scope.data);
				setTimeout( function() {
					var table = $("#table_" + scope.id);
					var e = $("#table_" + scope.id);
			        e.dataTable({
			                language: {
			                    aria: {
			                        sortAscending: ": activate to sort column ascending",
			                        sortDescending: ": activate to sort column descending"
			                    },
			                    emptyTable: "No data available in table",
			                    info: "Showing _START_ to _END_ of _TOTAL_ entries",
			                    infoEmpty: "No entries found",
			                    infoFiltered: "(filtered1 from _MAX_ total entries)",
			                    lengthMenu: "_MENU_ entries",
			                    search: "Search:",
			                    zeroRecords: "No matching records found"
			                },
			                buttons: [{
			                    extend: "print",
			                    className: "btn dark btn-outline"
			                }, {
			                    extend: "copy",
			                    className: "btn red btn-outline"
			                }, {
			                    extend: "pdf",
			                    className: "btn green btn-outline"
			                }, {
			                    extend: "excel",
			                    className: "btn yellow btn-outline "
			                }, {
			                    extend: "csv",
			                    className: "btn purple btn-outline "
			                }, {
			                    extend: "colvis",
			                    className: "btn dark btn-outline",
			                    text: "Columns"
			                }],
			                responsive: !0,
			                order: [
			                    [0, "asc"]
			                ],
			                lengthMenu: [
			                    [5, 10, 15, 20, -1],
			                    [5, 10, 15, 20, "All"]
			                ],
			                pageLength: 10,
			                oSelectorOpts: {
			                   "filter": "applied"
			                },
			                dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
			            });
			        

					scope.$watch('data', function() {
						if (scope.data == null || scope.data == undefined)
							return;
						var table1 = $("#table_" + scope.id).dataTable();
						var settings1 = table1.fnSettings();
						table1.fnClearTable(this);
						for (var i = 0; i < scope.data.length; i++) {
							table1.oApi._fnAddData(settings1, scope.data[i]);
						}

						settings1.aiDisplay = settings1.aiDisplayMaster.slice();
						table1.fnDraw();
					});
				}, 20);
			}
		}
	}
	return ret;
});