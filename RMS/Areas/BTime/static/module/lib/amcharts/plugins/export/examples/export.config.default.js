/**
 * This is a sample chart export config file. It is provided as a reference on
 * how miscelaneous items in export menu can be used and set up.
 *
 * You do not need to use this file. It contains default export menu options 
 * that will be shown if you do not provide any "menu" in your export config.
 *
 * Please refer to README.md for more information.
 */


/**
 * PDF-specfic configuration
 */
AmCharts.exportPDF = {
	"format": "PDF",
	"content": [ "", "", {
		"image": "reference",
		"fit": [ 523.28, 769.89 ] // fit image to A4
	} ]
};

/**
 * Print-specfic configuration
 */
AmCharts.exportPrint = {
	"format": "PRINT",
	"label": "Хэвлэх"
};

/**
 * Define main universal config
 */
AmCharts.exportCFG = {
	"enabled": true,
	"fileName": "chart",
	"menu": [ {
		"class": "export-main",
		"label": "Экспорт",
		"menu": [ {
			"label": "Татах ...",
			"menu": [ "PNG", "JPG", "SVG", AmCharts.exportPDF ]
		}, {
			"label": "Хадгалах ...",
			"menu": [ "CSV", "XLSX", "JSON" ]
		}, {
			"label": "Тэмдэглэл",
			"action": "draw"
		}, AmCharts.exportPrint ]
	} ],

	"drawing": {
		"menu": [ {
			"class": "export-drawing",
			"menu": [ {
				"label": "Нэмэх ...",
				"menu": [ {
					"label": "Текст",
					"action": "text"
				} ]
			}, {
				"label": "Өөрчлөх ...",
				"menu": [ {
					"label": "Горим ...",
					"action": "draw.modes"
				}, {
					"label": "Өнгө ...",
					"action": "draw.colors"
				}, {
					"label": "Хэмжээ ...",
					"action": "draw.widths"
				}, "UNDO", "REDO" ]
			}, {
				"label": "Татах ...",
				"menu": [ "PNG", "JPG", "SVG", "PDF" ]
			}, "PRINT", "CANCEL" ]
		} ]
	}
};