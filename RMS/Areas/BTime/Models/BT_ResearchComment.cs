﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_ResearchComment : BT_Model
    {
        [Key]
        public int id { get; set; }

        [Required]
        public string comment { get; set; }

        public BT_Research research { get; set; }

        public int user_id { get; set; }

        public DateTime date { get; set; }
    }
}