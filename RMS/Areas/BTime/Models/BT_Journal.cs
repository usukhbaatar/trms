﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Journal : BT_Model
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public string name { get; set; }

        public DateTime date { get; set; }

        public string description { get; set; }

        public int root_id { get; set; }
    }
}