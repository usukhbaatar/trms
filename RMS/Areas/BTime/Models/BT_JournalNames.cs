﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_JournalNames : BT_Model
    {
        [Key]
        public int id { get; set; }

        [Required]
        public string name { get; set; }

        public int root_id { get; set; }
    }
}