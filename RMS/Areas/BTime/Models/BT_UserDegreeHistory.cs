﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_UserDegreeHistory
    {
        [Key]
        public int id { get; set; }

        [JsonIgnore]
        public BT_User user { get; set; }

        public BT_UserDegree degree { get; set; }

        public string description { get; set; }

        public DateTime date { get; set; }
    }
}