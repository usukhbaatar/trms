﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_FormElementValidator : BT_Model
    {
        [Key]
        public int id { get; set; }

        [Required]
        [JsonIgnore]
        public BT_FormElement element { get; set; }

        public string name { get; set; }

        public bool is_valid(string value)
        {
            if (name == "required")
            {
                if (value == null || value.Length == 0)
                    return false;
            }
            else if (name == "float")
            {
                int intValue;
                float floatValue;
                return Int32.TryParse(value, out intValue) || float.TryParse(value, out floatValue);
            }
            return true;
        }
    }
}