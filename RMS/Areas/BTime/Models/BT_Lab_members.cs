﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Lab_members : BT_Model
    {
        [Key]
        public int key { get; set; }
        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public int user_id { get; set; }
        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public int lab_id { get; set; }
        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public string role { get; set; }
        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public Int16 precent { get; set; }

    }
}