﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_ResearchData : BT_Model
    {
        [Key]
        public int id { get; set; }

        [Required]
        [JsonIgnore]
        public BT_Research research { get; set; }

        [Required]
        public BT_FormElement element { get; set; }

        public string value { get; set; }
    }
}