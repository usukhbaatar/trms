﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_NoDb_ResearchItem
    {
        [Required]
        public int element_id { get; set; }

        public string value { get; set; }
    }
}