﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_JournalRoot : BT_Model
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public string name { get; set; }

        public string description { get; set; }

        public int dimension_id { get; set; }

    }
}