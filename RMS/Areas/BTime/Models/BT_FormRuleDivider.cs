﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_FormRuleDivider : BT_Model
    {
        [Key]
        public int id { get; set; }

        [JsonIgnore]
        public BT_Form form { get; set; }

        [Required]
        public double value { get; set; }
    }
}