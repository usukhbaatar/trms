﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_UserRole : BT_Model
    {
        [Key]
        public int id { get; set; }

        public string name { get; set; }

        [JsonIgnore]
        public virtual ICollection<BT_User> users { get; set; }
    }
}