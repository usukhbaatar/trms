﻿using Newtonsoft.Json;
using RMS.Areas.BTime.Models.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_FormElement : BT_Model
    {
        [Key]
        public int id { get; set; }

        [JsonIgnore]
        public BT_Form form { get; set; }

        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public string label { get; set; }

        public string description { get; set; }

        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public string type { get; set; }

        /* 'text'
         * 'number'
         * 'textarea'
         * 'file'
         * 'date'
         * 'select'
         */

        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public int permission { get; set; }

        public virtual ICollection<BT_FormElementValidator> validators { get; set; }

        public int static_type { get; set; }

        /* 0: Энгийн
         * 1: Сэтгүүлийн нэр
         * 2: Хурлын нэр
         * 3: Бүтээлийн төрөл
         * 4: Төслийн нэр
         * 5: Нэр
         * 6: Абстракт
         * 7: Огноо
         */

        public Nullable<int> selection_id { get; set; }

        public string prefix { get; set; }

        public string postfix { get; set; }

        public List<BT_NoDb_ErrorMessage> get_errors(string value)
        {
            var ret = new List<BT_NoDb_ErrorMessage>();
            BTimeDbContext db = new BTimeDbContext();
            int intValue;
            switch (this.static_type)
            {
                case 1:
                    if (!Int32.TryParse(value, out intValue))
                        ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Сэтгүүлийн нэр буруу байна!" });
                    else
                    {
                        intValue = Int32.Parse(value);
                        var tempJournal = db.journals.Find(intValue);
                        if (tempJournal == null)
                            ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Сэтгүүлийн нэр буруу байна!" });
                    }
                    break;
                case 2:
                    if (!Int32.TryParse(value, out intValue))
                        ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Сэтгүүлийн нэр буруу байна!" });
                    else
                    {
                        intValue = Int32.Parse(value);
                        var tempConference = db.journals.Find(intValue);
                        if (tempConference == null)
                            ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Хурлын нэр буруу байна!" });
                    }
                    break;
                case 3:
                    // Бүтээл
                    break;

                case 4:
                    // Төсөл
                    break;
            }
            if (value != null || value.Length > 0)
            {
                
                if (type == "number")
                {
                    float floatValue;
                    if (!(Int32.TryParse(value, out intValue) || float.TryParse(value, out floatValue)))
                        ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Тоо оруулна уу!" });
                }
                else if (type == "file")
                {
                    /*if (!Int32.TryParse(value, out intValue))
                        ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Файл сонгоно уу!" });
                    else
                    {
                        intValue = Int32.Parse(value);
                        var tempFile = db.files.Find(intValue);
                        if (tempFile == null)
                            ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Файл сонгоно уу!" });
                    }*/
                }
                else if (type == "date")
                {
                    DateTime res;
                    if (!DateTime.TryParse(value, out res))
                        ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Буруу форматтай байна!" });

                }
                else if (type == "select")
                {
                    if (!Int32.TryParse(value, out intValue))
                        ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Файл сонгоно уу!" });
                    else
                    {
                        intValue = Int32.Parse(value);
                        var tempSelection = db.form_element_selection_options.Find(intValue);
                        if (tempSelection == null)
                            ret.Add(new BT_NoDb_ErrorMessage { element_id = id, message = "Буруу cонголт!" });
                    }
                }

            }
            return ret;
        }
        
    }
}