﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Form : BT_Model
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage="Энэ талбар хоосон байж болохгүй!")]
        public double credit { get; set; }

        [Required(ErrorMessage="Энэ талбар хоосон байж болохгүй!")]
        public string title { get; set; }

        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public string description { get; set; }

        public string type { get; set; } // Б

        [Required]
        public int form_type { get; set; }

        /* 0: Сэтгүүл
         * 1: Хурал
         * 2: Төсөл
         * 4: Бүтээл
         * 5: Бусад
         */

        [Required]
        public int dimension_id { get; set; }

        [Required]
        public int group_id { get; set; }

        public virtual ICollection<BT_FormElement> elements { get; set; }

        public string status { get; set; }

        public double ph_score { get; set; }

        [JsonIgnore]
        public virtual ICollection<BT_FormRuleAuthors> authors { get; set; }

        [JsonIgnore]
        public virtual ICollection<BT_FormRuleMultiplier> multipliers { get; set; }

        [JsonIgnore]
        public virtual ICollection<BT_FormRuleDivider> dividers { get; set; }

        public string view { get; set; }
    }
}