﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Lab_resourses : BT_Model
    {
        [Key]
        public int id { get; set; }
        public int lab_id { get; set; }
        public int res_type_id { get; set; }
        public string decription { get; set; }
        public string unit { get; set; }
        public float unit_price { get; set; }
        public int unit_count { get; set; }


    }
}