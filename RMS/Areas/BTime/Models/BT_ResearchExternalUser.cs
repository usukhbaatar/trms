﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_ResearchExternalUser : BT_Model
    {
        [Key]
        public int id {get; set;}

        public string lastname { get; set; }
        
        public string firstname { get; set; }

        public BT_Research research { get; set; }

        public int degree { get; set; }

        public string institute { get; set; }
    }
}