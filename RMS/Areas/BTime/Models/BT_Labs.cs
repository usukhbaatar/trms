﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Labs
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public string name { get; set; }
        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public string description { get; set; }
        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public DateTime opened_date { get; set; }
        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public DateTime closed_date { get; set; }
        public string status { get; set; }


    }
}