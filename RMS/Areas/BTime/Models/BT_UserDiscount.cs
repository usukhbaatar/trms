﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_UserDiscount : BT_Model
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        
        public double credit { get; set; }

        [JsonIgnore]
        public BT_User user { get; set; }
        
        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public int year { get; set; }

        public int semister { get; set; }

        [Required]
        public string comment { get; set; }

        public int admin_id { get; set; }
    }
}