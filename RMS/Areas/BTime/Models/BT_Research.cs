﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Research : BT_Model
    {
        [Key]
        public int id { get; set; }

        [JsonIgnore]
        [Required]
        public int form_id { get; set; }

        [Required]
        public int user_id { get; set; }

        public DateTime send_date { get; set; }

        public Nullable<DateTime> review_date { get; set; }

        public string status { get; set; }

        public double credit { get; set; }

        public BT_ResearchType research_type { get; set; }

        [JsonIgnore]
        public virtual ICollection<BT_ResearchData> data { get; set; }

        public virtual ICollection<BT_ResearchUser> users { get; set; }

        public virtual ICollection<BT_ResearchExternalUser> external_users {get; set; }

        public virtual ICollection<BT_ResearchComment> comments { get; set; }

        public static double calc(BT_Form form, List<BT_NoDb_ResearchItem> data, int users)
        {
            double ret = form.credit;
            foreach (var mul in form.multipliers)
            {
                foreach (var item in data)
                    if (item.element_id == mul.element_id)
                        if (item.value != null && item.value != "")
                            ret = ret * float.Parse(item.value);
            }

            foreach (var item in form.dividers)
                if (item.value != 0)
                    ret = ret / item.value;

            foreach (var item in form.authors)
                if (item.min <= users && users <= item.max)
                    ret = ret * item.percent / 100;

            return ret;
        }

        public static List<int> filter(List<int> users)
        {
            List<int> ret = new List<int>();
            for (int i = 0; i < users.Count; i++)
            {
                bool exist = false;
                for (int j = i + 1; j < users.Count; j++)
                    if (users[i] == users[j])
                        exist = true;
                if (!exist)
                    ret.Add(users[i]);
            }
            return ret;
        }

        public static List<BT_NoDb_ResearchItem> filter(List<BT_NoDb_ResearchItem> items, BT_Form form)
        {
            List<BT_NoDb_ResearchItem> ret = new List<BT_NoDb_ResearchItem>();
            foreach (var item in items)
            {
                foreach (var elem in form.elements)
                    if (elem.id == item.element_id)
                    {
                        ret.Add(item);
                        break;
                    }
            }
            return ret;
        }

        public static BT_NoDb_ResearchItem findItem(int id, List<BT_NoDb_ResearchItem> items)
        {
            foreach (var item in items)
                if (item.element_id == id)
                    return item;
            return null;
        }

        public static List<BT_NoDb_ErrorMessage> get_errors(BT_Form form, List<BT_NoDb_ResearchItem> data)
        {
            var ret = new List<BT_NoDb_ErrorMessage>();
            foreach (var elem in form.elements)
            {
                var item = BT_Research.findItem(elem.id, data);
                if (item == null)
                    return new List<BT_NoDb_ErrorMessage> {
                        new BT_NoDb_ErrorMessage {element_id = 0, message = "Буруу хүсэлт!"}
                    };
                ret.AddRange(elem.get_errors(item.value));
                foreach (var validator in elem.validators)
                {
                    if (!validator.is_valid(item.value))
                    {
                        if (validator.name == "required")
                            ret.Add(new BT_NoDb_ErrorMessage { element_id = elem.id, message = "Энэ талбар хоосон байж болохгүй!" });
                        else if (validator.name == "float")
                            ret.Add(new BT_NoDb_ErrorMessage { element_id = elem.id, message = "Тоо оруулна уу!" });
                    }
                }
            }
            return ret;
        }


    }
}