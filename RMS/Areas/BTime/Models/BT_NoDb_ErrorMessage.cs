﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_NoDb_ErrorMessage
    {
        public int element_id { get; set; }
        public string message { get; set; }
    }
}