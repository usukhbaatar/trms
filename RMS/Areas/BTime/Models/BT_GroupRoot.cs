﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_GroupRoot : BT_Model
    {
        [Key]
        public int id { get; set; }

        public string number { get; set; }
        
        [Required]
        public string title { get; set; }
        
        public string description { get; set; }

        public int dimension_id { get; set; }

        /*
         * 0: ОУ
         * 1: Улсын
         * 2: Сургуулийн
         * 3: Тэнхимийн
         */
    }
}