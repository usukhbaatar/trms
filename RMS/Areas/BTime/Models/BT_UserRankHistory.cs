﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_UserRankHistory : BT_Model
    {
        [Key]
        public int id { get; set; }

        [JsonIgnore]
        public BT_User user { get; set; }

        public BT_UserRank rank { get; set; }

        public int year { get; set; }

        public int semister { get; set; }
    }
}