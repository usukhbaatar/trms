﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Dissertation
    {
        [Key]
        public int id { get; set; }

        [Required]
        public string name { get; set; }

        public string description {get; set;}

        public BT_File file { get; set; }

        public string type { get; set; }

        public DateTime date { get; set; }

        public BT_SchoolFaculty faculty { get; set; }

        public string status { get; set; }

    }
}