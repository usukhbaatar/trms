﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_School : BT_Model
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string short_name { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public virtual ICollection<BT_SchoolFaculty> faculties { get; set; }
    }
}