﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_NoDb_Research
    {
        public List<BT_NoDb_ResearchItem> items { get; set; }
        public List<int> users { get; set; }
        public List<BT_NoDb_ExternalUser> external_users { get; set; }
    }
}