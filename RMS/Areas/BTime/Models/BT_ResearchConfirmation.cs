﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_ResearchConfirmation : BT_Model
    {
        [Key]
        public int id { get; set; }

        public BT_Research research { get; set; }

        public string status { get; set; }

        public DateTime date { get; set; }

        public BT_SchoolFaculty faculty { get; set; }
    }
}