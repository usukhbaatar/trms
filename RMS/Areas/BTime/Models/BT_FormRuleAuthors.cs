﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_FormRuleAuthors : BT_Model
    {
        [Key]
        public int id { get; set; }

        [JsonIgnore]
        public BT_Form form { get; set; }

        [Required]
        public int min { get; set; }

        [Required]
        public int max { get; set; }

        [Required]
        public float percent { get; set; }
    }
}