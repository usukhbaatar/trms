﻿using RMS.Areas.BTime.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System;

using Microsoft.AspNet.Identity;
using System.Web;

namespace RMS.Areas.BTime.Models.DAL
{
    public class BTimeDbContext : DbContext
    {
        public BTimeDbContext()
            : base("DefaultConnection")
        {

        }

        public DbSet<BT_GroupRoot> group_roots { get; set; }
        public DbSet<BT_Group> groups { get; set; }

        public DbSet<BT_Form> forms { get; set; }
        public DbSet<BT_FormElement> form_elements { get; set; }
        public DbSet<BT_FormElementValidator> form_element_validators { get; set; }
        public DbSet<BT_FormRuleAuthors> form_rule_authors { get; set; }
        public DbSet<BT_FormRuleMultiplier> form_rule_multipliers { get; set; }
        public DbSet<BT_FormRuleDivider> form_rule_dividers { get; set; }
        public DbSet<BT_FormElementSelection> form_element_selections { get; set; }
        public DbSet<BT_FormElementSelectionOption> form_element_selection_options { get; set; }

        public DbSet<BT_ResearchType> research_types { get; set; }
        public DbSet<BT_Research> researches { get; set; }
        public DbSet<BT_ResearchData> research_datas { get; set; }

        public DbSet<BT_JournalRoot> journal_roots { get; set; }
        public DbSet<BT_JournalNames> journal_names { get; set; }
        public DbSet<BT_Journal> journals { get; set; }

        public DbSet<BT_ConferenceRoot> conference_roots { get; set; }
        public DbSet<BT_ConferenceNames> conference_names { get; set; }
        public DbSet<BT_Conference> conferences { get; set; }

        public DbSet<BT_Dimension> dimensions { get; set; }
        public DbSet<BT_File> files { get; set; }

        public DbSet<BT_ResearchUser> research_users { get; set; }
        public DbSet<BT_ResearchExternalUser> research_external_users { get; set; }

        public DbSet<BT_ResearchComment> comments { get; set; }

        public DbSet<BT_UserDiscount> discounts { get; set; }
        public DbSet<BT_Labs> labs { get; set; }
        public DbSet<BT_Lab_members> lab_members { get; set; }
        public DbSet<BT_Lab_resourses> lab_resources { get; set; }
        
        public DbSet<BT_User> users { get; set; }
        public DbSet<BT_UserRole> user_roles { get; set; }
        public DbSet<BT_UserDegree> degrees { get; set; }
        public DbSet<BT_UserDegreeHistory> degree_histories { get; set; }
        public DbSet<BT_UserRank> ranks { get; set; }
        public DbSet<BT_UserRankHistory> rank_histories { get; set; }

        public DbSet<BT_School> schools { get; set; }
        public DbSet<BT_SchoolFaculty> school_faculties { get; set; }

        public DbSet<BT_ResearchConfirmation> research_confirmations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<BT_FormElement>()
                .HasMany(x => x.validators)
                .WithRequired(y => y.element)
                .WillCascadeOnDelete();

            modelBuilder.Entity<BT_Form>()
                .HasMany(x => x.elements)
                .WithRequired(y => y.form)
                .WillCascadeOnDelete();

            modelBuilder.Entity<BT_Form>()
                .HasMany(x => x.multipliers)
                .WithRequired(y => y.form)
                .WillCascadeOnDelete();

            modelBuilder.Entity<BT_Form>()
                .HasMany(x => x.dividers)
                .WithRequired(y => y.form)
                .WillCascadeOnDelete();

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public override int SaveChanges()
        {
            var auditedEntities = ChangeTracker.Entries<BT_Model>();

            string userName = "Anonioumys";
            if (HttpContext.Current != null)
            {
                userName = HttpContext.Current.User.Identity.GetUserName();
            }

            var now = DateTime.Now;
            foreach (var entity in auditedEntities)
            {
                if (entity.Entity is BT_Model)
                {
                    BT_Model model = entity.Entity;
                    if (entity.State == EntityState.Added)
                    {
                        model._created_at = now;
                        model._updated_at = now;
                        model._created_by = userName;
                        model._updated_by = userName;
                    }
                    if (entity.State == EntityState.Modified)
                    {
                        model._updated_at = now;
                        model._updated_by = userName;
                    }
                }
            }

            return base.SaveChanges();
        }
    }
}