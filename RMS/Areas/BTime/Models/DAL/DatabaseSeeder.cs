﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models.DAL
{
    public class BTimeDbSeeder : System.Data.Entity.DropCreateDatabaseIfModelChanges<BTimeDbContext>
    {
        protected override void Seed(BTimeDbContext context) {

            var dimensions = new List<BT_Dimension> {
                new BT_Dimension {name = "Олон улсын"},
                new BT_Dimension {name = "Улсын"},
                new BT_Dimension {name = "Сургуулийн"},
                new BT_Dimension {name = "Салбар сургуулийн"},
                new BT_Dimension {name = "Тэнхимийн"},
                new BT_Dimension {name = "Бусад"}
            };

            dimensions.ForEach(c => context.dimensions.Add(c));
            context.SaveChanges();

            var group_roots = new List<BT_GroupRoot> {
                new BT_GroupRoot{id = 1, number = "Нүүр", title="Бүгд", description = ""},
                new BT_GroupRoot{id = 2, number = "Б.1.", title = "Эрдэм шинжилгээний сэтгүүлд өгүүлэл хэвлүүлэх", description = "Томсон-Ройтерсийн индекс {/IF-JCR/-тэй | /IF-JCR/-гүй} сэтгүүл"},
                new BT_GroupRoot{id = 3, number = "Б.2.", title = "Эрдэм шинжилгээний хурлын бүрэн илтгэл хэвлүүлэх", description = "Олон улсын хурал | Улслын хэмжээнд | Сургуулын хэмжээнд"},
                new BT_GroupRoot{id = 4, number = "Б.3.", title = "Эрдэм шинжилгээний бүтээл хэвлүүлэх", description = "Монограф | Шинжлэх ухааны докторын десертац | Сурах бичиг"},
                new BT_GroupRoot{id = 5, number = "Б.4.", title = "Оюуны өмчийн бүтээл", description = "Шинэ бүтээл | Оновчтой санал | Зохиогчийн эрхээр баталгаажуулсан бүтээл"},
                new BT_GroupRoot{id = 6, number = "Б.6.", title = "Эрдэм шинжилгээний бусад ажил", description = "Эрдэм шинжилгээний их семинарт илтгэл тавих | Мэргэжлийн түвшинд хянагддаггүй сэтгүүлд өгүүлэл хэвлүүлэх"}
                
            };

            group_roots.ForEach(c => context.group_roots.Add(c));
            context.SaveChanges();

            var groups = new List<BT_Group> {
                new BT_Group{id = 1, number = "Нүүр", parent_id = 0, type="root", group_year = 2015, root_id = 1, title = "Эрдэм шинжилгээний судалгааны бүлгүүд", description = "Эрдэм шинжилгээ, зохион бүтээх болонбусад бүтээлч үйл ажиллагаа"},
                new BT_Group{id = 2, number = "Б.1.", parent_id = 1, type = "leaf", group_year = 2015, root_id = 2, title = "Эрдэм шинжилгээний сэтгүүлд өгүүлэл хэвлүүлэх", description = "Томсон-Ройтерсийн индекс {/IF-JCR/-тэй | /IF-JCR/-гүй} сэтгүүл"},
                new BT_Group{id = 3, number = "Б.2.", parent_id = 1, type = "leaf", group_year = 2015, root_id = 3, title = "Эрдэм шинжилгээний хурлын бүрэн илтгэл хэвлүүлэх", description = "Олон улсын хурал | Улслын хэмжээнд | Сургуулын хэмжээнд"},
                new BT_Group{id = 4, number = "Б.3.", parent_id = 1, type = "leaf", group_year = 2015, root_id = 4, title = "Эрдэм шинжилгээний бүтээл хэвлүүлэх", description = "Монограф | Шинжлэх ухааны докторын десертац | Сурах бичиг"},
                new BT_Group{id = 5, number = "Б.4.", parent_id = 1, type = "leaf", group_year = 2015, root_id = 5, title = "Оюуны өмчийн бүтээл", description = "Шинэ бүтээл | Оновчтой санал | Зохиогчийн эрхээр баталгаажуулсан бүтээл"},
                new BT_Group{id = 6, number = "Б.6.", parent_id = 1, type = "leaf", group_year = 2015, root_id = 6, title = "Эрдэм шинжилгээний бусад ажил", description = "Эрдэм шинжилгээний их семинарт илтгэл тавих | Мэргэжлийн түвшинд хянагддаггүй сэтгүүлд өгүүлэл хэвлүүлэх"}
            };

            groups.ForEach(c => context.groups.Add(c));
            context.SaveChanges();

            var selections = new List<BT_FormElementSelection> {
                new BT_FormElementSelection {name = "Тийм, Үгүй"}
            };

            selections.ForEach(c => context.form_element_selections.Add(c));
            context.SaveChanges();

            var selection_options = new List<BT_FormElementSelectionOption> {
                new BT_FormElementSelectionOption {name = "Тийм", selection = selections[0]},
                new BT_FormElementSelectionOption {name = "Үгүй", selection = selections[0]}
            };

            selection_options.ForEach(c => context.form_element_selection_options.Add(c));
            context.SaveChanges();

            var forms = new List<BT_Form> {
                new BT_Form{id = 1, form_type=1, credit = 8, title = "Жишээ - 1", description = "Жишээ формын тайлбар.", group_id = 3, status = "active", type = "Б", dimension_id = 1, ph_score = 0, view = "[(1): (2)]\\[{3}]: (3)\\*** [[[(4)]]]"}
            };

            forms.ForEach(c => context.forms.Add(c));
            context.SaveChanges();

            var form_elements = new List<BT_FormElement> {
                new BT_FormElement{form = forms[0], static_type = 2, label = "Хурлын нэр", description = "", type = "search", permission = 0},
                new BT_FormElement{form = forms[0], static_type = 5, label = "Нэр", description = "Хуралд оролцон албан ёсны нэр", type = "text", permission = 0},
                new BT_FormElement{form = forms[0], static_type = 6, label = "Хураангуй", description = "", type = "textarea", permission = 0},
                new BT_FormElement{form = forms[0], static_type = 7, label = "Огноо", description = "", type = "date", permission = 0},
                new BT_FormElement{form = forms[0], static_type = 0, label = "Бүтээл", description = "PDF форматаар", type = "file", permission = 0},
                new BT_FormElement{form = forms[0], static_type = 0, label = "Тайлбар", description = "", type = "textarea", permission = 0},
                new BT_FormElement{form = forms[0], static_type = 0, label = "Тийм үү?", description = "", type = "select", permission = 1, selection_id = 1}
            };

            form_elements.ForEach(c => context.form_elements.Add(c));
            context.SaveChanges();

            var form_element_validators = new List<BT_FormElementValidator> {
                new BT_FormElementValidator{element = form_elements[0], name = "required"},
                new BT_FormElementValidator{element = form_elements[1], name = "required"},
                new BT_FormElementValidator{element = form_elements[2], name = "required"},
                new BT_FormElementValidator{element = form_elements[3], name = "required"},
                new BT_FormElementValidator{element = form_elements[4], name = "required"}
            };

            form_element_validators.ForEach(c => context.form_element_validators.Add(c));
            context.SaveChanges();

            var form_element_authors = new List<BT_FormRuleAuthors> {
                new BT_FormRuleAuthors {min = 1, max = 1, percent = 100, form = forms[0]},
                new BT_FormRuleAuthors {min = 2, max = 3, percent = 75, form = forms[0]},
                new BT_FormRuleAuthors {min = 4, max = 1000000, percent = 50, form = forms[0]}
            };

            form_element_authors.ForEach(c => context.form_rule_authors.Add(c));
            context.SaveChanges();

            var research_types = new List<BT_ResearchType> {
                new BT_ResearchType {name = "Байгалын ухаан"},
                new BT_ResearchType {name = "Нийгмийн ухаан"},
                new BT_ResearchType {name = "Төрөл - 3"},
                new BT_ResearchType {name = "Төрөл - 4"},
                new BT_ResearchType {name = "Төрөл - 5"},
                new BT_ResearchType {name = "Төрөл - 6"},
                new BT_ResearchType {name = "Төрөл - 7"},
                new BT_ResearchType {name = "Төрөл - 8"}
            };

            research_types.ForEach(c => context.research_types.Add(c));
            context.SaveChanges();

            var journal_roots = new List<BT_JournalRoot> {
                new BT_JournalRoot {name = "Сэтгүүл - 1", dimension_id = 1},
                new BT_JournalRoot {name = "Сэтгүүл - 2", dimension_id = 1},
                new BT_JournalRoot {name = "Сэтгүүл - 3", dimension_id = 2}
            };

            journal_roots.ForEach(c => context.journal_roots.Add(c));
            context.SaveChanges();

            var journal_names = new List<BT_JournalNames> {
                new BT_JournalNames {name = "Journal - 1", root_id = 1}
            };

            journal_names.ForEach(c => context.journal_names.Add(c));
            context.SaveChanges();

            var journals = new List<BT_Journal> {
                new BT_Journal {name = "Сэтгүүл - 1", date = new DateTime(2014, 3, 1), root_id = 1},
                new BT_Journal {name = "Сэтгүүл - 1", date = new DateTime(2015, 3, 1), root_id = 1},
                new BT_Journal {name = "Сэтгүүл - 2", date = new DateTime(2015, 3, 1), root_id = 2},
                new BT_Journal {name = "Сэтгүүл - 3", date = new DateTime(2015, 3, 1), root_id = 3}
            };

            journals.ForEach(c => context.journals.Add(c));
            context.SaveChanges();

            var conference_roots = new List<BT_ConferenceRoot> {
                new BT_ConferenceRoot {name = "Хурал - 1", dimension_id = 1},
                new BT_ConferenceRoot {name = "Хурал - 2", dimension_id = 1},
                new BT_ConferenceRoot {name = "Хурал - 3", dimension_id = 2}
            };

            conference_roots.ForEach(c => context.conference_roots.Add(c));
            context.SaveChanges();

            var conference_names = new List<BT_ConferenceNames> {
                new BT_ConferenceNames {name = "Conference - 1", root_id = 1}
            };

            conference_names.ForEach(c => context.conference_names.Add(c));
            context.SaveChanges();

            var conferences = new List<BT_Conference> {
                new BT_Conference {name = "Хурал - 1", date = new DateTime(2014, 3, 1), root_id = 1},
                new BT_Conference {name = "Хурал - 1", date = new DateTime(2015, 3, 1), root_id = 1},
                new BT_Conference {name = "Хурал - 2", date = new DateTime(2015, 3, 1), root_id = 2},
                new BT_Conference {name = "Хурал - 3", date = new DateTime(2015, 3, 1), root_id = 3}
            };

            conferences.ForEach(c => context.conferences.Add(c));
            context.SaveChanges();

            var schools = new List<BT_School> {
                new BT_School{ id = 1, short_name = "ХШУИС", name = "Хэрэглээний Шинжлэх Ухаан Инженерчлэлийн сургууль"},
                new BT_School{ id = 2, short_name = "ШУС", name = "Шунжлэх Ухааны Сургууль"},
                new BT_School{ id = 3, short_name = "ЭЗС", name = "Эдийн Засгийн Сургууль"}
            };

            schools.ForEach(c => context.schools.Add(c));
            context.SaveChanges();

            var faculties = new List<BT_SchoolFaculty> {
                new BT_SchoolFaculty{ id = 1, short_name = "МТ", name = "Компьютерийн Ухааны Тэнхим", is_school = 0, school = schools[0]},
                new BT_SchoolFaculty{ id = 2, short_name = "ЭХТ", name = "Шунжлэх Ухааны Сургууль", is_school = 0, school = schools[0]},
                new BT_SchoolFaculty{ id = 3, short_name = "ЭЗС", description = "Эдийн Засгийн Сургууль", is_school = 0, school = schools[0]}
            };

            faculties.ForEach(c => context.school_faculties.Add(c));
            context.SaveChanges();

            var roles = new List<BT_UserRole> {
                new BT_UserRole {name = "Оюутан"},
                new BT_UserRole {name = "Багш"},
                new BT_UserRole {name = "Тэнхимийн эрхлэгч"},
                new BT_UserRole {name = "Эрдэмтэн нарийн бичгийн дарга"},
                new BT_UserRole {name = "Админ"}
            };

            roles.ForEach(c => context.user_roles.Add(c));
            context.SaveChanges();

            var degrees = new List<BT_UserDegree> {
                new BT_UserDegree { name = "Бакалавр"},
                new BT_UserDegree { name = "Магистер"},
                new BT_UserDegree { name = "Доктор"},
                new BT_UserDegree { name = "Пост доктор"},
            };

            degrees.ForEach(c => context.degrees.Add(c));
            context.SaveChanges();


            var ranks = new List<BT_UserRank> {
                new BT_UserRank {name = "Сургалтын инженер", credit = 0},
                new BT_UserRank {name = "Дадлагажигч багш", credit = 4},
                new BT_UserRank {name = "Багш", credit = 6},
                new BT_UserRank {name = "Ахлах багш", credit = 8},
                new BT_UserRank {name = "Дэд профессор", credit = 10},
                new BT_UserRank {name = "Профессор", credit = 10}
            };

            ranks.ForEach(c => context.ranks.Add(c));
            context.SaveChanges();

            var users = new List<BT_User> {
                new BT_User{id = 1, username = "usukhbaatar", firstname = "Өсөхбаатар", lastname = "Дотгонванчиг", faculty = faculties[0], role = roles[4]},
                new BT_User{id = 2, username = "bat", firstname = "Бат", lastname = "Равжаа", faculty = faculties[0], role = roles[1]}
            };

            users.ForEach(c => context.users.Add(c));
            context.SaveChanges();

            var rank_histoies = new List<BT_UserRankHistory> {
                new BT_UserRankHistory{ rank = ranks[0], user = users[0], year = 2013, semister = 1},
                new BT_UserRankHistory{ rank = ranks[1], user = users[0], year = 2015, semister = 1},
            };

            rank_histoies.ForEach(c => context.rank_histories.Add(c));
            context.SaveChanges();

            var degree_histoies = new List<BT_UserDegreeHistory> {
                new BT_UserDegreeHistory{ degree = degrees[0], user = users[0], date = new DateTime(2013, 6, 15), description = "МУИС-МТС"},
                new BT_UserDegreeHistory{ degree = degrees[1], user = users[0], date = new DateTime(2015, 6, 15), description = "МУИС-МТС"},
            };

            degree_histoies.ForEach(c => context.degree_histories.Add(c));
            context.SaveChanges();
        }
        
    }
}