﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_ResearchUser : BT_Model
    {
        [Key]
        public int id { get; set; }

        [Required]
        public BT_User user { get; set; }

        [Required]
        public BT_Research research { get; set; }

        public string status { get; set; }

        public int year { get; set; }

        public int semister { get; set; }

    }
}