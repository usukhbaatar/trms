﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_File : BT_Model
    {
        [Key]
        public int id {get; set;}

        public string name { get; set; }

        public string ext { get; set; }

        public string path { get; set; }
    }
}