﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_DissertationCoach
    {
        [Key]
        public int id { get; set; }

        public BT_User user { get; set; }

        public BT_Dissertation dissertation { get; set; }

    }
}