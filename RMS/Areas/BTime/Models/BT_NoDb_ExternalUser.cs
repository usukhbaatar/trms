﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_NoDb_ExternalUser
    {
        public string firstname { get; set; }

        public string lastname { get; set; }

        public int degree { get; set; }

        public string institute { get; set; }
    }
}