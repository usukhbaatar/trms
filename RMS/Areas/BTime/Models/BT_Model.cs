﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Model
    {

        public string _created_by { get; set; }

        public string _updated_by { get; set; }

        public DateTime _created_at { get; set; }

        public DateTime _updated_at { get; set; }
    }
}