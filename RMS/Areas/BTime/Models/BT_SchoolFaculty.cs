﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_SchoolFaculty
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string short_name { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public int is_school { get; set; }

        [JsonIgnore]
        public virtual ICollection<BT_UserRankHistory> users { get; set; }

        [JsonIgnore]
        public BT_School school { get; set; }

    }
}