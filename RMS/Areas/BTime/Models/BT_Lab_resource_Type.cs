﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Lab_resource_Type
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
    }
}