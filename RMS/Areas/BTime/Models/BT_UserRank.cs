﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_UserRank : BT_Model
    {
        [Key]
        public int id { get; set; }

        public string name { get; set; }

        public double credit { get; set; }

        [JsonIgnore]
        public virtual ICollection<BT_UserRankHistory> histories { get; set; }
    }
}