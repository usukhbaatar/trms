﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string username { get; set; }

        public string firstname { get; set; }

        public string lastname { get; set; }

        public string picture { get; set; }

        public BT_UserRole role { get; set; }

        public BT_SchoolFaculty faculty { get; set; }

        public virtual ICollection<BT_UserRankHistory> rank_histories { get; set; }

        public virtual ICollection<BT_UserDegreeHistory> degree_histories { get; set; }

        public virtual ICollection<BT_UserDiscount> discounts { get; set; }

    }
}
