﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Areas.BTime.Models
{
    public class BT_Group : BT_Model
    {
        [Key]
        public int id { get; set; }

        public string number { get; set; }

        [Required(ErrorMessage = "Энэ талбар хоосон байж болохгүй!")]
        public string title { get; set; }

        public string description { get; set; }

        public string type { get; set; }

        public int group_year { get; set; }

        public int parent_id { get; set; }

        public int root_id {get; set; }
        
    }
}