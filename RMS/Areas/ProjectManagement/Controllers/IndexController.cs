﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMS.Areas.ProjectManagement.Controllers
{
    public class IndexController : Controller
    {
        // GET: ProjectManagement/Index
        public ActionResult Index()
        {
            return View();
        }
    }
}