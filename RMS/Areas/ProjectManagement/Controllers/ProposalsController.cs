﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RMS.Areas.ProjectManagement.Models;
using RMS.Areas.ProjectManagement.Models.DAL;
using RMS.Areas.ProjectManagement.Models.Binding;
using RMS.Filters;

namespace RMS.Areas.ProjectManagement.Controllers
{
    
    public class ProposalsController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();

        // GET: api/Proposals
        [PermissionActionFilter("proposal.list")]
        public IQueryable<Proposal> GetProposals()
        {
            return db.Proposals;
        }

        // GET: api/Proposals/5
        [ResponseType(typeof(Proposal))]
        public IHttpActionResult GetProposal(int id)
        {
            Proposal proposal = db.Proposals.Find(id);
            if (proposal == null)
            {
                return NotFound();
            }

            return Ok(proposal);
        }

        // PUT: api/Proposals/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProposal(int id, Proposal proposal)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != proposal.ID)
            {
                return BadRequest();
            }

            db.Entry(proposal).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProposalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Proposals
        [ResponseType(typeof(Proposal))]
        public IHttpActionResult PostProposal(ProposalBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Proposal proposal = new Proposal() { 
                ProjectName = model.ProjectName,
                HeadRequirement = model.HeadRequirement,
                ParticipantRequirement = model.ParticipantRequirement,
                Motivation = model.Motivation,
                Outcome = model.Outcome,
                ContactAddress = model.ContactAddress,
                ContactEmail = model.ContactEmail,
                ContactPhone = model.ContactPhone,
                Cost = model.Cost,
                StartDate = model.StartDate,
                EndDate = model.EndDate                
            };

            db.Proposals.Add(proposal);
            db.SaveChanges();

            

            return CreatedAtRoute("DefaultApi", new { id = proposal.ID }, proposal);
        }

        // DELETE: api/Proposals/5
        [ResponseType(typeof(Proposal))]
        public IHttpActionResult DeleteProposal(int id)
        {
            Proposal proposal = db.Proposals.Find(id);
            if (proposal == null)
            {
                return NotFound();
            }

            db.Proposals.Remove(proposal);
            db.SaveChanges();

            return Ok(proposal);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProposalExists(int id)
        {
            return db.Proposals.Count(e => e.ID == id) > 0;
        }
    }
}