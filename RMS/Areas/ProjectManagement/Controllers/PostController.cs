﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RMS.Areas.ProjectManagement.Models;
using RMS.Areas.ProjectManagement.Models.DAL;


namespace RMS.Areas.ProjectManagement.Controllers
{
    public class PostController : ApiController
    {
        private ProjectDbContext db;

        public PostController()
        {
            db = new ProjectDbContext();
        }

        // GET api/Post
        [HttpGet]
        [Queryable]
        public HttpResponseMessage get()
        {
            try
            {
                var now = DateTime.Now;
                var Posts = db.Posts.Include(p => p.PLabels).Where(p => p.sData <= now && p.eData >= now);
                return Request.CreateResponse(HttpStatusCode.OK, Posts.AsQueryable());
            }
            catch (Exception ex) {
                System.Console.WriteLine(ex.ToString());
                return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }

        }


        // GET api/Post/5
        [ResponseType(typeof(Post))]
        public IHttpActionResult PostPost(int id)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return NotFound();
            }

            return Ok(post);
        }

        // PUT api/Post/5
        public IHttpActionResult PutPost(int id, Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != post.id)
            {
                return BadRequest();
            }

            db.Entry(post).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Post
        [ResponseType(typeof(Post))]
        public IHttpActionResult PostPost(Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Posts.Add(post);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = post.id }, post);
        }

        // DELETE api/Post/5
        [ResponseType(typeof(Post))]
        public IHttpActionResult DeletePost(int id)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return NotFound();
            }

            db.Posts.Remove(post);
            db.SaveChanges();

            return Ok(post);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PostExists(int id)
        {
            return db.Posts.Count(e => e.id == id) > 0;
        }
    }
}