﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RMS.Areas.ProjectManagement.Models;
using RMS.Areas.ProjectManagement.Models.DAL;

namespace RMS.Areas.ProjectManagement.Controllers
{
    public class PLabelController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();

        // GET api/PLabel
        [HttpPost]        
        [HttpGet]
        public HttpResponseMessage get()
        {
            try
            {
                var PLabels = db.PLabels;
                return Request.CreateResponse(HttpStatusCode.OK, PLabels.AsQueryable());
            }
            catch (Exception ex)
            {
                System.Console.Error.WriteLine(ex.ToString());
                return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }

        }


        // GET api/Default1/5
        [ResponseType(typeof(PLabel))]
        public IHttpActionResult GetPLabel(int id)
        {
            PLabel plabel = db.PLabels.Find(id);
            if (plabel == null)
            {
                return NotFound();
            }

            return Ok(plabel);
        }

        // PUT api/Default1/5
        public IHttpActionResult PutPLabel(int id, PLabel plabel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != plabel.id)
            {
                return BadRequest();
            }

            db.Entry(plabel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PLabelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Default1
        [ResponseType(typeof(PLabel))]
        public IHttpActionResult PostPLabel(PLabel plabel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PLabels.Add(plabel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = plabel.id }, plabel);
        }

        // DELETE api/Default1/5
        [ResponseType(typeof(PLabel))]
        public IHttpActionResult DeletePLabel(int id)
        {
            PLabel plabel = db.PLabels.Find(id);
            if (plabel == null)
            {
                return NotFound();
            }

            db.PLabels.Remove(plabel);
            db.SaveChanges();

            return Ok(plabel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PLabelExists(int id)
        {
            return db.PLabels.Count(e => e.id == id) > 0;
        }
    }
}