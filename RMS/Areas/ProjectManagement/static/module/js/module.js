﻿'use strict';
var ProjectModule = angular.module("ProjectModule", []);
var projectBaseUrl = "/Areas/ProjectManagement/static/module";

ProjectModule.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        //Example
        .state('example', {
            url: "/example",
            templateUrl: projectBaseUrl + "/views/example.html",
            data: { pageTitle: 'Жишээ', pageSubTitle: 'Жишээ' },
            controller: "ExampleController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'ProjectModule',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            projectBaseUrl + '/js/controllers/ExampleController.js'
                        ]
                    });
                }]
            }
        })

    //create post
    .state('createpost', {
        url: "/post/create",
        templateUrl: projectBaseUrl + "/views/Post/createOrUpdate.html",
        data: { pageTitle: 'Мэдээ', pageSubTitle: 'Мэдээ нэмэх'},
        controller: "PostController",
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'ProjectModule',
                    insertBefore: '#ng_load_plugins_before',
                    files: [
                        '/assets/global/plugins/bootstrap-select/bootstrap-select.min.css',
                        '/assets/global/plugins/select2/select2.css',
                        '/assets/global/plugins/jquery-multi-select/css/multi-select.css',

                       '/assets/global/plugins/clockface/css/clockface.css',
                         '/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                         '/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css',
                         '/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css',
                         '/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                         '/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                        '/assets/global/plugins/bootstrap-select/bootstrap-select.min.js',
                        '/assets/global/plugins/select2/select2.min.js',
                        '/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js',

                       '/assets/admin/pages/scripts/components-dropdowns.js',

                       '/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                            '/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '/assets/global/plugins/clockface/js/clockface.js',
                            '/assets/global/plugins/bootstrap-daterangepicker/moment.min.js',
                            '/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                            '/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                            '/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                        '/assets/admin/pages/scripts/components-pickers.js',
                        '/assets/global/plugins/moment.min.js',

                        projectBaseUrl + '/js/controllers/PostController.js'

                    ]
                });
            }]
        }
    })
    .state('uodatepost', {
        url: "/post/update/:post_id",
        templateUrl: projectBaseUrl + "/views/Post/createOrUpdate.html",
        data: { pageTitle: 'Мэдээ', pageSubTitle: 'Мэдээ засварлах' },
        controller: "PostController",
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'ProjectModule',
                    insertBefore: '#ng_load_plugins_before',
                    files: [
                        '/assets/global/plugins/bootstrap-select/bootstrap-select.min.css',
                        '/assets/global/plugins/select2/select2.css',
                        '/assets/global/plugins/jquery-multi-select/css/multi-select.css',

                       '/assets/global/plugins/clockface/css/clockface.css',
                         '/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                         '/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css',
                         '/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css',
                         '/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                         '/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                        '/assets/global/plugins/bootstrap-select/bootstrap-select.min.js',
                        '/assets/global/plugins/select2/select2.min.js',
                        '/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js',

                       '/assets/admin/pages/scripts/components-dropdowns.js',

                       '/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                            '/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '/assets/global/plugins/clockface/js/clockface.js',
                            '/assets/global/plugins/bootstrap-daterangepicker/moment.min.js',
                            '/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                            '/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
                            '/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                        '/assets/admin/pages/scripts/components-pickers.js',
                        '/assets/global/plugins/moment.min.js',

                        projectBaseUrl + '/js/controllers/PostController.js'

                    ]
                });
            }]
        }
    })

    ;//end state
}]);