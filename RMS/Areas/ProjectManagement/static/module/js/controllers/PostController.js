﻿'use strict';

ProjectModule.controller('PostController', ["$rootScope", "$scope", "$http", "$timeout", '$stateParams','$state', function ($rootScope, $scope, $http, $timeout, $stateParams, $state) {
    $scope.$on('$viewContentLoaded', function() {
        init();
    });

    function init() {

        $scope.post_id = 0;        

        $scope.error = {
            is: false,
            message: "error"
        }

        $scope.warning = {
            is: false,
            message: "warning"
        }
        $scope.info = {
            is: false,
            message: "info"
        }
        if ($stateParams.post_id) {
            $scope.post_id = $stateParams.post_id;
            fillData();

        } else {
            fillEmpty();
        }

        $scope.formTitle = "Шинэ мэдээ бүртгэх";

        jQuery("#input_tags").select2({
            tags: $scope.getAllTags()
        });
    }

    function fillData() {

        $scope.post = {
            Title: "",
            content: "",
            Url: "",
            tags: "",
            sDate: moment().format('YYYY-MM-DD'),
            eDate: moment().add(7, 'days').format('YYYY-MM-DD')
        }

        jQuery("#input_tags").val($scope.post.tags)
    }

    function fillEmpty() {
        $scope.post={
            Title : "",
            content : "",
            Url : "",
            tags : "",
            sDate : moment().format('YYYY-MM-DD'),
            eDate: moment().add(7, 'days').format('YYYY-MM-DD')
        }
        jQuery("#input_tags").val($scope.post.tags)
    }

    $scope.getAllTags = function () {

        var request = {
            method: 'GET',
            url: '/api/PLabel',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
            },
            //data: formData
        };
        var res=[]
        $http(request)
            .success(function (data, status, headers, config) {
                console.log(status);
                console.log(data);
                jQuery.each(data, function (idx, value) {
                    res.push(value.tag);
                });
            })
            .error(function (data, status, headers, config) {                
                console.log(status);
                console.log(data);
            });

        return res;
    }


    $scope.fnReload = function () {
        alert("Reload");
        if ($scope.post_id > 0) {
            fillEmpty();
        } else {
            fillData();
        }
    }

    $scope.register = function () {
        $scope.tags = jQuery("#input_tags").val();
        console.log($scope.post.sDate);
        console.log($scope.post.eDate);
        console.log($scope.post_id)
    }
}]);