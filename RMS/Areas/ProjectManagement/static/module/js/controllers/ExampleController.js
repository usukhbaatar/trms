'use strict';

ProjectModule.controller('ExampleController', ["$rootScope", "$scope", "$http", "$timeout", function ($rootScope, $scope, $http, $timeout) {
    $scope.$on('$viewContentLoaded', function() {   
        init();
    });

    function init() {
        $scope.projectMessage = "ExampleController: Hello World!";

    }
}]);