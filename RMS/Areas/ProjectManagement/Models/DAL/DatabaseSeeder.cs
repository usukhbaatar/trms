﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Areas.ProjectManagement.Models.DAL
{
    public class ProjectDbSeeder : System.Data.Entity.DropCreateDatabaseIfModelChanges<ProjectDbContext>
    {
        protected override void Seed(ProjectDbContext context){
            var companies = new List<Company> { 
                new Company{Name = "Монгол Улсын Их Сургууль", Web="http://num.edu.mn", Address="Бага тойруу-14200, Сүхбаатар дүүрэг, Улаанбаатар, Монгол", Phone="75754444"},
                new Company{Name = "Хэрэглээний Шинжлэх Ухааны Сургууль, Монгол Улсын Их Сургууль", Web="http://seas.num.edu.mn", Address="Бага тойруу-14200, Сүхбаатар дүүрэг, Улаанбаатар, Монгол", Phone="75754444"}
            };

            companies.ForEach(c => context.Companies.Add(c));
            context.SaveChanges();

            var proposals = new List<Proposal> { 
                new Proposal(){ Company = companies[0], ProjectName="Туршилтын төсөл", ContactAddress="хаяг", ContactEmail="info@example.com", ContactPhone="99999999", Cost=1566543, StartDate=DateTime.Now, EndDate=DateTime.Now, HeadRequirement="Профессор", ParticipantRequirement="Masters", Motivation="Туршилт", Outcome="Туршилт"},
            };

            proposals.ForEach(p => context.Proposals.Add(p));
            context.SaveChanges();

            var PLabels = new List<PLabel> { 
                new PLabel(){ tag="Элсэлт"},
                new PLabel(){ tag="Төсөл"},
                new PLabel(){ tag="Шинэ төсөл"},
            };

            PLabels.ForEach(p => context.PLabels.Add(p));
            context.SaveChanges(); 
        }
        
    }
}