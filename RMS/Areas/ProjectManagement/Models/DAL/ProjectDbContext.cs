﻿using RMS.Areas.ProjectManagement.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System;

using Microsoft.AspNet.Identity;
using System.Web;

namespace RMS.Areas.ProjectManagement.Models.DAL
{    
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext()
            : base("DefaultConnection")
        { 
            
        }
        
        public DbSet<Company> Companies { get; set; }
        public DbSet<Proposal> Proposals { get; set; }
        public DbSet<PLabel> PLabels { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public override int SaveChanges()
        {       
            var auditedEntities = ChangeTracker.Entries<Model>();
              //.Where(p => p.State == EntityState.Added)
              //.Select(p => p.Entity);

            string userName = "Anonioumys";
            if (HttpContext.Current != null) {
                userName = HttpContext.Current.User.Identity.GetUserName();
            }

            var now = DateTime.Now;

            foreach (var entity in auditedEntities)
            {
                if (entity.Entity is Model) {
                    Model model = entity.Entity;

                    if (entity.State == EntityState.Added) {                    
                        model.CreatedAt = now;
                        model.UpdatedAt = now;
                        model.CreatedBy = userName;
                    }
                    if (entity.State == EntityState.Modified) {
                        model.UpdatedAt = now;
                        model.UpdatedBy = userName;
                    }
                }
            }
            return base.SaveChanges();
        }
    }


}