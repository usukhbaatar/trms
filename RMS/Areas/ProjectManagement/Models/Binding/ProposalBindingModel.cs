﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace RMS.Areas.ProjectManagement.Models.Binding
{
    public class ProposalBindingModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Төслийн нэр")]
        public string ProjectName { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Удирдагчийн шаардлага")]        
        public string HeadRequirement { get; set; }

        [Required]
        [StringLength(1000, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Оролцогчдийн шаардлага")]        
        public string ParticipantRequirement { get; set; }

        [Required]
        [StringLength(1000, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Хэрэгжүүлэх үндэслэл")]
        public string Motivation { get; set; }

        [Required]
        [StringLength(1000, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Хийх ажил, үр дүн")]        
        public string Outcome { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Холбоо барих хаяг")]        
        public string ContactAddress { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Холбоо барих и-мэйл")]        
        public string ContactEmail { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Утасны дугаар")]        
        public string ContactPhone { get; set; }

        [Required]        
        [DataType(DataType.Currency)]
        [Display(Name = "Төсөв")]        
        public float Cost { get; set; }

        [Required]        
        [DataType(DataType.Date)]
        [Display(Name = "Эхлэх огноо")]        
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Дуусах огноо")]        
        public DateTime EndDate { get; set; }

    }
}