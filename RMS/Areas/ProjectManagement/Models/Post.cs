﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RMS.Areas.ProjectManagement.Models
{
    public class Post : Model
    {

        public Post()
        {
            this.PLabels = new List<PLabel>();
        }

        [Key]
        public int id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Гарчиг")]
        public string Title { get; set; }
                
        [StringLength(50, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Зурагны зам")]
        public string Url { get; set; }

        [Required]
        [StringLength(1000, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [Display(Name = "Агуулга")]
        public string content { get; set; }

        [Required(ErrorMessage="Эхлэх огноо хоосон байна.")]        
        [DataType(DataType.Date)]
        [Display(Name = "Эхлэх огноо")]
        public DateTime sData { get; set; }

        [Required(ErrorMessage = "Дуусах огноо хоосон байна.")]
        [DataType(DataType.Date)]
        [Display(Name = "Эхлэх огноо")]
        public DateTime eData { get; set; }

        public string shortContent
        {
            get
            {
                string temp = System.Text.RegularExpressions.Regex.Replace(this.content, "<.*?>", string.Empty);
                int len = 50;
                if (temp.Length <= len)
                    return temp;
                return temp.Substring(0, len);
            }
        }
        [NotMapped]
        public string pLabelsString{get; set;}

        public virtual ICollection<PLabel> PLabels { get; set; }
        
    }
}