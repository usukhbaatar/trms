﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Areas.ProjectManagement.Models
{
    public class Proposal : Model
    {
        
        public int ID { get; set; }
        public string ProjectName { get; set; }
        public string HeadRequirement { get; set; }
        public string ParticipantRequirement { get; set; }
        public string Motivation { get; set; }
        public string Outcome { get; set;  }
        public string ContactAddress { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public float Cost { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public Company Company { get; set; }
    }
}