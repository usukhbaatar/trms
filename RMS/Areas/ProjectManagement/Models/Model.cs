﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Areas.ProjectManagement.Models
{
    public class Model
    {
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}