﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace RMS.Areas.ProjectManagement.Models
{
    public class Company : Model
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Web { get; set; }
        public string Description { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<Proposal> Proposals { get; set; }
    }
}