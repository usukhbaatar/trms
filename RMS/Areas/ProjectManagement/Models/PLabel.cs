﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RMS.Areas.ProjectManagement.Models
{
    public class PLabel
    {
        [Key]
        public int id { get; set; }

        
        [Required]
        [Index(IsUnique = true)]
        [StringLength(1000, ErrorMessage = "{0} нь багадаа {2} тэмдэгт байна.", MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string tag { get; set; }

        [JsonIgnore]
        public virtual ICollection<Post> Posts { get; set; }
    }
}
