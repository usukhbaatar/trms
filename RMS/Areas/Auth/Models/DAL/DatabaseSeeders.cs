﻿using RMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS.Areas.Auth.Models.DAL
{
    public class RolePermissionSeeder : System.Data.Entity.DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var perms = new List<Permission>
            {
                new Permission(){ Slug = "user.list", Name = "Хэрэглэгч жагсаан харах" },
                new Permission(){ Slug = "user.view", Name = "Хэрэглэгчийн дэлгэрэнгүй мэдээлэл харах" },
                new Permission(){ Slug = "user.edit", Name = "Хэрэглэгчийн мэдээлэл засах" },
                new Permission(){ Slug = "user.create", Name = "Хэрэглэгч үүсгэх" },
                new Permission(){ Slug = "user.set_password", Name = "Хэрэглэгчийн нууц үг тохируулах" },
            };
            perms.ForEach(i => context.Permissions.Add(i));

            ApplicationRole admin = new ApplicationRole("admin", "admin");
            admin.Permissions = perms;
            var roles = new List<ApplicationRole> { 
               admin,
               new ApplicationRole("company", "company"),
            };
            roles.ForEach(i => context.IdentityRoles.Add(i));
            context.SaveChanges();
            
        }
    }

}