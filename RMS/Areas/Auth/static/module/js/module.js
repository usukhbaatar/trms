﻿'use strict';
var AuthModule = angular.module("AuthModule", []);
var authBaseUrl = "/Areas/Auth/static/module";

AuthModule.factory('authInterceptor', function ($q, $cookies) {        
        return {
            request: function (config) {
                if($cookies.access_token)
                    config.headers.Authorization = $cookies.token_type + " " + $cookies.access_token;
                return config;
            },
            response: function (response) {
                if (response.status === 401) {
                    // TODO: handle the case where the user is not authenticated
                }
                return response || $q.when(response);
            }
        };    
});

AuthModule.config(["$httpProvider", function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
}]);

//States
AuthModule.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        //Register
        .state('register', {
            url: "/register",
            templateUrl: authBaseUrl + "/views/register.html",
            data: { pageTitle: 'Бүртгэл', pageSubTitle: '' },
            controller: "RegisterController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AuthModule',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            authBaseUrl + '/js/controllers/RegisterController.js'
                        ]
                    });
                }]
            }
        })
        //Login
        .state('login', {
            url: "/login",
            templateUrl: authBaseUrl + "/views/login.html",
            data: { pageTitle: 'Нэвтрэх', pageSubTitle: '' },
            controller: "LoginController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AuthModule',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            authBaseUrl + '/js/controllers/LoginController.js'
                        ]
                    });
                }]
            }
        });
}]);

