﻿'use strict';

AuthModule.controller('LoginController', ["$rootScope", "$scope", "$http","$cookies", '$cookieStore', 
    function ($rootScope, $scope, $http, $cookies, $cookieStore) {
    $scope.$on('$viewContentLoaded', function () {
        init();
    });

    function init() {
        

    }

    $scope.login = function () {
        var formData = { grant_type: 'password', username: $scope.username, password: $scope.password };
        formData = $.param(formData);
        var request = {
            method: 'POST',
            url: '/Token',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8' },
            data: formData
        };

        $http(request)
            .success(function (data, status, headers, config) {                
                $cookies.access_token= data.access_token;
                $cookieStore.token_type =data.token_type;
                $cookieStore.username = data.userName;
            })
            .error(function (data, status, headers, config) {
                console.log(status);
                console.log(data);
            });
    }

    $scope.logout = function () {
        $cookieStore.remove('access_token');
        $cookieStore.remove('token_type');
        $cookieStore.remove('username');
    }
}]);