'use strict';

AuthModule.controller('RegisterController', ["$rootScope", "$scope", "$http", "$timeout", function ($rootScope, $scope, $http, $timeout) {
    $scope.$on('$viewContentLoaded', function() {   
        init();
    });

    function init() {
        $scope.projectMessage = "Hello World!";

    }

    $scope.register = function () {
        var formData = { Email: $scope.Email, Password: $scope.Password, ConfirmPassword: $scope.ConfirmPassword };
        console.log(formData);
        var request = {
            method: 'POST',
            url: '/api/Account/Register',
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
            },
            data: formData
        };

        $http(request)
            .success(function (data, status, headers, config) {
                console.log(status);
                console.log(data);
            })
            .error(function (data, status, headers, config) {
                console.log(status);
                console.log(data);
            });
    }
}]);