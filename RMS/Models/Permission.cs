﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace RMS.Models
{
    public class Permission
    {
        public int ID { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<ApplicationRole> Roles { get; set; }
    }
}