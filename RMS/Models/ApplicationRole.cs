﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace RMS.Models
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string name, string description) : base(name) {
            this.Description = description;       
        }

        public string Description { get; set; }

        [JsonIgnore]
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}