﻿using RMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Security.Principal;
using System.Web;
using System.Web.Http.Filters;
using Microsoft.AspNet.Identity;
using System.Net;

using RMS.Utils;

namespace RMS.Filters
{
    public class PermissionActionFilter : ActionFilterAttribute
    {
        private readonly String permission;
        public readonly static String PUPLIC_PERMISSION = "public";
        public PermissionActionFilter(String permission) {
            this.permission = permission;
        }

        public PermissionActionFilter() { }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            Type type = actionContext.ControllerContext.Controller.GetType();

            IPrincipal user=null;
            if(HttpContext.Current!=null)
                user = HttpContext.Current.User;
            if (this.permission.ToLower().Equals(PUPLIC_PERMISSION) == false)
            {

                ApplicationUser appUser = ApplicationUser.GetCurrentUser();
                if (appUser == null || user.Identity.IsAuthenticated == false)
                {
                    throw new HttpException(401, "Not authorized.");
                }                
                if (appUser.hasPermission(this.permission) == false)
                {
                    HttpResponseExceptionCreater.CreateHttpResponseException("Permission denied", HttpStatusCode.Forbidden);
                }
            }
            base.OnActionExecuting(actionContext);
        }    
    }
}